#pragma once
#ifndef _SCN_TRANSFORM_H_
#define _SCN_TRANSFORM_H_
#include <d3dx9.h>

class scnTRANSFORM
{
public:
   scnTRANSFORM();
   D3DXMATRIX& Get() { return mat; }
   const D3DXMATRIX& GetConst() const { return mat; }
   scnTRANSFORM& Translate(const D3DXVECTOR3& v);
   scnTRANSFORM& SetTranslate(const D3DXVECTOR3& v);
   scnTRANSFORM& Scale(const D3DXVECTOR3& v);
   scnTRANSFORM& SetScale(const D3DXVECTOR3& v);
   scnTRANSFORM& RotateX(float angle);
   scnTRANSFORM& SetRotateX(float angle);
   scnTRANSFORM& RotateY(float angle);
   scnTRANSFORM& SetRotateY(float angle);
   scnTRANSFORM& RotateZ(float angle);
   scnTRANSFORM& SetRotateZ(float angle);
   scnTRANSFORM& operator*=(const scnTRANSFORM& right);
   scnTRANSFORM operator*(const scnTRANSFORM& right);
   scnTRANSFORM& Id();
private:
   D3DXMATRIX mat;
};
#endif