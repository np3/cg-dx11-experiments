#ifndef _VID_DRV_H_
#define _VID_DRV_H_
#include <d3dx9.h>
#include "d3d_drv.h"



class BASE_SHADER : public d3dSHADER
{
public:
   const D3D11_INPUT_ELEMENT_DESC* CreateIALayout(int *num);
};

class scnPRIM;
class scnHIERARCHY;
class rendMANAGER
{
private:
   static const int vecWidth = 4 * sizeof(float);
   static const int matWidth = 4 * vecWidth;
   // Common constants per frame
   d3dCONSTANT_BUFFER *pFrameCB;
   d3dCONSTANT_BUFFER *pObjCB;
   d3dCONSTANT_BUFFER *pLightCB;

   static const int COMMON_SLOT = 0;
   static const int OBJ_SLOT = 1;
   static const int LIGHTING_SLOT = 2;
   static const int COMMON_VIEWPROJ_MATRIX_OFFSET = 0;
   static const int COMMON_EYEPOS_OFFSET = 4;
   static const int COMMON_BUFFER_SIZE = 5 * vecWidth;
   static const int OBJ_WORLD_MATRIX_OFFSET = 0;
   static const int OBJ_WORLD_INV_MATRIX_OFFSET = 4;
   static const int OBJ_BUFFER_SIZE = 8 * vecWidth;
   static const int LIGHTING_BUFFER_SIZE = 6 * 3 * vecWidth;
public:
   rendMANAGER();
   ~rendMANAGER();
   void InitCommonData(d3dDRIVER *);
   void SetTransform(const D3DXMATRIX &m);
   void SetViewProjTransform(const D3DXMATRIX &m);
   void SetLightingData(void *pData, int size);
   void SetEyePos(const D3DXVECTOR3& eyePos);
   void UploadCommonData(d3dDRIVER *);
   void Render(d3dDRIVER*, scnPRIM *);
   void Render(d3dDRIVER*, scnHIERARCHY *);
};
#endif