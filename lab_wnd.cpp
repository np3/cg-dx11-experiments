#include "lab_wnd.h"
#include "scn_prim.h"
#include <cstdio>

enum PRIMITIVE_ID
{
   CYL_BASE_0,
   SPH_BASE_0,
   CYL_BASE_1,
   SPH_BASE_1,
   CYL_BASE_2,
   SPH_BASE_2,
   CYL_LEFT,
   SPH_LEFT,
   CYL_LEFT_END,
   CYL_RIGHT,
   SPH_RIGHT,
   CYL_RIGHT_END,
};

class scnTRANSFORM_PREDICATE
{
private:
   float t;
public:
   scnTRANSFORM_PREDICATE(float _t) : t(_t) {}
   void operator()(scnHIERARCHY::NODE *pNode)
   {
      scnTRANSFORM& tr = pNode->trans;
      switch(pNode->ID) {
         case CYL_BASE_0:
            tr.SetRotateY(cos(t)*1.25f);
            break;
         case SPH_BASE_0:
            tr.SetTranslate(D3DXVECTOR3(0, 1.5f, 0));
            break;
         case CYL_BASE_1:
            tr.SetRotateX(-D3DX_PI * 0.5f);
            break;
         case SPH_BASE_1:
            tr.SetTranslate(D3DXVECTOR3(0, 1.0f, 0)).RotateY(t);
            break;
         case CYL_BASE_2:
            tr.SetRotateZ(-0.5f * cos(t));
            break;
         case SPH_BASE_2:
            tr.SetTranslate(D3DXVECTOR3(0, 1.0f, 0));
            break;
         case CYL_LEFT:
            tr.SetRotateZ(-D3DX_PI * 0.25f);
            break;
         case SPH_LEFT:
            tr.SetTranslate(D3DXVECTOR3(0, 1.0f, 0)).RotateY(cos(t)*0.3f);
            break;
         case CYL_LEFT_END:
            tr.SetRotateZ(D3DX_PI * 0.25f);
            break;
         case CYL_RIGHT:
            tr.SetRotateZ(D3DX_PI * 0.25f);
            break;
         case SPH_RIGHT:
            tr.SetTranslate(D3DXVECTOR3(0, 1.0f, 0)).RotateY(-cos(t)*0.4f);
            break;
         case CYL_RIGHT_END:
            tr.SetRotateZ(-D3DX_PI * 0.25f);
            break;
      }
   }
};

LAB_WND::LAB_WND(const char *pTitle, int Width, int Height) : WND(pTitle, Width, Height), 
                                                              prevTime(0.0f),
                                                              prevMouseX(0),
                                                              prevMouseY(0),
                                                              plane(0xFF00FF00),
                                                              dirLight(scnLIGHT::DIRECTIONAL),
                                                              pointLight(scnLIGHT::POINT),
                                                              spotLight(scnLIGHT::SPOT)
{
   memset(handParts, 0, sizeof(handParts));
   frameCounter = 0;
   d3dDRIVER *drv = d3dDRIVER::Inst();
   if (!drv->Init(GetHandle(), Width, Height)) {
      Exit();
      return;
   }
   ShowWindow(true);
   camera.BuildPerspProjMatrix(D3DX_PI * 0.25f, (float)Width / (float)Height, 0.01f, 10000.0f);
   camera.LookAt(D3DXVECTOR3(6.0f, 4.5f, -12.5f), D3DXVECTOR3(0.f, 0.f, 0.f), 
                 D3DXVECTOR3(0.f, 1.f, 0.f));
   rendMgr.InitCommonData(drv);
   //
   plane.CreatePlane();
   plane.GetPrimitiveDesc().cullMode = d3dSTATE_DESC::NONE;
   //
   plane.Init(drv);
   plane.LocalTransform().SetRotateX(D3DX_PI * 0.5f).Scale(D3DXVECTOR3(25.f, 25.f, 25.f)).Translate(D3DXVECTOR3(-10.5f, 0, -10.5f));
   //
   dirLight.SetDirection(D3DXVECTOR3(2, 1, 0));
   dirLight.GetDiffuse() = D3DXVECTOR3(0.25f, 0.25f, 0.25f);
   pointLight.SetPosition(D3DXVECTOR3(0.0f, 4.f, 0.0f));
   pointLight.SetAttenuation(1.0f, 0.0f, 0.0f);
   pointLight.GetSpecular() = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 180.8f);
   pointLight.SetRange(5.0f);
   spotLight.SetPosition(D3DXVECTOR3(0.0f, 4.0f, 0.0f));
   spotLight.SetDirection(D3DXVECTOR3(1.0f, 1.0f, 0.0f));
   spotLight.SetAttenuation(1.0f, 0.0f, 0.0f);
   spotLight.GetSpecular() = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 180.8f);
   spotLight.SetRange(9.0f);
   spotLight.SetTheta(D3DX_PI * 0.5f);
   spotLight.SetPhi(0.0125f);
   //
   lightMgr.PushLight(&dirLight);
   lightMgr.PushLight(&pointLight);
   lightMgr.PushLight(&spotLight);
   float cc[4] = {0.25f, 0.25f, 0.25f, 0.0f};
   drv->SetClearColor(cc);
   baseShader.Init(drv, "shaders\\base.hlsl");
   drv->SetShader(baseShader);
   // Hierarchy init
   const float cylRad = 0.1f;
   const float sphRad = 0.255f;
   for (int i = 0; i < 12; ++i) {
      handParts[i] = new scnPRIM();
      switch(i) {
         case CYL_BASE_0:
            handParts[i]->LocalTransform().SetScale(D3DXVECTOR3(1, 1.5f, 1));
            handParts[i]->CreateCyl(cylRad);
            break;
         case CYL_LEFT_END:
         case CYL_RIGHT_END:
            handParts[i]->GetPrimitiveDesc().cullMode = d3dSTATE_DESC::NONE;
            handParts[i]->SetColor(0xFF0000F0);
            handParts[i]->CreateCyl(cylRad * 0.5f);
            break;
         case CYL_BASE_1:
         case CYL_BASE_2:
         case CYL_LEFT:
         case CYL_RIGHT:
            handParts[i]->CreateCyl(cylRad);
            break;
         case SPH_BASE_0:
         case SPH_BASE_1:
         case SPH_BASE_2:
         case SPH_LEFT:
         case SPH_RIGHT:
            handParts[i]->SetColor(0xFF0000F0);
            handParts[i]->CreateSphere(sphRad);
            break;
      }
      handParts[i]->Init(drv);
   }
   SetupHier();
}

LAB_WND::~LAB_WND()
{
   for (int i = 0; i < 12; ++i) {
      delete handParts[i];
   }
}

void LAB_WND::SetupHier()
{
   auto base0 = hier.GetRoot();
   base0->SetGeom(CYL_BASE_0, handParts[CYL_BASE_0]);
   base0 = hier.GetRoot()->AddChild();
   base0->SetGeom(SPH_BASE_0, handParts[SPH_BASE_0]);
   auto base1 = base0->AddChild();
   base1->SetGeom(CYL_BASE_1, handParts[CYL_BASE_1]);
   base1 = base1->AddChild();
   base1->SetGeom(SPH_BASE_1, handParts[SPH_BASE_1]);
   auto base2 = base1->AddChild();
   base2->SetGeom(CYL_BASE_2, handParts[CYL_BASE_2]);
   base2 = base2->AddChild();
   base2->SetGeom(SPH_BASE_2, handParts[SPH_BASE_2]);
   auto left = base2->AddChild();
   left->SetGeom(CYL_LEFT, handParts[CYL_LEFT]);
   //return;
   left = left->AddChild();
   left->SetGeom(SPH_LEFT, handParts[SPH_LEFT]);
   left = left->AddChild();
   left->SetGeom(CYL_LEFT_END, handParts[CYL_LEFT_END]);
   auto right = base2->AddChild();
   right->SetGeom(CYL_RIGHT, handParts[CYL_RIGHT]);
   right = right->AddChild();
   right->SetGeom(SPH_RIGHT, handParts[SPH_RIGHT]);
   right = right->AddChild();
   right->SetGeom(CYL_RIGHT_END, handParts[CYL_RIGHT_END]);
}

void LAB_WND::InputFunc(UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg) {
      case WM_SIZE:
         camera.BuildPerspProjMatrix(D3DX_PI * 0.25f, (float)GetW() / (float)GetH(), 0.01f, 10000.0f);
         d3dDRIVER::Inst()->Resize(GetW(), GetH());
         break;
      case WM_KEYDOWN:
         switch (wParam)
         {
            case VK_ESCAPE:
               Exit();
               break;
            case 'W':
               d3dDRIVER::Inst()->ToggleWireframe();
               break;
            case '1':
               dirLight.Toggle();
               break;
            case '2':
               pointLight.Toggle();
               break;
            case '3':
               spotLight.Toggle();
               break;
         }
         break;
      case WM_MOUSEMOVE:
         {
            int xPos = LOWORD(lParam);
            int yPos = HIWORD(lParam);

            if ((wParam & MK_LBUTTON) && prevMouseX >= 0) {
               camera.RotateLookAtHorz(-(float)(xPos - prevMouseX) * 0.01f);
               camera.RotateLookAtVert((float)(yPos - prevMouseY) * 0.01f);
            }
            prevMouseX = xPos;
            prevMouseY = yPos;
            break;
         }
      case WM_MOUSEWHEEL:
      {
         float zDelta = (float)((short)(HIWORD(wParam)));

         camera.Zoom(-zDelta * 0.007f);
         break;
      }
   }
}

void LAB_WND::PrepareFrame()
{
   const float fpsMeasurementTime = 1.25f;

   fpsTimer.Update();
   float time = fpsTimer.GetTimeSec();
   float dt = time - prevTime;
   if (dt > fpsMeasurementTime) {
      const int FPS_STRLEN = 60;
      char fpsStr[FPS_STRLEN] = {0};
      float fps = (float)frameCounter / dt;

      sprintf_s<FPS_STRLEN>(fpsStr, "%s FPS = %f", GetTitle(), fps);
      SetWindowText(GetHandle(), fpsStr);
      prevTime = time;
      frameCounter = 0;
   }
   ++frameCounter;
   //
   d3dDRIVER* d3d = d3dDRIVER::Inst();
   if (camera.IsMoved()) {
      rendMgr.SetViewProjTransform(camera.GetViewProjMatrix());
      rendMgr.SetEyePos(camera.GetPos());
      camera.ResetMoveFlag();
   }
   pointLight.Transform().SetTranslate(D3DXVECTOR3(8.f, 0, 0)).RotateY(time);
   spotLight.Transform().SetTranslate(D3DXVECTOR3(0.f, 0, -1.f)).RotateY(-time);
   rendMgr.SetLightingData(lightMgr.BuildLightData(), lightMgr.GetDataSize());
   scnTRANSFORM_PREDICATE pred(time);
   hier.Apply(pred);
}

void LAB_WND::ProcessFrame()
{
   d3dDRIVER* d3d = d3dDRIVER::Inst();

   d3d->Clear();
   d3d->BeginRender();
   rendMgr.Render(d3d, &plane);
   rendMgr.Render(d3d, &hier);
   d3d->EndRender();
}
