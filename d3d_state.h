#ifndef _D3D_STATE_H_
#define _D3D_STATE_H_

struct d3dSTATE_DESC
{
   enum CULL_MODE
   {
      NONE,
      BACK,
      FRONT
   }; 
   CULL_MODE cullMode;
   bool zWrite;
   enum ZTEST_FUNC {
      LESS_EQUAL,
      LESS,
      ALWAYS,
      GREATER,
      GREATER_EQUAL
   };
   ZTEST_FUNC zFunc;
   bool zTest;
   enum FILL_MODE
   {
      SOLID,
      WIREFRAME
   };
   FILL_MODE fillMode;
   d3dSTATE_DESC() : cullMode(BACK), zWrite(true), zFunc(LESS_EQUAL), zTest(true), fillMode(SOLID) {}
   unsigned int GetCode() const {
      unsigned int val = 0;

      val |= (int)cullMode; // 2 bits
      val |= ((int)zWrite) << 2; // 3 bits
      val |= ((int)zFunc) << 3; // 6 bits
      val |= ((int)zTest) << 6; // 7 bits
      val |= ((int)fillMode) << 7; // 7 bits
      return val;
   }
};

#endif