#pragma once
#ifndef _WND_H_
#define _WND_H_
#include <windows.h>

class WND
{
private:
   int W, H;
   HWND hWnd;
   const char *pCurTitle;
   void Create(const char *pTitle);
   static LRESULT CALLBACK WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
protected:
   virtual void InputFunc(UINT msg, WPARAM wParam, LPARAM lParam) = 0;
   virtual void PrepareFrame() {}
   virtual void ProcessFrame() {}
public:
   WND(const char *pTitle, int Width, int Height);
   virtual ~WND() {}
   HWND GetHandle() const { return hWnd; }
   const char* GetTitle() const { return pCurTitle; }
   int Loop();
   int GetW() const { return W; }
   int GetH() const { return H; }
   void Exit();
   void SetCursor(bool enable) { ShowCursor(enable); }
   void ShowWindow(bool show);
};
#endif