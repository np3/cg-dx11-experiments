#include "wnd.h"

LRESULT CALLBACK WND::WndProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam)
{
     static WND *pInst;

     switch(msg)
     {
        case WM_CREATE:
           pInst = (WND *)(((CREATESTRUCT*)lParam)->lpCreateParams);
           break;
        case WM_DESTROY:
           PostQuitMessage(0);
           break;
        case WM_SIZE:
           pInst->W = LOWORD(lParam), pInst->H = HIWORD(lParam);
        case WM_KEYUP:
        case WM_KEYDOWN:
        case WM_MOUSEMOVE:
        case WM_MOUSEWHEEL:
           pInst->InputFunc(msg, wParam, lParam);
           break;
        default:
           return DefWindowProc(hWnd, msg, wParam, lParam);
     }

     return 0;
}

WND::WND(const char *pTitle, int Width, int Height)
{
   W = Width;
   H = Height;
   Create(pTitle);
}

void WND::Create(const char *pTitle)
{
   pCurTitle = pTitle;
   const char *pClass = "LAB_APP";
   WNDCLASSEX wc = {0};
   HINSTANCE hInst = GetModuleHandle(NULL);

   wc.cbSize        = sizeof(WNDCLASSEX);
   wc.style         = 0;
   wc.lpfnWndProc   = WndProc;
   wc.cbClsExtra    = 0;
   wc.cbWndExtra    = 0;
   wc.hInstance     = hInst;
   wc.hIcon         = LoadIcon(NULL, IDI_APPLICATION);
   wc.hCursor       = LoadCursor(NULL, IDC_ARROW);
   wc.hbrBackground = (HBRUSH)COLOR_BACKGROUND;
   wc.lpszMenuName  = NULL;
   wc.lpszClassName = pClass;
   wc.hIconSm       = LoadIcon(NULL, IDI_APPLICATION);

   if (!RegisterClassEx(&wc)) {
      return;
   }
   hWnd = CreateWindowEx(WS_EX_CLIENTEDGE, pClass, pTitle, 
                         WS_OVERLAPPEDWINDOW, CW_USEDEFAULT, CW_USEDEFAULT,
                         W, H, NULL, NULL, hInst, this);
   if (hWnd == NULL) {
      return;
   }
   ShowWindow(false);
   UpdateWindow(hWnd);
}

int WND::Loop()
{
   MSG msg;

   while (1) 
   {
      if(PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
      {
         if (msg.message == WM_QUIT) {
            break;
         }
         TranslateMessage(&msg);
         DispatchMessage(&msg);
      }
      else 
      {
         PrepareFrame(); // update & etc
         ProcessFrame(); // render
         //Sleep(1);
      }
   }
   return 0;
}

void WND::Exit()
{
   PostQuitMessage(0);
}

void WND::ShowWindow(bool show)
{
   ::ShowWindow(hWnd, show ? SW_NORMAL : SW_HIDE);
}