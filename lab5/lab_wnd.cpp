#include "lab_wnd.h"
#include "scn_prim.h"
#include <cstdio>

#define ENABLE_STENCIL
LAB_WND::LAB_WND(const char *pTitle, int Width, int Height) : WND(pTitle, Width, Height), 
                                                              prevTime(0.0f),
                                                              prevMouseX(0),
                                                              prevMouseY(0),
                                                              plane(0xFF00FF00),
                                                              dirLight(scnLIGHT::DIRECTIONAL),
                                                              pointLight(scnLIGHT::POINT),
                                                              spotLight(scnLIGHT::SPOT)
{
   frameCounter = 0;
   d3dDRIVER *drv = d3dDRIVER::Inst();
   if (!drv->Init(GetHandle(), Width, Height)) {
      Exit();
      return;
   }
   ShowWindow(true);
   camera.BuildPerspProjMatrix(D3DX_PI * 0.25f, (float)Width / (float)Height, 0.01f, 10000.0f);
   /*camera.LookAt(D3DXVECTOR3(-102.6f, 68.2f, -83.6f), D3DXVECTOR3(0.f, 0.f, 0.f), 
                 D3DXVECTOR3(0.f, 1.f, 0.f));*/
   camera.LookAt(D3DXVECTOR3(58.0f, 58.f, -58.0f), D3DXVECTOR3(0.0f, 0.f, 0.f), 
                 D3DXVECTOR3(0.f, 1.f, 0.f));
   
   rendMgr.InitCommonData(drv);
   //
   plane.CreatePlane();
   plane.GetPrimitiveDesc().cullMode = d3dSTATE_DESC::NONE;

   screen_quad.CreateFullScreenQuad();
   screen_quad.GetPrimitiveDesc().cullMode = d3dSTATE_DESC::NONE;
#ifdef ENABLE_STENCIL
   screen_quad.GetPrimitiveDesc().stencilMode = d3dSTATE_DESC::S_CHECK;
#endif
   screen_quad.GetPrimitiveDesc().zTest = false;
   screen_quad.GetPrimitiveDesc().zWrite = false;
   bbox.CreateBox();
   bbox.GetPrimitiveDesc().stencilMode = d3dSTATE_DESC::S_SET;
   bbox.GetPrimitiveDesc().zWrite = false;
   //
   screen_quad2.CreateFullScreenQuad();
   screen_quad2.GetPrimitiveDesc().cullMode = d3dSTATE_DESC::NONE;
   screen_quad2.GetPrimitiveDesc().stencilMode = d3dSTATE_DESC::S_SET;
   screen_quad2.GetPrimitiveDesc().zTest = false;
   screen_quad2.GetPrimitiveDesc().zWrite = false;

   plane.Init(drv);
   screen_quad.Init(drv);
   screen_quad2.Init(drv);
   bbox.Init(drv);
   plane.LocalTransform().SetScale(D3DXVECTOR3(10, 10, 10)).RotateX(D3DX_PI * 0.5f).RotateY(D3DX_PI * 0.5f).
     Translate(D3DXVECTOR3(-5, -0.5, 5));
   //
   dirLight.SetDirection(D3DXVECTOR3(0, 1, 0));
   dirLight.GetDiffuse() = D3DXVECTOR3(0.95f, 0.95f, 0.95f);
   pointLight.SetPosition(D3DXVECTOR3(0.0f, 10.f, 0.0f));
   pointLight.SetAttenuation(1.0f, 0.0f, 0.125f);
   pointLight.GetSpecular() = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 180.8f);
   pointLight.SetRange(25.0f);
   spotLight.SetPosition(D3DXVECTOR3(0.0f, 10.0f, 0.0f));
   spotLight.SetDirection(D3DXVECTOR3(1.0f, 1.0f, 0.0f));
   spotLight.SetAttenuation(1.0f, 0.0f, 0.0f);
   spotLight.GetSpecular() = D3DXVECTOR4(1.0f, 1.0f, 1.0f, 180.8f);
   spotLight.SetRange(20.0f);
   spotLight.SetTheta(D3DX_PI * 0.5f);
   spotLight.SetPhi(0.0125f);
   //
   lightMgr.PushLight(&dirLight);
   //lightMgr.PushLight(&pointLight);
   //lightMgr.PushLight(&spotLight);
   float cc[4] = {0.1f, 0.1f, 0.1f, 0.0f};
   //float cc[4] = {0.25f, 0.25f, 0.25f, 0.0f};
   //float cc[4] = {0/255.f, 125/255.f, 73/255.f, 0.0f};
   drv->SetClearColor(cc);
   baseShader.Init(drv, "shaders\\base.hlsl");
   baseStencilShader.Init(drv, "shaders\\base.hlsl");
   rmShader.Init(drv, "shaders\\raymarch.hlsl");
   rendMgr.SetBoxesData(BRIDGE2, bboxes);
   for (int i = 0; i < 9; ++i) {
     bboxes[i].Init(drv);
   }
   //float b[12] = {0};
   //b[0] = (float)Width;
   //b[1] = (float)Height;
   //memcpy_s(&b[4], 4 * sizeof(float), &camera.GetPos(), sizeof(D3DXVECTOR3));
   //drv->GetD3DContext()->UpdateSubresource(pFrameCB, 0, NULL, b, 0, 0);
}

LAB_WND::~LAB_WND()
{
}

void LAB_WND::InputFunc(UINT msg, WPARAM wParam, LPARAM lParam)
{
   switch(msg) {
      case WM_SIZE:
         camera.BuildPerspProjMatrix(D3DX_PI * 0.25f, (float)GetW() / (float)GetH(), 0.01f, 10000.0f);
         d3dDRIVER::Inst()->Resize(GetW(), GetH());
         break;
      case WM_KEYDOWN:
         switch (wParam)
         {
            case VK_ESCAPE:
               Exit();
               break;
            case 'W':
               d3dDRIVER::Inst()->ToggleWireframe();
               break;
            case '1':
               dirLight.Toggle();
               break;
            case '2':
               pointLight.Toggle();
               break;
            case '3':
               spotLight.Toggle();
               break;
         }
         break;
      case WM_MOUSEMOVE:
         {
            int xPos = LOWORD(lParam);
            int yPos = HIWORD(lParam);

            if ((wParam & MK_LBUTTON) && prevMouseX >= 0) {
               camera.RotateLookAtHorz(-(float)(xPos - prevMouseX) * 0.01f);
               camera.RotateLookAtVert((float)(yPos - prevMouseY) * 0.01f);
            }
            prevMouseX = xPos;
            prevMouseY = yPos;
            break;
         }
      case WM_MOUSEWHEEL:
      {
         float zDelta = (float)((short)(HIWORD(wParam)));

         camera.Zoom(-zDelta * 0.01f);
         break;
      }
   }
}

void LAB_WND::PrepareFrame()
{
   const float fpsMeasurementTime = 1.25f;

   fpsTimer.Update();
   float time = fpsTimer.GetTimeSec();
   float dt = time - prevTime;
   if (dt > fpsMeasurementTime) {
      const int FPS_STRLEN = 100;
      char fpsStr[FPS_STRLEN] = {0};
      float fps = (float)frameCounter / dt;

      auto camPos = camera.GetPos();
      sprintf_s<FPS_STRLEN>(fpsStr, "%s FPS = %f", GetTitle(), fps);
      SetWindowText(GetHandle(), fpsStr);
      prevTime = time;
      frameCounter = 0;
   }
   ++frameCounter;
   //
   d3dDRIVER* d3d = d3dDRIVER::Inst();
   if (camera.IsMoved()) {
      rendMgr.SetViewProjTransform(camera.GetViewProjMatrix());
      rendMgr.SetEyePos(camera.GetPos());
      camera.ResetMoveFlag();
   }
   pointLight.Transform().SetTranslate(D3DXVECTOR3(45.f, 20, 0)).RotateY(time);
   spotLight.Transform().SetTranslate(D3DXVECTOR3(1.f, 10, -1.f)).RotateY(-time);
   rendMgr.SetLightingData(lightMgr.BuildLightData(), lightMgr.GetDataSize());
}

void LAB_WND::ProcessFrame()
{
   d3dDRIVER* d3d = d3dDRIVER::Inst();
   d3d->Clear(d3dDRIVER::C_COLOR | d3dDRIVER::C_DEPTH | d3dDRIVER::C_STENCIL);
   

   d3d->BeginRender();
#ifdef ENABLE_STENCIL
   d3d->SetShader(baseStencilShader);
   for (int i = 0; i < 9; ++i) {
    rendMgr.Render(d3d, &bboxes[i]);
   }
#endif
   //rendMgr.Render(d3d, &bbox);
   d3d->SetShader(rmShader);
   D3DXMATRIX trView;
   D3DXMatrixTranspose(&trView, &camera.GetViewMatrix());
   rendMgr.SetFullscreenData(camera.GetPos(), camera.GetTarget(), 
                             camera.GetProjMatrix(), trView, fpsTimer.GetTimeSec());
   rendMgr.RenderScreenQuad(d3d, &screen_quad);
   d3d->EndRender();
}
