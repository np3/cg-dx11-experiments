#ifndef _SCN_LIGHT_H_
#define _SCN_LIGHT_H_
#include "scn_transform.h"
#include <d3dx9.h>
#include <vector>
class scnLIGHT_MANAGER;

class scnLIGHT
{
   friend class scnLIGHT_MANAGER;
private:
   scnTRANSFORM tr;
   scnTRANSFORM dirTr;
   bool enabled;
   D3DXVECTOR3 diffuse;
   D3DXVECTOR4 specular;
   void TransformPoint(D3DXVECTOR3& p, scnTRANSFORM &t);
   void TransformDir(D3DXVECTOR3& d, scnTRANSFORM &t);
   D3DXVECTOR3 direction;
   D3DXVECTOR3 position;
   D3DXVECTOR3 attenuation;
   float theta; // outer cone
   float phi; // inner cone
   float range;
   float falloff;
public:
   enum Type
   {
      DIRECTIONAL,
      POINT,
      SPOT
   };
   Type type;
   scnLIGHT(Type t);
   ~scnLIGHT() {}
   Type GetType() { return type; }
   scnTRANSFORM& Transform() { return tr; }
   void Enable(bool isOn) { enabled = isOn; }
   void Toggle() { enabled = !enabled; }
   D3DXVECTOR3& GetDiffuse() { return diffuse; }
   D3DXVECTOR4& GetSpecular() { return specular; }
   void SetPosition(const D3DXVECTOR3 &p) { position = p; }
   void SetDirection(const D3DXVECTOR3 &d) { direction = d; }
   void SetRange(float r) { range = r; }
   void SetFalloff(float f) { falloff = f; }
   void SetAttenuation(float att0, float att1 = 0.0f, float att2 = 0.0f);
   void SetTheta(float t) { theta = t; }
   void SetPhi(float t) { phi = t; }
};

class scnLIGHT_MANAGER
{
private:
  std::vector<scnLIGHT *> lights;
  D3DXVECTOR4 ambientColor;
  struct PACKED_LIGHT {
     /*float4 posDir; // w - parallel/non-parallel
     float4 spotDir;
     float4 diffuse;
     float4 specular;
     float4 params; // x, y - spot angular factors, z - falloff
     float4 att;*/
     D3DXVECTOR4 posDir;
     D3DXVECTOR4 spotDir;
     D3DXVECTOR4 diffuse;
     D3DXVECTOR4 specular;
     D3DXVECTOR4 params;
     D3DXVECTOR4 att;

     PACKED_LIGHT() {
        memset(this, 0, sizeof(PACKED_LIGHT));
        specular.x = specular.y = specular.z = 1.0f;
        specular.w = 1.0f;
        params.y = params.z = 1.0f;
        att.x = 1.0f;
        posDir.y = 1.0f;
     }
  };
  std::vector<PACKED_LIGHT> packed;
public:
   scnLIGHT_MANAGER() {}
   void PushLight(scnLIGHT *p);
   int GetLightsNum() { return (int)lights.size(); }
   int GetDataSize() { return 3 * sizeof(PACKED_LIGHT); }
   void* BuildLightData();
};

#endif