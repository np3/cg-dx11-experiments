#include "common.h"
#include <cstdlib>
#include <cmath>

#define M_PI       3.14159265358979323846f

void *align_malloc(int size, int alignment)
{
   // Very simple alignment
   int offset = alignment - 1 + sizeof(void *);
   void *p = malloc(size + offset);
   void *res = (void *)(((int)p + alignment) & ~offset);
   // pack original ptr for correct free
   *((int*)res - 1) = (int)p;
   return res;
}

void align_free(void *p)
{
   void *pp = ((int *)p - 1);
   free(pp);
}

float InterpolateLinear(float a, float b, float t)
{
   return a + t * (b - a);
}

float InterpolateCosine(float a, float b, float t)
{
   float r = t * M_PI;
   r = (1 - cos(r)) * 0.5f;

   return InterpolateLinear(a, b, r);
}

static float noise[512][512];

void initNoiseTable()
{
   for (int i = 0; i < 512; ++i) {
      for (int j = 0; j < 512; ++j) {
         noise[i][j] = (rand() % 32768) / 32768.0f;
      }
   }
}

float rawNoise(int x, int y)
{
   return noise[(x + 512) & 511][(y + 512) & 511];
}

float interpolatedNoise(float x, float y) {
   int ix = (int) x;
   float fx = x - (float) ix;

   int iy = (int) y;
   float fy = y - (float) iy;

   float nx1y1 = rawNoise(ix, iy);
   float nx2y1 = rawNoise(ix + 1, iy);
   float nx1y2 = rawNoise(ix, iy + 1);
   float nx2y2 = rawNoise(ix + 1, iy + 1);

   float ny1 = InterpolateCosine(nx1y1, nx2y1, fx);
   float ny2 = InterpolateCosine(nx1y2, nx2y2, fx);

   return InterpolateCosine(ny1, ny2, fy);
}

float Turbulence(float x, float y, float size)
{
   float val = 0.0, initSize = size;

   while(size >= 1.0f) {
      val += interpolatedNoise(x / size, y / size) * size;
      size /= 2.0f;
   }
   return val / initSize;
}

unsigned char* GeneratePerlinLikeNoise(int W, int H)
{
   unsigned char *pBuf = new unsigned char[W * H];
   unsigned char *pRes = pBuf;

   initNoiseTable();
   for (int i = 0; i < H; ++i) {
      for (int j = 0; j < W; ++j) {
         float n = 0.5f * Turbulence((float)j, (float)i, 64.0f);
         *pRes++ = (unsigned char)((unsigned int)(255.0f * n) & 0xFF);
      }
   }
   return pBuf;
}