#include "common.h"
#include "rend_mgr.h"
#include "d3d_drv.h"
#include "scn_prim.h"

const D3D11_INPUT_ELEMENT_DESC* BASE_SHADER::CreateIALayout(int *num)
{
   //
   static D3D11_INPUT_ELEMENT_DESC inpDesc[4];

   inpDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
   inpDesc[0].InputSlot = 0;
   inpDesc[0].AlignedByteOffset = 0;
   inpDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
   inpDesc[0].SemanticIndex = 0;
   inpDesc[0].SemanticName = "POSITION";
   inpDesc[0].InstanceDataStepRate = 0;

   inpDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
   inpDesc[1].InputSlot = 0;
   inpDesc[1].AlignedByteOffset = 3 * sizeof(float);
   inpDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
   inpDesc[1].SemanticIndex = 0;
   inpDesc[1].SemanticName = "NORMAL";
   inpDesc[1].InstanceDataStepRate = 0;

   inpDesc[2].Format = DXGI_FORMAT_B8G8R8A8_UNORM;
   inpDesc[2].InputSlot = 0;
   inpDesc[2].AlignedByteOffset = 3 * sizeof(float) + inpDesc[1].AlignedByteOffset;
   inpDesc[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
   inpDesc[2].SemanticIndex = 0;
   inpDesc[2].SemanticName = "COLOR";
   inpDesc[2].InstanceDataStepRate = 0;

   inpDesc[3].Format = DXGI_FORMAT_R32G32_FLOAT;
   inpDesc[3].InputSlot = 0;
   inpDesc[3].AlignedByteOffset = sizeof(DWORD) + inpDesc[2].AlignedByteOffset;
   inpDesc[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
   inpDesc[3].SemanticIndex = 0;
   inpDesc[3].SemanticName = "TEXCOORD";
   inpDesc[3].InstanceDataStepRate = 0;
   //
   *num = sizeof(inpDesc) / sizeof(D3D11_INPUT_ELEMENT_DESC);

   return inpDesc;
}

const D3D11_INPUT_ELEMENT_DESC* QUAD_SHADER::CreateIALayout(int *num)
{
  num = 0;
  return nullptr;
}

rendMANAGER::rendMANAGER() {

}

rendMANAGER::~rendMANAGER() {

}

void rendMANAGER::SetupConstantBuffer(d3dDRIVER *d3d, d3dCONSTANT_BUFFER *pBuf, UINT slot)
{
  if (bufStates[slot] != pBuf) {
    d3d->SetConstantBuffer(slot, pBuf);
    bufStates[slot] = pBuf;
  }
}

void rendMANAGER::InitCommonData(d3dDRIVER *d3d) {
   pFrameCB = d3d->CreateConstantBuffer(COMMON_BUFFER_SIZE);
   pObjCB = d3d->CreateConstantBuffer(OBJ_BUFFER_SIZE);
   pLightCB = d3d->CreateConstantBuffer(LIGHTING_BUFFER_SIZE);
   pQuadFrameCB = d3d->CreateConstantBuffer(QUAD_FRAME_BUFFER_SIZE);
   pQuadBoxesCB = d3d->CreateConstantBuffer(QUAD_BOXES_BUFFER_SIZE);
}

void rendMANAGER::UploadCommonData(d3dDRIVER *d3d)
{
   if (pFrameCB->NeedUpload()) {
      d3d->UploadConstantBuffer(pFrameCB);
   }
   if (pObjCB->NeedUpload()) {
      d3d->UploadConstantBuffer(pObjCB);
   }
   if (pLightCB->NeedUpload()) {
      d3d->UploadConstantBuffer(pLightCB);
   }
   if (pQuadFrameCB->NeedUpload()) {
      d3d->UploadConstantBuffer(pQuadFrameCB);
   }
   if (pQuadBoxesCB->NeedUpload()) {
      d3d->UploadConstantBuffer(pQuadBoxesCB);
   }
}

void rendMANAGER::SetTransform(const D3DXMATRIX &m)
{
   D3DXMATRIX tW;
   D3DXMatrixTranspose(&tW, &m);
   pObjCB->SetConst(OBJ_WORLD_MATRIX_OFFSET * vecWidth, (void *)&tW, matWidth);
   D3DXMatrixInverse(&tW, nullptr, &m);
   pObjCB->SetConst(OBJ_WORLD_INV_MATRIX_OFFSET * vecWidth, (void *)&tW, matWidth);
}

void rendMANAGER::SetViewProjTransform(const D3DXMATRIX &m)
{
   pFrameCB->SetConst(COMMON_VIEWPROJ_MATRIX_OFFSET * vecWidth, (void *)&m, matWidth);
}

void rendMANAGER::SetLightingData(void *pData, int size)
{
   pLightCB->SetConst(0, pData, size);
}

void rendMANAGER::RenderScreenQuad(d3dDRIVER *drv, scnPRIM *prim)
{
  SetupConstantBuffer(drv, pQuadFrameCB, COMMON_SLOT);
  SetupConstantBuffer(drv, pQuadBoxesCB, OBJ_SLOT);
  UploadCommonData(drv);
  prim->Render(drv);
}

void rendMANAGER::Render(d3dDRIVER* drv, scnPRIM *prim)
{
   SetupConstantBuffer(drv, pFrameCB, COMMON_SLOT);
   SetupConstantBuffer(drv, pObjCB, OBJ_SLOT);
   SetupConstantBuffer(drv, pLightCB, LIGHTING_SLOT);
   SetTransform(prim->LocalTransform().GetConst());
   UploadCommonData(drv);
   prim->Render(drv);
}

void rendMANAGER::Render(d3dDRIVER* drv, scnHIERARCHY *hierPrim)
{
   scnHIERARCHY_ITER iter(hierPrim);

   for (iter.Begin(); !iter.IsDone(); ++iter)
   {
      if (iter->prim) {
         scnTRANSFORM finalTr = iter->prim->LocalTransform() * iter->trans;
         SetTransform(finalTr.GetConst());
         UploadCommonData(drv);
         iter->prim->Render(drv);
      }
   }
}

void rendMANAGER::SetEyePos(const D3DXVECTOR3& eyePos)
{
   pFrameCB->SetConst(COMMON_EYEPOS_OFFSET * vecWidth, (void*)&eyePos, 3 * sizeof(float));
}

void rendMANAGER::SetFullscreenData(const D3DXVECTOR3& eyePos, const D3DXVECTOR3& eyeTarget, 
                                    const D3DXMATRIX &projM, const D3DXMATRIX &viewM, float time)
{
  float v[4] = {0};
  float v2[4] = {0};
  pQuadFrameCB->SetConst(0, (void *)&eyePos, 3 * sizeof(float));
  pQuadFrameCB->SetConst(vecWidth, (void *)&eyeTarget, 3 * sizeof(float));
  v[0] = 1.0f / projM(0, 0);
  v[1] = 1.0f / projM(1, 1);
  v[2] = projM(2, 2);
  v[3] = projM(3, 2);
  v2[0] = time;
  pQuadFrameCB->SetConst(2 * vecWidth, (void *)v, vecWidth);
  pQuadFrameCB->SetConst(3 * vecWidth, (void *)&viewM, matWidth);
  pQuadFrameCB->SetConst(3 * vecWidth + matWidth, (void *)v2, vecWidth);
}

void rendMANAGER::SetBoxesData(SCENE_NAME scn, scnPRIM *boxes)
{
  struct NODE {
    D3DXMATRIX txx;
    D3DXVECTOR4 rad;
  };

  struct NODES {
    NODE box[MAX_NODES_COUNT];
    D3DXVECTOR4 color[MAX_NODES_COUNT];
  } nodes;

  if (scn == BRIDGE2) {
    static const float BASE_LENGTH = 30.0f;
    static const float BASE_WIDTH  = 8.5f;
    static const float BASE_THICKNESS  = 0.5f;

    static const float BEAM_LENGTH = 19.99f;
    static const float BEAM_THICKNESS = 0.5f;
    static const float BEAM_WIDTH = 0.8f;
    static const float BEAM_POS = 1.0f;

    static const float CONN_WIDTH = 0.8f;
    static const float CONN_THICKNESS = 0.5f;
    static const float CONN_LENGTH = 5.0f;

    static const float STRIP_WIDTH = 0.2f;
    static const float STRIP_THICKNESS = 0.01f;
    static const float STRIP_LENGTH = 3.0f;
    static const float BSTRIP_LENGTH = (BASE_LENGTH - 0.01f);

    D3DXVECTOR3 gSizes[MAX_NODES_COUNT];
    D3DXVECTOR3 gPositions[MAX_NODES_COUNT];
    D3DXVECTOR3 gColors[MAX_NODES_COUNT];
    // TODO: setup those from CPU
    gSizes[0] = D3DXVECTOR3(BASE_WIDTH, BASE_THICKNESS, BASE_LENGTH);                // Base
    gSizes[1] = D3DXVECTOR3(BEAM_WIDTH + 2.5f, BEAM_THICKNESS + 2.5f*3.f, BEAM_LENGTH);  // Beam 1
    gSizes[2] = D3DXVECTOR3(BEAM_WIDTH + 2.5f, BEAM_THICKNESS + 2.5f*3.f, BEAM_LENGTH);  // Beam 2
    gSizes[3] = D3DXVECTOR3(STRIP_WIDTH, STRIP_THICKNESS, BASE_LENGTH);              // Road center strip
    gSizes[4] = D3DXVECTOR3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH); 
    gSizes[5] = D3DXVECTOR3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH*0.5f);                
    gSizes[6] = D3DXVECTOR3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH*0.5f);                
    gSizes[7] = D3DXVECTOR3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH*0.5f);                
    gSizes[8] = D3DXVECTOR3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH*0.5f);                

    gPositions[0] = D3DXVECTOR3( 0.0f, 0.0f, 0.0f);                                     // Base
    gPositions[1] = D3DXVECTOR3(+ (BASE_WIDTH - BEAM_POS) - 2.5f, 5.5f, 0.0f);          // Beam 1
    gPositions[2] = D3DXVECTOR3(- (BASE_WIDTH - BEAM_POS) + 2.5f, 5.5f, 0.0f);          // Beam 2
    gPositions[3] = D3DXVECTOR3(0.f, BASE_THICKNESS, 0.f);                               // Road center strip
    gPositions[4] = D3DXVECTOR3(0.f, 12.5f, 0.f);                                         // Upper horisontal beam
    gPositions[5] = D3DXVECTOR3(0.f, 6.5f,  BEAM_LENGTH*0.65f);                          // Horisontal beam 1                                     
    gPositions[6] = D3DXVECTOR3(0.f, 9.5f,  BEAM_LENGTH*0.45f);                          // Horisontal beam 2                                     
    gPositions[7] = D3DXVECTOR3(0.f, 6.5f, -BEAM_LENGTH*0.65f);                          // Horisontal beam 1
    gPositions[8] = D3DXVECTOR3(0.f, 9.5f, -BEAM_LENGTH*0.45f);                          // Horisontal beam 2

    gColors[0] = D3DXVECTOR3(0.9f, 0.1f, 0.0f);
    gColors[1] = D3DXVECTOR3(0.1f, 0.9f, 0.0f);
    gColors[2] = D3DXVECTOR3(0.1f, 0.0f, 0.9f);
    gColors[3] = D3DXVECTOR3(0.9f, 0.9f, 0.0f);
    gColors[4] = D3DXVECTOR3(0.9f, 0.0f, 0.9f);
    gColors[5] = D3DXVECTOR3(0.0f, 0.9f, 0.9f);
    gColors[6] = D3DXVECTOR3(0.4f, 0.9f, 0.4f);
    gColors[7] = D3DXVECTOR3(0.9f, 0.4f, 0.4f);
    gColors[8] = D3DXVECTOR3(0.4f, 0.4f, 0.9f);
    static int BOXES_COUNT = 9;
    if (boxes != NULL) {
      for (int i = 0; i < BOXES_COUNT; ++i) {
        boxes[i].CreateBox();
        boxes[i].GetPrimitiveDesc().zWrite = true;
        boxes[i].GetPrimitiveDesc().stencilMode = d3dSTATE_DESC::S_SET;
        boxes[i].LocalTransform().SetScale(gSizes[i]).Translate(gPositions[i]);
      }
    }
    for (int i = 0; i < BOXES_COUNT; ++i) {
      D3DXMatrixTranslation(&nodes.box[i].txx, gPositions[i].x, gPositions[i].y, gPositions[i].z);
      D3DXMatrixInverse(&nodes.box[i].txx, NULL, &nodes.box[i].txx);
      D3DXMatrixTranspose(&nodes.box[i].txx, &nodes.box[i].txx);
      nodes.box[i].rad = D3DXVECTOR4(gSizes[i], 0.0f);
      nodes.color[i] = D3DXVECTOR4(gColors[i], 0.0f);
    }
  } else {
    // 
  }
  pQuadBoxesCB->SetConst(0, (void *)&nodes, sizeof(nodes));
}