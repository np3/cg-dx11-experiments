#pragma once
#ifndef _LAB_WND_H_
#define _LAB_WND_H_
#include "common.h"
#include "wnd.h"
#include "timer.h"
#include "d3d_drv.h"
#include "scn_camera.h"
#include "scn_prim.h"
#include "rend_mgr.h"
#include "scn_light.h"

class LAB_WND : public WND
{
private:
   Timer fpsTimer;
   int frameCounter;
   scnCAMERA camera;
   scnPRIM plane;
   scnPRIM bbox;
   scnPRIM bboxes[9];
   scnPRIM screen_quad;
   scnPRIM screen_quad2;
   int prevMouseX;
   int prevMouseY;
   rendMANAGER rendMgr;
   scnLIGHT dirLight;
   scnLIGHT pointLight;
   scnLIGHT spotLight;
   scnLIGHT_MANAGER lightMgr;
   BASE_SHADER baseShader;
   BASE_STENCIL_SHADER baseStencilShader;
   QUAD_SHADER rmShader;
   float prevTime;
   d3dTEX hmap;
   d3dTEX diff0;
   d3dTEX diff1;
   d3dTEX diff2;
   d3dTEX noiseMap;
public:
   LAB_WND(const char *pTitle, int Width, int Height);
   ~LAB_WND();
   void InputFunc(UINT msg, WPARAM wParam, LPARAM lParam);
   void PrepareFrame();
   void ProcessFrame();
};
#endif