#pragma once
#ifndef _D3D_DRV_H_
#define _D3D_DRV_H_
#include <d3d11.h>
#include <vector>
#include "d3d_state.h"

#ifdef _DEBUG
#define D3D_CHECK(x) if (FAILED(x)) { *(int *)0 = 1; }
#else
#define D3D_CHECK(x) (x)
#endif

#define D3D_RELEASE(x) if (x) { \
                        x->Release(); \
                        x = nullptr; \
                       }
class d3dDRIVER;
class d3dBUFFER;
class d3dSHADER
{
   friend class d3dDRIVER;
protected:
    ID3D11VertexShader *pVS;
    ID3D11PixelShader  *pPS;
    ID3D11InputLayout  *pInputLayout;
    d3dBUFFER          *pPassCB;
    bool               skipPixelShader;
public:
    d3dSHADER() : pVS(nullptr), pPS(nullptr), pInputLayout(nullptr), pPassCB(nullptr), skipPixelShader(false) {}
    void Init(d3dDRIVER *drv, const char *pFilename);
    virtual ~d3dSHADER();
    virtual const D3D11_INPUT_ELEMENT_DESC* CreateIALayout(int *num) { *num = 0; return NULL; }
};

class d3dBUFFER
{
   friend class d3dDRIVER;
protected:
   ID3D11Buffer *buf;
   d3dBUFFER() : buf(nullptr) {}
public:
   ~d3dBUFFER() {
      D3D_RELEASE(buf);
   }
   operator ID3D11Buffer*()
   {
      return buf;
   }
};

class d3dCONSTANT_BUFFER : public d3dBUFFER
{
   friend class d3dDRIVER;
private:
   BYTE *cpuData;
   bool needUpload;
   int size;
   d3dCONSTANT_BUFFER(int byteSize) : needUpload(false), size(byteSize) {
      cpuData = new BYTE[byteSize];
      memset(cpuData, 0, byteSize);
   }
public:
   void SetConst(int offset, void *Data, int byteSize);
   ~d3dCONSTANT_BUFFER() {
      if (cpuData) {
         delete[] cpuData;
      }
   }
   bool NeedUpload() { return needUpload; }
};

class d3dTEX
{
   friend class d3dDRIVER;
private:
   ID3D11ShaderResourceView *pView;
   ID3D11Texture2D *pTex2D;
   int W, H;
   void Release() {
      D3D_RELEASE(pView);
      D3D_RELEASE(pTex2D);
      W = 0;
      H = 0;
   }
public:
   d3dTEX() : pView(nullptr), pTex2D(nullptr), W(0), H(0) {}
   ~d3dTEX() {
      Release();
   }
   bool LoadFromFile(d3dDRIVER *drv, const char *pFileName);
   bool GenerateNoiseTex(d3dDRIVER *drv, int Width, int Height);
   ID3D11ShaderResourceView* GetSRV() const { return pView; }
};

class d3dDRIVER
{
private:
   IDXGISwapChain *pSwapChain;
   ID3D11Device *pDevice;
   ID3D11DeviceContext *pImmContext;
   // Z - buffer
   ID3D11Texture2D *pDepthStencilBuf;
   ID3D11DepthStencilView *pDSV;
   // Back buffer
   ID3D11RenderTargetView  *pRTV;
   // Samplers
   ID3D11SamplerState *pSamplers[2];
   //
   // States
   struct d3dSTATE
   {
      ID3D11BlendState* pBlendState;
      ID3D11DepthStencilState* pDSState;
      ID3D11RasterizerState* pRasterState;
      float blendFactors[4];
      UINT sampleMask;
      UINT stencilRef;
      UINT descID;

      d3dSTATE() {}
      ~d3dSTATE() {
         D3D_RELEASE(pBlendState);
         D3D_RELEASE(pDSState);
         D3D_RELEASE(pRasterState);
      }
   };
   std::vector<d3dSTATE *> states;
   d3dSTATE *curState;
   // Special wireframe state
   bool isWireframe;
   d3dSTATE *wireframeState;
   d3dSTATE* CreateNewState(const d3dSTATE_DESC &desc);
   int W, H;
   bool SetupRTV();
   bool SetupDSV();
   bool SetupSamplers();
   void SetupViewport();
   d3dDRIVER() : isWireframe(false), inited(false) {}
   ~d3dDRIVER() { Release(); }
   float clearColor[4];
   bool inited;
   std::vector<d3dBUFFER *> buffersPool;
public:
   static d3dDRIVER* Inst();
   bool Init(HWND hWnd, int W, int H);
   bool IsInited() const { return inited; }
   void Release();
   void Resize(int nW, int nH);
   enum CLEAR_STATE {
     C_COLOR = 1,
     C_DEPTH = 2,
     C_STENCIL = 4
   };
   void Clear(UINT c = (C_COLOR | C_DEPTH));
   void BeginRender();
   void EndRender();
   void ToggleWireframe();
   void SetClearColor(float cc[4]);
   void BackupStates();
   void RestoreStates();
   d3dCONSTANT_BUFFER* CreateConstantBuffer(int byteWidth);
   d3dBUFFER* CreateVertexBuffer(void *pVert, int byteWidth);
   d3dBUFFER* CreateIndexBuffer(void *pInd, int byteWidth);
   void UploadConstantBuffer(d3dCONSTANT_BUFFER *pBuf);
   ID3D11Device* GetD3DDevice() { return pDevice; }
   ID3D11DeviceContext* GetD3DContext() { return pImmContext; }
   void SetVB(d3dBUFFER *pVB, UINT *strides, UINT *offsets);
   void SetIB(d3dBUFFER *pIB);
   void SetConstantBuffer(int slot, d3dCONSTANT_BUFFER *pCB);
   void SetShader(const d3dSHADER &shd);
   void Render(int numVert, D3D11_PRIMITIVE_TOPOLOGY topology, int vertOffset = 0);
   void RenderIndexed(int numInd, D3D11_PRIMITIVE_TOPOLOGY topology, 
                      int vertOffset = 0, int indOffset = 0);
   UINT FitD3DState(const d3dSTATE_DESC &desc);
   void SetD3DState(UINT ID);
};
#endif