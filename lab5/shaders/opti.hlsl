#ifndef _OPTI_HLSL_
#define _OPTI_HLSL_

void initScene()
{
  // TODO: setup those from CPU
  gSizes[0] = float3(1,   1,   1)  ;
  gSizes[1] = float3(1.5, 1.5, 1.5);
  gSizes[2] = float3(2,   2,   2)  ;
  gSizes[3] = float3(2,   2,   2)  ;
  gSizes[4] = float3(1,   1,   1)  ;
  gSizes[5] = float3(1,   1,   1)  ;

  gPositions[0] = float3( 4.0, 0.0, 0.0);
  gPositions[1] = float3( 0.0, 0.0, 0.0);
  gPositions[2] = float3(-4.0, 0.0, 0.0);
  gPositions[3] = float3(-8.0, 0.0, 0.0);
  gPositions[4] = float3(-12.0, 0.0, 0.0);
  gPositions[5] = float3(-15.0, 0.0, 0.0);

  gColors[0] = float3(0.9, 0.1, 0.0);
  gColors[1] = float3(0.1, 0.9, 0.0);
  gColors[2] = float3(0.0, 0.1, 0.9);
  gColors[3] = float3(0.9, 0.1, 0.9);
  gColors[4] = float3(0.9, 0.9, 0.0);
  gColors[5] = float3(0.0, 0.9, 0.9);

  // Compute bounding boxes transformation matrix and its inverse
  for (int i = 0; i < NODE_COUNT; ++i)
  {
    gVisible[i] = true;

    //float4x4 rot = rotationAxisAngle(normalize(float3(1.0,1.0,0.0)), params.x);
    float4x4 tra = translate(gPositions[i]);
    //txi = identity();
    //txi = tra * rot;
    float4x4 txi = tra; // Unneeded
    float4x4 txx = inverse(txi);
    gBoundingBoxes[i].txi = txi; // Unneeded
    gBoundingBoxes[i].txx = txx;
    gBoundingBoxes[i].rad = gSizes[i];
  }
}

float2 map(in float3 pos)
{
  float dis = INF;

#if USE_BB_OPTIMIZATION
  // 2st option: use distance fields for visible nodes only
#if 1 // Hack: twice more fps (suits for box sdf's)
  if (gNearestNode >= 0)
  {
    dis = sdBox1(pos - gPositions[gNearestNode].xyz, gSizes[gNearestNode].xyz);
  }
#else // Fair
  [branch] if (gVisible[0])
  {
    dis = min(dis, sdBox1(pos - gPositions[0], gSizes[0]));
  }
  [branch] if (gVisible[1])
  {
    dis = min(dis, sdBox1(pos - gPositions[1], gSizes[1]));
  }
  [branch] if (gVisible[2])
  {
    dis = min(dis, sdBox1(pos - gPositions[2], gSizes[2]));
  }
  [branch] if (gVisible[3])
  {
    dis = min(dis, sdBox1(pos - gPositions[3], gSizes[3]));
  }
  [branch] if (gVisible[4])
  {
    dis = min(dis, sdBox1(pos - gPositions[4], gSizes[4]));
  }
  [branch] if (gVisible[5])
  {
    dis = min(dis, sdBox1(pos - gPositions[5], gSizes[5]));
  }
#endif

  // 2nd option: use unmodified distance field
  //dis = min(dis, sdBox1(pos - gPositions[0], gSizes[0])); 
  //dis = min(dis, sdBox1(pos - gPositions[1], gSizes[1]));
  //dis = min(dis, sdBox1(pos - gPositions[2], gSizes[2]));
  //dis = min(dis, sdBox1(pos - gPositions[3], gSizes[3]));
  //dis = min(dis, sdBox1(pos - gPositions[4], gSizes[4]));
  //dis = min(dis, sdBox1(pos - gPositions[5], gSizes[5]));
#else
  dis = min(dis, sdBox1(pos - gPositions[0].xyz, gSizes[0].xyz)); 
  dis = min(dis, sdBox1(pos - gPositions[1].xyz, gSizes[1].xyz));
  dis = min(dis, sdBox1(pos - gPositions[2].xyz, gSizes[2].xyz));
  dis = min(dis, sdBox1(pos - gPositions[3].xyz, gSizes[3].xyz));
  dis = min(dis, sdBox1(pos - gPositions[4].xyz, gSizes[4].xyz));
  dis = min(dis, sdBox1(pos - gPositions[5].xyz, gSizes[5].xyz));

  //dis = min(dis, sdBox1(pos - float3( 4.0, 0.0, 0.0), float3(1,   1,   1)  )); 
  //dis = min(dis, sdBox1(pos - float3( 0.0, 0.0, 0.0), float3(1.5, 1.5, 1.5)));
  //dis = min(dis, sdBox1(pos - float3(-4.0, 0.0, 0.0), float3(2,   2,   2)  ));
  //dis = min(dis, sdBox1(pos - float3(-8.0, 0.0, 0.0), float3(2,   2,   2)  ));
#endif // USE_BB_OPTIMIZATION

  return float2(dis * 0.25, 0);
}

#endif // OPTI