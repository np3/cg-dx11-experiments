#if 1
#define MAX_NODES_COUNT 9
#include "shaders\resources.hlsl"
#include "shaders\sd.hlsl"
// Global params
#define USE_BB_OPTIMIZATION       1
#define USE_FULL_MAP_FOR_SHADOWS  1
#define USE_BB_DEBUG              0
#define SHOW_ITERATIONS_COUNT     0
#define DISCARD_BACKGROUND        1
#define MAX_RAY_DISTANCE        500
#define MAX_RAY_STEPS           250

// Shadows params
#define USE_ADAPTIVE_SHADOWS      0
// FIXME: should depend on minimal geometry thickness (otherwise seams in shadows)
#define SHADOWS_TMIN           0.02
#define SHADOWS_TMAX           35.0
//#define SOFT_SHADOWS_STEP       0.5
#define SOFT_SHADOWS_MAXSTEPS   200

// TODO: use preprocessor for this
// Btw speed fps is increasing when setting those to false, so it's ok, i guess
static const bool hasShadows   = true;
static const bool softShadows  = true;
static const bool hasAO        = true;
static const bool hasSpecular  = false;
// Lighting params
#define USE_DYNAMIC_LIGHTING      1
//static const float3 iLightPosition = float3(0.3, 0.5, 1.2);
static const float3 iLightPosition = float3(0.0, 0.4, 0.5);

// Some other params
static const float iGlobalTime = params.x * 0.8;
static const float gammaExponent = 1.0 / 3.2;


// Scene selection (OPTI, BRIDGE2, COLUMNS are for bb-optimization testing for now)
#define BRIDGE2

// Scene nodes count
#ifdef OPTI
  #define NODE_COUNT  6  
#endif // OPTI
#ifdef BRIDGE2
  #define NODE_COUNT  9
#endif // BRIDGE2
#ifdef COLUMNS
  #define NODE_COUNT  2
#endif // COLUMNS

// Array of visibility flags for scene nodes
static bool   gVisible[NODE_COUNT];
// Current single visible node
static float    gNearestNode;

#ifdef OPTI
  #include "shaders\opti.hlsl"
#endif
#ifdef BRIDGE2
  #include "shaders\bridge2.hlsl"
#endif
#ifdef COLUMNS
  #include "shaders\columns.hlsl"
#endif

struct VS_OUTPUT
{
  float4 Pos : SV_POSITION;
  float2 UV : TEXCOORD0;
};


VS_OUTPUT VS(in uint vertexID : SV_VertexID)
{
  VS_OUTPUT output = (VS_OUTPUT)0;

  output.UV = float2((vertexID << 1) & 2, vertexID & 2);
  output.Pos = float4(float2(2.f, -2.0f) * output.UV + float2(-1.f, 1.0f), 0.2f, 1.0f);

  return output;
}


float3 shade(in float3 pos, in float3 nor, in float3 rd, in float matID)
{
  float3 mate = float3(0.0, 0.0, 0.0);
	
  mate = lerp(float3(0.3,0.3,0.3)*0.8, float3(1.0,1.0,1.0)*2.0, saturate(matID - 0.5));
  mate = lerp(mate, float3(0.1,0.0,1.0)*0.0, saturate(matID - 1.5));
  mate = lerp(mate, float3(1.0,1.0,1.0)*0.0, saturate(matID - 2.5));
  // 0 = plane
  //if (matID < 0.5)
  //{
  //  //mate.xyz = float3(0.1,0.2,0.0)*0.5;
  //  mate.xyz = float3(0.3,0.3,0.3)*0.8;
  //}
  //// 1 = sphere
  //else if (matID < 1.5)
  //{
  //  //mate.xyz = float3(0.1,1.0,0.0)*0.5;
  //  mate.xyz = float3(1.0,1.0,1.0)*2.0;
  //}
  // 2 = box
  //else if (matID < 2.5)
  //{
  //  mate.xyz = float3(0.1,0.0,1.0)*0.5;
  //}
  // 3 = cross
  //else if (matID < 3.5)
  //{
  //  //mate.xyz = float3(1.0,0.0,0.0)*0.5;
  //  mate.xyz = float3(1.0,1.0,1.0)*0.5;
  //}
  //// 4 = bridge
  //else if (matID < 4.5)
  //{
  //  mate.xyz = float3(1.0,1.0,1.0)*0.5;
  //}

  return mate;
}

// Ray-Box intersection, by convertig the ray to the local space of the box.
// Returns: t
float iBox(in float3 ro, in float3 rd, in float4x4 txx, in float4x4 txi, in float3 rad) 
{
  // convert from ray to box space
  float3 rdd = (mul(float4(rd, 0.0), txx)).xyz;
  float3 roo = (mul(float4(ro, 1.0), txx)).xyz;

  // ray-box intersection in box space
  float3 m = rcp(rdd);//1.0 / rdd;
  float3 n = m * roo;
  float3 k = abs(m) * rad;

  float3 t1 = -n - k;
  float3 t2 = -n + k;

  float tN = max(max(t1.x, t1.y), t1.z);
  float tF = min(min(t2.x, t2.y), t2.z);

  /*if (tN > tF || tF < 0.0)
  {
    return -1.0;
  }*/
  return lerp(tN, -1.0, max(sign(tN - tF - 0.00001), 0.0) * max(sign(tF + 0.00001), 0.0));
  //return tN;
}
#endif // 1
// Ray-Box intersection, by convertig the ray to the local space of the box.
// Returns: t
float iBox(in float3 ro, in float3 rd, in int bboxIdx) 
{
  // convert from ray to box space
  // mul() operands order matters!!!
  float3 rdd = (mul(float4(rd, 0.0), gBoundingBoxes[bboxIdx].txx)).xyz;
  float3 roo = (mul(float4(ro, 1.0), gBoundingBoxes[bboxIdx].txx)).xyz;

  // ray-box intersection in box space
  float3 m = rcp(rdd);
  float3 n = m * roo;
  float3 k = abs(m) * gBoundingBoxes[bboxIdx].rad;

  float3 t1 = -n - k;
  float3 t2 = -n + k;

  float tN = max(max(t1.x, t1.y), t1.z);
  float tF = min(min(t2.x, t2.y), t2.z);

  /*if (tN > tF || tF < 0.0)
  {
    return -1.0;
  }*/

   
   return lerp(tN, -1.0, max(sign(tN - tF - 0.00001), 0.0) * max(sign(tF + 0.00001), 0.0));
}

#ifdef BRIDGE1

#define BASE_LENGTH     10.0
#define BASE_WIDTH      3.0
#define BASE_THICKNESS  0.3

#define CABLES_RADIUS   0.1
#define CABLES_H        10.0
#define CABLES_N        1
#define CABLES_POS      0.4

float2 map(in float3 pos)
{
  //float br = bridge(pos);

  // Holy shit: Error while compiling pixel shaderinternal error: compilation aborted unexpectedly
  //float2 dis = float2(br, 4.0);
  //float2 dis = float2(br, 3.0);

  //dis = opU(dis, float2(sdTesseract(float4(pos, 0.0)-float4( 1.0,0.25, 0.1, 0.2), float4(0.25, 0.25, 0.25, 0.25)), 3.0));

  //
  // Bridge base
  //
  float t = params.x * 0.0;
  float s = sin(t);
  float f = 0.5 - 0.5 * cos(PI * pos.z / (20.0 * (/*abs*/(s) + 1.2)));
  float signum = 1.0;//sign(s);

  float3 xpos = float3(pos.x, /*1.0 - 6.0 + */pos.y + signum * f * 4.0, pos.z);

  float base = sdBox(xpos, float3(BASE_WIDTH, BASE_THICKNESS, BASE_LENGTH)).x;

  float2 dis = float2(INF, -1);
  dis = opU(dis, float2(base, 3.0));

  // 
  // Cables
  //
  float3 apos = pos - float3(0, CABLES_H, 0);
  float angleStart = PI * 0.2;
  float angleStep  = (angleStart * 2) / (CABLES_N - 1);
  //for (int i = 0; i < CABLES_N; ++i)
  int i = 0;
  {
    float3 p = apos;

    float angle = angleStart - i * angleStep;
    float4 q = makeQuat(z, angle);

    float scale = 1.0 / cos(angle);

    //p = scaleVector(p, scale);
    p = rotateVector(q, p);

    float3 p1 = p - float3(BASE_WIDTH - CABLES_POS, 0, 0);
    float3 p2 = p + float3(BASE_WIDTH - CABLES_POS, 0, 0);

    float c1 = sdCapsule(p1, float3(0.0, 0.0, 0.0),
                             float3(0.0, scale * CABLES_H + signum * f * 4.0, 0.0),
                             CABLES_RADIUS);

    float c2 = sdCapsule(p2, float3(0.0, 0.0, 0.0),
                             float3(0.0, scale * CABLES_H + signum * f * 4.0, 0.0),
                             CABLES_RADIUS);

    float cables = min(c1, c2);

    //float halfH = scale * h * 0.5;
    //float c = sdBox(p - float3(0, halfH, 0), float3(CABLES_RADIUS, halfH, CABLES_RADIUS));

    dis = opU(dis, float2(cables, 3.0));
  }

  // Test opRep. It works quite strange actually
  //float c = sdCapsule(opRep(apos, float3(10.0, 10, 10)),
  //                    float3(0.0, 0.0, 0.0),
  //                    float3(0.0, h, 0.0),
  //                    radius);
  //dis = opU(dis, float2(c, 3.0));
  

  //dis = opU(dis, sdPlane(pos + float3(0.0, 1.3, 0.0)));

  //float3 sizeBox = float3(5.0, 0.2, 10.0);
  //dis = opU(dis, sdBox(opCheapBend(pos), sizeBox));

  // 0.25 is actually magic
  return float2(dis.x * 0.25, dis.y);;
}
#endif // BRIDGE1

#ifdef CSG
float2 map(in float3 pos)
{
  float3 sizeBox = float3(1.0, 1.0, 1.0);
  float4 sizeSphere = float4(0.0, 0.0, 0.0, 1.35);
  float3 sizeCutBox1 = float3(0.1, 2.0, 2.0);
  //float4x4 rot = rotationAxisAngle(float3(0.0, 1.0, 0.0), PI / 2.0);
  //float3 sizeCutBox1 = mul(float4(sizeCutBox, 0.0), rot).xyz;
  float3 sizeCutBox2 = sizeCutBox1.yxz;
  float3 sizeCutBox3 = sizeCutBox1.yzx;

  float3 sizeCrsBox1 = float3(0.15, 0.15, 0.6);
  float3 sizeCrsBox2 = sizeCrsBox1.xzy;
  float3 sizeCrsBox3 = sizeCrsBox1.zxy;



  float2 dis = opS(opS(opS(opS(sdBox(pos, sizeBox), sdSphere(pos, sizeSphere)),
                           sdBox(pos, sizeCutBox1)),
                       sdBox(pos, sizeCutBox2)),
                   sdBox(pos, sizeCutBox3));

  float2 cross = opU(opU(sdBox(pos, sizeCrsBox1), sdBox(pos, sizeCrsBox2)), sdBox(pos, sizeCrsBox3));
  cross.y = 3.0;

  dis = opU(dis, cross);

  //dis = opU(dis, sdPlane(pos + float3(0.0, 1.3, 0.0)));

  return dis;
}
#endif // CSG

#ifdef TEST
float2 map(in float3 pos)
{
  return /*opU(*/opU(sdSphere(pos - float3(0.4, 0.8, 0.0), 0.25), opS(sdBox(pos - float3(0.15, 0, 0.15), float3(0.15, 0.55, 0.15)),
                                                                  sdSphere(pos, 0.25)));//,
                /*sdPlane(pos + float3(0.0, 0.6, 0.0)));*/
  //return sdBox(pos - float3(5.0, 1.65, -5.0), float3(2.15, 1.55, 2.15));
}
#endif // TEST

#ifdef CONSTANT_WIDTH
float2 map(in float3 pos)
{
  float2 dis = float2(INF, -1);

  //float co = sdCone(pos + float3(16.0, 0.0, 0.0), float3(0.2,0.2,10.3));
  //dis = opU(dis, float2(co, 1.0));

  float tp = sdTriPrism(pos + float3(12.0, 0.0, 0.0), float2(1, 3));
  dis = opU(dis, float2(tp, 1.0));

  float hp = sdHexPrism(pos + float3(8.0, 0.0, 0.0), float2(1, 3));
  dis = opU(dis, float2(hp, 2.0));

  float c6 = sdCylinder6(pos + float3(4.0, 0.0, 0.0), float2(1.2, 3));
  dis = opU(dis, float2(c6, 2.0));

  float cw3 = sdConstantWidthCylinder3(pos - float3(0.0, 0.0, 0.0), float2(2, 3));
  dis = opU(dis, float2(cw3, 2.0));

  float cw5 = sdConstantWidthCylinder5(pos - float3(4.0, 0.0, 0.0), float2(2, 3));
  dis = opU(dis, float2(cw5, 2.0));

  //pos = opRep(pos, float3(5.0, 10.0, 5.0));


  float3 pos_cw3d = pos - float3(9.0, 0.0, 0.0);
  float t = params.x * 1.0;
  float4 q = makeQuat(normalize(y), t);
  pos_cw3d = rotateVector(q, pos_cw3d);
  pos_cw3d.y *= -1;

  float cw3d = sdConstantWidth3d(pos_cw3d, 4);
  dis = opU(dis, float2(cw3d, 2.0));

  //dis = opU(dis, mapBridge(pos));

  return dis;
}
#endif // CONSTANT_WIDTH

#if 0
// Sphere tracing
float3 castRay(in float3 ro, in float3 rd, in float maxd)
{
  float precision = 0.005;
  float h = precision * 5.0;
  float t = 0.0;
  float m = -1.0;

  uint i;
  for (i = 0; i < 150; ++i)
  {
    if (abs(h) < precision || t > maxd)
    {
      break;
    }

    t += h;
#if USE_BB_OPTIMIZATION
    float2 res = mapVisible(ro + rd * t);
#else
    float2 res = map(ro + rd * t);
#endif // USE_BB_OPTIMIZATION
    h = res.x;
    m = res.y;
  }
  if (t > maxd)
  {
    m = -1.0;
  }
  return float3(t, m, i/* * (t < maxd)*/);
}
#else

// Significally decreases fps on my scenes - need to research
#define NEW_LIMIT_STEP false

// Hybrid sphere tracing (https://www.shadertoy.com/view/Mdj3W3)
float3 castRay(in float3 ro, in float3 rd, in float maxd, in float startT)
{
  float precision = 0.0005;
  // initial guess for t - just pick the start of the ray
  float t = startT;
  float m = -1.0;

  float d = map(ro + rd * t).x;
	float tNext, dNext;
  
  float last_t = t + 10000.; // something far away from t0

  uint i;
  for (i = 0; i < MAX_RAY_STEPS; ++i)
  {
    // termination condition - iteration has converged to surface
    if (abs(last_t - t) < precision || t > maxd)
    {
      break;
    }

    tNext = t + d;
//#if USE_BB_OPTIMIZATION
    float2 res = mapVisible(ro + rd * tNext);
//#else
//    float2 res = mapFull(ro + rd * tNext);
//#endif // USE_BB_OPTIMIZATION
	  dNext = res.x;
    m     = res.y;

    // NEW ALGORITHM
		// are we crossing the surface? (sign(d) != sign(dNext)).
		// im detecting this by dividing the two and checking
		// if the result is negative. the only reason i use a divide is because
		// ill reuse the division result later. i thought it would be unstable but
		// it seems to work fine!
		float dNext_over_d = dNext/d;
		if (NEW_LIMIT_STEP && dNext_over_d < 0.0)
		{
			// fit a line from (current t, current d) to (next t, next d),
			// and set t to the approximated intersection of the line with d=0
			
			// the human readable version
			// float grad = (dNext - d) / d;
			// d /= -grad;
			// steeper gradient means smaller step. this is analytically
			// correct (to a linear approximation of the surface)

			// optimised (confuscated) version
			d /= 1.0 - dNext_over_d;
			
			// re-evaluate at the partial step location
			tNext = t + d;
			//dNext = sign(d) * 0.00001; // use +/- epsilon as approximated dist?
#if USE_BB_OPTIMIZATION
      float2 res = mapVisible(ro + rd * tNext);
#else
      float2 res = map(ro + rd * tNext);
#endif // USE_BB_OPTIMIZATION
	    dNext = res.x;
      m     = res.y;

			// OPTION - terminate march after doing this correction step. perhaps
			// i'll visualise the error from this later
      //return float3(t, m, i);
		}
		// END OF NEW ALGORITHM
    
    last_t = t;
		t      = tNext;
		d      = dNext;
  }
  m = lerp(m, -1.0, max(sign(t - maxd - 0.00001), 0.0));
  /*if (t > maxd)
  {
    m = -1.0;
  }*/
  return float3(t, m, i/* * (t < maxd)*/);
}
#endif

#if 1
// Taken from https://www.shadertoy.com/view/XsS3Dm, i.e. not fully 3d normal (and no so fast either)
#define USE_FAST_NORMAL_APPROXIMATION 0

float3 calcNormal(in float3 pos)
{
  float3 eps = float3(0.0001, 0.0, 0.0);

#if USE_FAST_NORMAL_APPROXIMATION	
  float d = map(pos + eps.xyy).x;
	return normalize(float3(map(pos + eps.xyy).x - d,
                          map(pos + eps.yxy).x - d,
                          map(pos + eps.yyx).x - d));
#else	
  float3 nor = float3(map(pos + eps.xyy).x - map(pos - eps.xyy).x,
                      map(pos + eps.yxy).x - map(pos - eps.yxy).x,
                      map(pos + eps.yyx).x - map(pos - eps.yyx).x);
  return normalize(nor);
#endif // USE_FAST_NORMAL_APPROXIMATION
}

float calcAO(in float3 pos, in float3 nor)
{
  float totao = 0.0;
  float sca = 1.0;
  for (float aoi = 0; aoi < 5.0; aoi += 1.0f)
  {
    float hr = 0.01 + 0.05 * aoi;
    float3 aopos =  nor * hr + pos;
    float dd = map(aopos).x;
    totao += -(dd - hr) * sca;
    sca *= 0.75;
  }

  return saturate(1.0 - 4.0 * totao);
}

float shadow(in float3 ro, in float3 rd, float mint, float maxt)
{
  float res = 1.0;
  for (float t = mint; t < maxt; )
  {
#if USE_BB_OPTIMIZATION && USE_FULL_MAP_FOR_SHADOWS 
    float2 h = map(ro + rd * t);
#else 
    float2 h = mapVisible(ro + rd * t);
#endif
    
    if (h.x < 0.001)
    {
      res = 0.0;
      break;
    }
    //t += h.x;
    t += clamp(h, 0.02, 1.0);
  }

  return res;
}

float softShadow(in float3 ro, in float3 rd, in float mint, in float maxt, in float k)
{
  float res = 1.0;
  float t = mint;

  for (uint i = 0; i < SOFT_SHADOWS_MAXSTEPS; ++i)
  {
	  if (t >= maxt)
    {
      break;
    }
	  {
#if USE_BB_OPTIMIZATION && USE_FULL_MAP_FOR_SHADOWS
      float h = map(ro + rd * t).x;
#else 
      float h = mapVisible(ro + rd * t).x;
#endif
      res = min(res, k * h / t);
      // Constant step -- seams in shadows
      //t += SOFT_SHADOWS_STEP;
      // Optimal step
      // TODO: measure perfomance
      t += clamp(h, 0.02, 1.0);
	  }
  }

  return saturate(res);
}

float softShadowAdaptive(in float3 ro, in float3 rd, in float mint, in float maxt, in float k, in float depth)
{
  float res = 1.0;
  float t = mint;

  // FIXME: should depend on minimal geometry thickness (otherwise seams in shadows)
  const float step = min(depth / MAX_RAY_DISTANCE * 4.0 + 0.1, 0.5);
  // Fallback to hard shadows for distant pixels
  // FIXME: hard shadows are slower then soft
  //if (step >= 0.5)
  // return shadow(ro, rd, mint, maxt);

  for (uint i = 0; i < 100; ++i)
  {
	  if (t >= maxt)
    {
      break;
    }
	  {
#if USE_BB_OPTIMIZATION && USE_FULL_MAP_FOR_SHADOWS 
      float h = map(ro + rd * t).x;
#else 
      float h = mapVisible(ro + rd * t).x;
#endif
      res = min(res, k * h / t);
      t += step;
	  }
  }

  return saturate(res);
}

#endif // 1


//#define DECLARE_MAP(i) \
//  float2 map(in float3 pos) \
//  { \
//      return sdConstantWidth3d(pos + NODE_##i_POS, NODE_##i_SIZE.x); \
//  }

float3 render(in float3 ro, in float3 rd, out float3 wpos, /*out float3 wrefl, */out float _ao)
{
//#ifdef USE_BB_OPTIMIZATION
//  float4x4 txi = identity();
//  float4x4 txx = inverse(txi);
//  // raytrace box
//  float3 box = bbox;
//  float result = iBox(ro, rd, txx, txi, box);
//  // clip
//  clip(-(result <= 0.0));
//#endif // USE_BB_OPTIMIZATION


  float3 bbcolor = float3(0, 0, 0);

#if USE_BB_OPTIMIZATION
  float   nearestNode = -1;
  float minDistance = INF;
  for (float i = 0; i < NODE_COUNT; i += 1.0f)
  {
    float isect = iBox(ro, rd, i);
    //float isect = iBox(ro, rd, gBoundingBoxes[0].txx, gBoundingBoxes[0].txi, gBoundingBoxes[0].rad);
    if (isect > 0.0 /*!= -1*/)
    {
      gVisible[i] = true;
      bbcolor += gColors[i];
    //  bbcolor = gColors[i];
    //  wpos = ro + rd * isect;
    //  _ao = 1.0;
    //  return bbcolor;
    }
    //if (nearestNode >= 0)
    //  continue;
    if (isect > 0 /*!= -1*/ && isect < minDistance)
    {
      nearestNode = i;
      minDistance = isect;
      //break;
    }
  }
  // Set nearest node
  gNearestNode = nearestNode;
  // Clip background pixels
  clip(nearestNode);
  float3 t = castRay(ro, rd, MAX_RAY_DISTANCE, minDistance);
#else // USE_BB_OPTIMIZATION
  float3 t = castRay(ro, rd, MAX_RAY_DISTANCE, 0.0);
#endif

  // Cast ray and get intersection distance + object/material index
  
  // Background ?
  float back = step(0.0, t.y);
  // Discard all background pixels (gives more fps!)
#if DISCARD_BACKGROUND && !USE_BB_DEBUG
  clip(t.y + 0.001);
#endif

#if USE_BB_OPTIMIZATION && USE_BB_DEBUG
  if (t.y < 0)
  {
    return float3(bbcolor*0.1);
  }
#endif

  //if (nearestNode > 0)
  //{
  //  wpos = ro + rd * minDistance;
  //  return bbcolor;
  //}


  // Intersection position
  float3 pos = ro + rd * t.x;
  wpos = pos;
  wpos.z = lerp(1.0f, wpos.z, back);
  // Calculate sufrace normal
  float3 norm = calcNormal(pos);
  // Calculate ambient occlusion
  float ao = hasAO * calcAO(pos, norm) + (1 - hasAO);
  _ao = ao;
  // Directional light source
  float3 lig = normalize(iLightPosition);
#if USE_DYNAMIC_LIGHTING
  lig = rotateVector(makeQuat(x, iGlobalTime), lig);
#endif // USE_DYNAMIC_LIGHTING
  // Dot product
  float diff = saturate(dot(lig, norm));
  // Shadow
  float sh = 1.0;
  if (softShadows)
  {
#if USE_ADAPTIVE_SHADOWS
    sh = lerp(sh, softShadowAdaptive(pos, lig, SHADOWS_TMIN, SHADOWS_TMAX, 80.0, t.x), hasShadows);
#else                                           
    sh = lerp(sh, softShadow(pos, lig, SHADOWS_TMIN, SHADOWS_TMAX, 80.0), hasShadows);
#endif // USE_ADAPTIVE_SHADOWS
    //sh += 0.2;
  }
  else
  {
    sh = lerp(sh, shadow(pos, lig, 0.02, 15.0), hasShadows);
  }
  float3 wrefl = reflect(rd, norm);
  // Specular exponent
  float spec = saturate(dot(lig, wrefl));
  // Diffuse component
  float3 diffuse = diff * sh * ao;
  // Objects material color
  //float3 color = lerp(float3(0, 0.5, 0), float3(1, 0.5, 0), t.y);
  float3 color = shade(pos, norm, rd, t.y);
  // Specular component
  float3 specular = hasSpecular * pow(spec, 1024) * sh;
  // Ambient component
  float3 ambient = 0.70 * float3(0.10, 0.11, 0.13);
  // ???
  //float fre = ao * pow(saturate(1.0 + dot(norm, rd)), 5.0) * 0.75;
  float fre = 0.0;


  color += abs(sin(iGlobalTime)) * float3(1.0, 0.3, 0.0) / (centerStrips(pos - norm*.01) * 20.0 + 0.75)/**pow(bass,2.)*/ * 3.0;


  // Final color
#if USE_BB_DEBUG
  float3 res = saturate((diffuse + ambient) * color + specular + fre + bbcolor * 0.1) * back;
#else
  float3 res = saturate((diffuse + ambient) * color + specular + fre) * back;
#endif
  //float3 res = saturate((1.0 + ambient) * color + specular + fre) * back;
  //res *= exp(0.0625 * 0.25 * -t.x * t.x);

  //return float3(t.x / MAX_RAY_DISTANCE, t.x / MAX_RAY_DISTANCE, t.x / MAX_RAY_DISTANCE);
  //return float3(ao, ao, ao);
  //return float3(sh, sh, sh);
  //return float3(ao, ao, ao) * sh;

  //return abs(norm);

#if SHOW_ITERATIONS_COUNT
  //float steps = saturate(log10(t.z + 10) / 10.0);
  float steps = t.z / 150;
  return float3(steps, steps, steps);
#else
  return res;
#endif
}

//float4 PS(in VS_OUTPUT input, out float depth : SV_Depth) : SV_Target
float4 PS(in VS_OUTPUT input/*, out float depth : SV_DepthLessEqual*/) : SV_Target
{
  float2 p = input.UV;
  p = 2 * p - 1;

  // Simple clipping (to check possible benefits from stencil test)
  //clip(-((p.y > 0.6) || (p.y <= -0.6)));

//  float fov = 3.1415925356 * 0.25f;
//  float tfov = tan(fov * 0.5f);
//  p.x *= (WndParams.x / WndParams.y);
//  p.xy *= tfov;
  p.xy *= camParams.xy;
  // cam
  float3 ro = camPos.xyz;//float3(-0.5, 1, 0.5);
  float3 ta = camTarget.xyz;//float3(0.0, 0.0, 0.0);
  //
  float3 cw = normalize( ta-ro );
	float3 cp = float3( 0.0, 1.0, 0.0 );
  // DX left-handed coordinate system
	float3 cu = normalize(cross(cp, cw));
	float3 cv = normalize(cross(cu, cw));
	float3 rd = normalize(p.x * cu + p.y * cv + cw);
  float3 newPos, newDir;
  float ao;

//#ifdef USE_BB_OPTIMIZATION
  initScene();
//#endif // USE_BB_OPTIMIZATION

  float3 res = render(ro, rd, newPos, /*newDir, */ao);

  float3 curPos = newPos;
  float3 origPos = curPos;
  //float3 curDir = newDir;
  //const uint REFL_DEPTH = 0;
  //for (uint i = 0; i < REFL_DEPTH; ++i) {
  //  float3 cur = render(curPos, curDir, newPos, newDir, ao);
  //  
  //  cur *= ao;
  //  res += cur;
  //  curPos = newPos;
  //  curDir = newDir;
  //}
  // Post - proj Z
  float4 viewSpacePos = mul(float4(origPos.xyz, 1.0f), (viewM));
  //depth = camParams.z + camParams.w / (viewSpacePos.z);
  // gamma
  //return float4(pow(res, 1.0 / 2.2), 1);
  return float4(pow(res, gammaExponent), 1);
}