#ifndef _BRIDGE2_HLSL_
#define _BRIDGE2_HLSL_
  #define BASE_LENGTH      30.0
  #define BASE_WIDTH        8.5
  #define BASE_THICKNESS    0.5

  #define BEAM_LENGTH      19.99
  #define BEAM_THICKNESS    0.5
  #define BEAM_WIDTH        0.8
  #define BEAM_POS          1.0

  #define CONN_WIDTH        0.8
  #define CONN_THICKNESS    0.5
  #define CONN_LENGTH       5.0

  #define STRIP_WIDTH       0.2
  #define STRIP_THICKNESS   0.01
  #define STRIP_LENGTH      1.0
  #define BSTRIP_LENGTH     (BASE_LENGTH - 0.01)

  void initScene()
  {
    // TODO: setup those from CPU
    //gSizes[0] = float3(BASE_WIDTH, BASE_THICKNESS, BASE_LENGTH);                // Base
    //gSizes[1] = float3(BEAM_WIDTH + 2.5, BEAM_THICKNESS + 2.5*3, BEAM_LENGTH);  // Beam 1
    //gSizes[2] = float3(BEAM_WIDTH + 2.5, BEAM_THICKNESS + 2.5*3, BEAM_LENGTH);  // Beam 2
    //gSizes[3] = float3(STRIP_WIDTH, STRIP_THICKNESS, BASE_LENGTH);              // Road center strip
    //gSizes[4] = float3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH); 
    //gSizes[5] = float3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH*0.5);                
    //gSizes[6] = float3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH*0.5);                
    //gSizes[7] = float3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH*0.5);                
    //gSizes[8] = float3(BASE_WIDTH/* - fx*/ - BEAM_POS, BEAM_THICKNESS, BEAM_WIDTH*0.5);                

    //gPositions[0] = float3( 0.0, 0.0, 0.0);                                     // Base
    //gPositions[1] = float3(+ (BASE_WIDTH - BEAM_POS) - 2.5, 5.5, 0.0);          // Beam 1
    //gPositions[2] = float3(- (BASE_WIDTH - BEAM_POS) + 2.5, 5.5, 0.0);          // Beam 2
    //gPositions[3] = float3(0, BASE_THICKNESS, 0);                               // Road center strip
    //gPositions[4] = float3(0, 12.5, 0);                                         // Upper horisontal beam
    //gPositions[5] = float3(0, 6.5,  BEAM_LENGTH*0.65);                          // Horisontal beam 1                                     
    //gPositions[6] = float3(0, 9.5,  BEAM_LENGTH*0.45);                          // Horisontal beam 2                                     
    //gPositions[7] = float3(0, 6.5, -BEAM_LENGTH*0.65);                          // Horisontal beam 1
    //gPositions[8] = float3(0, 9.5, -BEAM_LENGTH*0.45);                          // Horisontal beam 2

    //gColors[0] = float3(0.9, 0.1, 0.0);
    //gColors[1] = float3(0.1, 0.9, 0.0);
    //gColors[2] = float3(0.1, 0.0, 0.9);
    //gColors[3] = float3(0.9, 0.9, 0.0);
    //gColors[4] = float3(0.9, 0.0, 0.9);
    //gColors[5] = float3(0.0, 0.9, 0.9);
    //gColors[6] = float3(0.4, 0.9, 0.4);
    //gColors[7] = float3(0.9, 0.4, 0.4);
    //gColors[8] = float3(0.4, 0.4, 0.9);

    // Compute bounding boxes transformation matrix and its inverse
    //for (int i = 0; i < NODE_COUNT; ++i)
    //{
      //gVisible[i] = false;

    //  //float4x4 rot = rotationAxisAngle(normalize(float3(1.0,1.0,0.0)), params.x);
    //  float4x4 tra = translate(gPositions[i]);
    //  //txi = identity();
    //  //txi = tra * rot;
    ////  float4x4 txi = tra; // Unneeded
    //  float4x4 txx = inverse(tra);
    ////  gBoundingBoxes[i].txi = txi; // Unneeded
    //  gBoundingBoxes[i].txx = txx;
    //  gBoundingBoxes[i].rad = float4(gSizes[i], 0);
 //   }
  }

  float2 map(in float3 pos);

  float centerStrips(in float3 pos)
  {
    float  strip_pos_z = pos.z + BASE_LENGTH;
    strip_pos_z = opRep(strip_pos_z, 4) + INF*(pos.z > BASE_LENGTH || pos.z < -BASE_LENGTH);
    //strip_pos_z = pos.z;
    float3 strip_pos = float3(pos.x, pos.y - BASE_THICKNESS, strip_pos_z);
    float strip = sdBox1(strip_pos, float3(STRIP_WIDTH, STRIP_THICKNESS, STRIP_LENGTH));
    return strip;
  }

  float2 mapVisible(in float3 pos)
  {
    float2 dis = float2(INF, -1.0);

#if USE_BB_OPTIMIZATION 
    // Deformations
    float fy = 0.5 - 2.5 * cos(PI * pos.z / 35.0);
    float fx = 0.5 + 2.5 * cos(PI * pos.z / 35.0);

#if 1
    // Base
    [branch] if (gVisible[0])
    {
      float base = sdBox1(pos, float3(BASE_WIDTH, BASE_THICKNESS, BASE_LENGTH));
      dis = opU(dis, float2(base,     0.0));
    }
    // Beam 1
    [branch] if (gVisible[1])
    {
      float3 beam_pos1 = float3(pos.x - (BASE_WIDTH - BEAM_POS) + fx, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.z);
      float beam1 = sdBox1(beam_pos1, float3(BEAM_WIDTH, BEAM_THICKNESS, BEAM_LENGTH));
      dis = opU(dis, float2(beam1,    1.0));
    }
    // Beam 2
    [branch] if (gVisible[2])
    {   
      float3 beam_pos2 = float3(pos.x + (BASE_WIDTH - BEAM_POS) - fx, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.z);
      float beam2 = sdBox1(beam_pos2, float3(BEAM_WIDTH, BEAM_THICKNESS, BEAM_LENGTH));
      dis = opU(dis, float2(beam2,    1.0));
    }
    // Road center strip
    [branch] if (gVisible[3])
    {
      //float3 strip_pos1 = float3(fmod(pos.z * 2, 3), pos.y, pos.x);    
      float strip = centerStrips(pos);
      dis = opU(dis, float2(strip,    1.0));
    }
    // Horisontal upper beam
    [branch] if (gVisible[4])
    {
      float3 beam_hor_up_pos = float3(pos.z, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
      float beam_hor_up = sdBox1(beam_hor_up_pos, float3(BEAM_WIDTH, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
      dis = opU(dis, float2(beam_hor_up, 1.0));
    }
    // Horisontal beam 1
    [branch] if (gVisible[5])
    {
      float3 beam_hor1_pos = float3(pos.z - BEAM_LENGTH*0.65, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
      float beam_hor1 = sdBox1(beam_hor1_pos, float3(BEAM_WIDTH*0.5, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
      dis = opU(dis, float2(beam_hor1, 1.0));
    }
    // Horisontal beam 2
    [branch] if (gVisible[6])
    {
      float3 beam_hor2_pos = float3(pos.z - BEAM_LENGTH*0.45, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
      float beam_hor2 = sdBox1(beam_hor2_pos, float3(BEAM_WIDTH*0.5, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
      dis = opU(dis, float2(beam_hor2, 1.0));
    }
    // Horisontal beam 1
    [branch] if (gVisible[7])
    {
      float3 beam_hor1_pos_ = float3(pos.z + BEAM_LENGTH*0.65, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
      float beam_hor1_ = sdBox1(beam_hor1_pos_, float3(BEAM_WIDTH*0.5, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
      dis = opU(dis, float2(beam_hor1_, 1.0));
    }
    // Horisontal beam 2
    [branch] if (gVisible[8])
    {
      float3 beam_hor2_pos_ = float3(pos.z + BEAM_LENGTH*0.45, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
      float beam_hor2_ = sdBox1(beam_hor2_pos_, float3(BEAM_WIDTH*0.5, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
      dis = opU(dis, float2(beam_hor2_, 1.0));
    }

#elif 1
    // Base
    if (gVisible[0])
    {
      float base = sdBox1(pos, float3(BASE_WIDTH, BASE_THICKNESS, BASE_LENGTH));
      dis = opU(dis, float2(base,     0.0));
    }
    // Beam 1
    if (gVisible[1])
    {
      float3 beam_pos1 = float3(pos.x - (BASE_WIDTH - BEAM_POS) + fx, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.z);
      float beam1 = sdBox1(beam_pos1, float3(BEAM_WIDTH, BEAM_THICKNESS, BEAM_LENGTH));
      dis = opU(dis, float2(beam1,    1.0));
    }
    // Beam 2
    if (gVisible[2])
    {   
      float3 beam_pos2 = float3(pos.x + (BASE_WIDTH - BEAM_POS) - fx, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.z);
      float beam2 = sdBox1(beam_pos2, float3(BEAM_WIDTH, BEAM_THICKNESS, BEAM_LENGTH));
      dis = opU(dis, float2(beam2,    1.0));
    }
    // Road center strip
    if (gVisible[3])
    {
      //float3 strip_pos1 = float3(fmod(pos.z * 2, 3), pos.y, pos.x);    
      float  strip_pos_z = fmod(pos.z * 1, 4) + INF*(pos.z > BASE_LENGTH || pos.z < -BASE_LENGTH);
      float3 strip_pos = float3(pos.x, pos.y - BASE_THICKNESS, strip_pos_z);
      float strip = sdBox1(strip_pos, float3(STRIP_WIDTH, STRIP_THICKNESS, STRIP_LENGTH));
      dis = opU(dis, float2(strip,    1.0));
    }
#else
    // Base
    float base = (!gVisible[0])*INF + sdBox1(pos, float3(BASE_WIDTH, BASE_THICKNESS, BASE_LENGTH));
    dis = opU(dis, float2(base,     0.0));
    // Beam 1
    float3 beam_pos1 = float3(pos.x - (BASE_WIDTH - BEAM_POS) + fx, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.z);
    float beam1 = (!gVisible[1])*INF + sdBox1(beam_pos1, float3(BEAM_WIDTH, BEAM_THICKNESS, BEAM_LENGTH));
    dis = opU(dis, float2(beam1,    1.0));
    // Beam 2
    float3 beam_pos2 = float3(pos.x + (BASE_WIDTH - BEAM_POS) - fx, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.z);
    float beam2 = (!gVisible[2])*INF + sdBox1(beam_pos2, float3(BEAM_WIDTH, BEAM_THICKNESS, BEAM_LENGTH));
    dis = opU(dis, float2(beam2,    1.0));
    // Road center strip
    //float3 strip_pos1 = float3(fmod(pos.z * 2, 3), pos.y, pos.x);    
    float  strip_pos_z = fmod(pos.z * 1, 4) + INF*(pos.z > BASE_LENGTH || pos.z < -BASE_LENGTH);
    float3 strip_pos = float3(pos.x, pos.y - BASE_THICKNESS, strip_pos_z);
    float strip = (!gVisible[3])*INF + sdBox1(strip_pos, float3(STRIP_WIDTH, STRIP_THICKNESS, STRIP_LENGTH));
    dis = opU(dis, float2(strip,    1.0));
#endif

    return float2(dis.x * 0.25, dis.y);

    //return fmod(p, c) - 0.5 * c;
    float3 conn_pos1 = float3(fmod(pos.z * 2, 3), pos.y, pos.x);

    float  bstrip_pos1_x = fmod(pos.x - (BASE_WIDTH - BEAM_POS) * 1.0, BASE_WIDTH * 1.8) + INF*(pos.x > 20.0 || pos.x < -10);
    float3 bstrip_pos1 = float3(bstrip_pos1_x, pos.y - BASE_THICKNESS, pos.z);
    //float3 bstrip_pos2 = float3(pos.x + (BASE_WIDTH - BEAM_POS), pos.y - BASE_THICKNESS, pos.z);

    // Connectors
    //float conn1 = sdBox1(conn_pos1, float3(CONN_WIDTH, CONN_THICKNESS, CONN_LENGTH));
    //dis = opU(dis, float2(conn1,  1.0));
    
    // Road border strips
    float bstrip1 = sdBox1(bstrip_pos1, float3(STRIP_WIDTH, STRIP_THICKNESS, BSTRIP_LENGTH));
    dis = opU(dis, float2(bstrip1,  1.0));
    //float bstrip2 = sdBox1(bstrip_pos2, float3(STRIP_WIDTH, STRIP_THICKNESS, BSTRIP_LENGTH));
    //dis = opU(dis, float2(bstrip2,  1.0));
   
    // 0.25 is actually magic
    return float2(dis.x * 0.25, dis.y); 
#else
    return map(pos);
#endif // USE_BB_OPTIMIZATION
  }
  
#if 0
  float glass(float3 p)
  {	
	  float a = (length(p.xz)-1.0-p.y*.15)*.85;
	  a = max(abs(p.y)-1.0,a);
	  float a2 = (length(p.xz)-0.9-p.y*.15)*.85;
	  a = max(a,-max(-.8-p.y,a2));
	  a = max(a,-length(p+float3(.0,4.0,.0))+3.09);
	  a = a;
	
	  float3 p2 = p; p2.xz*=(1.0-p.y*.15);
	  float angle = atan2(p2.x,p2.z);
	  float mag = length(p2.xz);
	  angle = mod(angle,3.14159*.125)-3.14159*.125*.5;
	  p2.xz = float2(cos(angle),sin(angle))*mag;
	  a = max(a,(-length(p2+float3(-7.0,0.0,0.0))+6.05)*.85);
	
	  return a;
  }

  float2 map(in float3 pos)
  {
    float2 dis = float2(INF, -1.0);

    dis = opU(dis, float2(glass(pos * 0.4), 1.0));

    return float2(dis.x * 0.25, dis.y);
  }

#else
  float2 map(in float3 pos)
  {
    float2 dis = float2(INF, -1.0);

    //float t = params.x * 1.0;
    ////float s = sin(t);

    //float4 q = makeQuat(-y, t);
    //pos = rotateVector(q, pos);
    //pos.y *= -1;
    //
    // Deformations
    float fy = 0.5 - 2.5 * cos(PI * pos.z / 35.0);
    float fx = 0.5 + 2.5 * cos(PI * pos.z / 35.0);

    // Base
    float base = sdBox1(pos, float3(BASE_WIDTH, BASE_THICKNESS, BASE_LENGTH));
    dis = opU(dis, float2(base,     0.0));

    // Beam 1
    float3 beam_pos1 = float3(pos.x - (BASE_WIDTH - BEAM_POS) + fx, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.z);
    float beam1 = sdBox1(beam_pos1, float3(BEAM_WIDTH, BEAM_THICKNESS, BEAM_LENGTH));
    dis = opU(dis, float2(beam1,    1.0));

    // Beam 2    
    float3 beam_pos2 = float3(pos.x + (BASE_WIDTH - BEAM_POS) - fx, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.z);
    float beam2 = sdBox1(beam_pos2, float3(BEAM_WIDTH, BEAM_THICKNESS, BEAM_LENGTH));
    dis = opU(dis, float2(beam2,    1.0));

    // Road center strip
    dis = opU(dis, float2(centerStrips(pos),    1.0));

    // Horisontal upper beam
    float3 beam_hor_up_pos = float3(pos.z, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
    float beam_hor_up = sdBox1(beam_hor_up_pos, float3(BEAM_WIDTH, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
    dis = opU(dis, float2(beam_hor_up, 1.0));

    // Horisontal beam 1
    float3 beam_hor1_pos = float3(pos.z - BEAM_LENGTH*0.65, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
    float beam_hor1 = sdBox1(beam_hor1_pos, float3(BEAM_WIDTH*0.5, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
    dis = opU(dis, float2(beam_hor1, 1.0));

    // Horisontal beam 2
    float3 beam_hor2_pos = float3(pos.z - BEAM_LENGTH*0.45, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
    float beam_hor2 = sdBox1(beam_hor2_pos, float3(BEAM_WIDTH*0.5, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
    dis = opU(dis, float2(beam_hor2, 1.0));

    // Horisontal beam 1
    float3 beam_hor1_pos_ = float3(pos.z + BEAM_LENGTH*0.65, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
    float beam_hor1_ = sdBox1(beam_hor1_pos_, float3(BEAM_WIDTH*0.5, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
    dis = opU(dis, float2(beam_hor1_, 1.0));

    // Horisontal beam 2
    float3 beam_hor2_pos_ = float3(pos.z + BEAM_LENGTH*0.45, -4.5 + /*1.0 - 6.0 + */pos.y + fy * 4.0, pos.x);
    float beam_hor2_ = sdBox1(beam_hor2_pos_, float3(BEAM_WIDTH*0.5, BEAM_THICKNESS, BASE_WIDTH - fx - BEAM_POS));
    dis = opU(dis, float2(beam_hor2_, 1.0));

    return float2(dis.x * 0.25, dis.y);

    ////return fmod(p, c) - 0.5 * c;
    //float3 conn_pos1 = float3(fmod(pos.z * 2, 3), pos.y, pos.x);

    //float  bstrip_pos1_x = fmod(pos.x - (BASE_WIDTH - BEAM_POS) * 1.0, BASE_WIDTH * 1.8) + INF*(pos.x > 20.0 || pos.x < -10);
    //float3 bstrip_pos1 = float3(bstrip_pos1_x, pos.y - BASE_THICKNESS, pos.z);
    ////float3 bstrip_pos2 = float3(pos.x + (BASE_WIDTH - BEAM_POS), pos.y - BASE_THICKNESS, pos.z);
    //
    //// Connectors
    ////float conn1 = sdBox1(conn_pos1, float3(CONN_WIDTH, CONN_THICKNESS, CONN_LENGTH));
    ////dis = opU(dis, float2(conn1,  1.0));
    //
    //// Road border strips
    //float bstrip1 = sdBox1(bstrip_pos1, float3(STRIP_WIDTH, STRIP_THICKNESS, BSTRIP_LENGTH));
    //dis = opU(dis, float2(bstrip1,  1.0));
    ////float bstrip2 = sdBox1(bstrip_pos2, float3(STRIP_WIDTH, STRIP_THICKNESS, BSTRIP_LENGTH));
    ////dis = opU(dis, float2(bstrip2,  1.0));
  }
#endif // 1

#endif // BRIDGE2