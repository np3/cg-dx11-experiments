#ifndef _SD_HLSL_
#define _SD_HLSL_

float LENGTH_FUNC(float x) {
  return rcp(rsqrt(dot(x, x)));
}

float LENGTH_FUNC(float2 x) {
  return rcp(rsqrt(dot(x, x)));
}

float LENGTH_FUNC(float3 x) {
  return rcp(rsqrt(dot(x, x)));
}
// Constants
#define PI  3.1415926535
#define INF 1000000.0

// Axes in 3d space
static const float3 x = float3(1.0, 0.0, 0.0),
                    y = float3(0.0, 1.0, 0.0),
                    z = float3(0.0, 0.0, 1.0);

// Plane SDF
float2 sdPlane(in float3 p)
{
  return float2(p.y, 0.0);
  //return float2(1000, 0.0);
}

// Sphere SDF
float2 sdSphere(in float3 p, in float4 e)
{
  //return float2(length(p) - s, 1.0);
  float3 di = p - e.xyz;
  return float2((float)LENGTH_FUNC(di) - e.w, 1.0);
}

// Box SDF
float2 sdBox(in float3 p, in float3 b)
{
  float3 d = abs(p) - b;
  return float2(min(max(d.x,max(d.y,d.z)), 0.0) + LENGTH_FUNC(max(d,0.0)), 2.0);
}

// Box SDF
float sdBox1(in float3 p, in float3 b)
{
  float3 d = abs(p) - b;
  return min(max(d.x, max(d.y, d.z)), 0.0) + LENGTH_FUNC(max(d, 0.0));
}

// Cone SDF
float sdCone(in float3 p, in float3 c)
{
  float2 q = float2(LENGTH_FUNC(p.xz), p.y);
	return max(max(dot(q, c.xy), p.y), -p.y-c.z);
}

// Capsule SDF
float sdCapsule(float3 p, float3 a, float3 b, float r)
{
	float3 pa = p - a;
	float3 ba = b - a;
	float h = clamp( dot(pa,ba)/dot(ba,ba), 0.0, 1.0 );
	
	return LENGTH_FUNC( pa - ba*h ) - r;
}

// Triangle prism SDF
float sdTriPrism(float3 p, float2 h)
{
  p.xyz = p.zxy;
  float3 q = abs(p);
  return max(q.z-h.y,max(q.x*0.866025+p.y*0.5,-p.y)-h.x*0.5);
}

// Hexagonal prism SDF
float sdHexPrism(float3 p, float2 h)
{
  p.xyz = p.zxy;
  float3 q = abs(p);
  return max(q.z-h.y,max(q.x+q.y*0.57735,q.y*1.1547)-h.x);
}

float length2(float2 p)
{
	return rcp(rsqrt(dot(p, p)));
}

float length6(float2 p)
{
	p = p*p*p; p = p*p;
	return pow(p.x + p.y, 1.0 / 6.0);
}

float length8(float2 p)
{
	p = p*p; p = p*p; p = p*p;
	return pow(p.x + p.y, 1.0 / 8.0);
}

// Cylinder6 SDF
float sdCylinder6(float3 p, float2 h)
{
  return max(length6(p.xz)-h.x, abs(p.y)-h.y);
}

// tesseract (4d hypercube)
// https://www.shadertoy.com/view/4sjGWw
float sdTesseract(float4 p, float4 b)
{
  float4 d = abs(p) - b;
  return min(max(d.x,max(d.y,max(d.z,d.w))),0.0) + length(max(d,0.0));
}

float udRoundTesseract(float4 p, float4 b, float r)
{
  return length(max(abs(p)-b,0.0))-r;
}

// Cylinder SDF
float sdCylinder(in float3 p, in float2 h)
{
  return max(length(p.xz)-h.x, abs(p.y)-h.y);
}

// Constant width cylinder 3 SDF
float sdConstantWidthCylinder3(in float3 p, in float2 h)
{
  //return max(length(p.xy) - h.x,
  //           abs(p.z)     - h.y);

  //float an = 2.0 / 3.0 * PI /* angle of 120 degrees */;
  //float c = cos(an), s = sin(an);

  //float c = -0.5, s = 0.8660254;
  //float3 o1 = float3(1.0, 0.0, 0.0); 
  //float3 o2 = float3(o1.x*c - o1.z*s, 0.0,
  //                   o1.x*s + o1.z*c);
  //float3 o3 = float3(o2.x*c - o2.z*s, 0.0,
  //                   o2.x*s + o2.z*c);

  float3 o1 = float3( 1.0, 0.0, 0.0); 
  float3 o2 = float3(-0.5, 0.0,  0.866025);
  float3 o3 = float3(-0.5, 0.0, -0.866025);

  float c1 = sdCylinder(p + o1, h);
  float c2 = sdCylinder(p + o2, h);
  float c3 = sdCylinder(p + o3, h);
  
  return max(max(c1, c2), c3);
  //return min(min(c1, c2), c3);
}

// Constant width cylinder 5 SDF
float sdConstantWidthCylinder5(in float3 p, in float2 h)
{
  float an = 2.0 / 5.0 * PI;
  float c = cos(an), s = sin(an);
  float3 o1 = float3(1.0, 0.0, 0.0); 
  float3 o2 = float3(o1.x*c - o1.z*s, 0.0,
                     o1.x*s + o1.z*c);
  float3 o3 = float3(o2.x*c - o2.z*s, 0.0,
                     o2.x*s + o2.z*c);
  float3 o4 = float3(o3.x*c - o3.z*s, 0.0,
                     o3.x*s + o3.z*c);
  float3 o5 = float3(o4.x*c - o4.z*s, 0.0,
                     o4.x*s + o4.z*c);
  float c1 = sdCylinder(p + o1, h);
  float c2 = sdCylinder(p + o2, h);
  float c3 = sdCylinder(p + o3, h);
  float c4 = sdCylinder(p + o4, h);
  float c5 = sdCylinder(p + o5, h);
  
  return max(max(max(max(c1, c2), c3), c4), c5);
  //return min(min(c1, c2), c3);
}

// Sphere SDF
float sdSphere1(in float3 p, in float s)
{
  return length(p) - s;
}

// Constant width 3d SDF
float sdConstantWidth3d(in float3 p, in float h)
{
  float3 o1 = float3( 1, 1, 1),
         o2 = float3( 1,-1,-1),
         o3 = float3(-1, 1,-1),
         o4 = float3(-1,-1, 1);

  float s1 = sdSphere1(p + o1, h);
  float s2 = sdSphere1(p + o2, h);
  float s3 = sdSphere1(p + o3, h);
  float s4 = sdSphere1(p + o4, h);
  return max(max(max(s1, s2), s3), s4);

  //return min(min(s1, s2), s3);

  // this optimization gives nothing
  //float s1 = length(p + o1);
  //float s2 = length(p + o2);
  //float s3 = length(p + o3);
  //float s4 = length(p + o4);
  //
  //return max(max(max(s1, s2), s3), s4) - h;
}

// Exponential smooth min (k = 32);
float smin1(float a, float b, float k)
{
  float res = exp(-k * a) + exp(-k * b);
  return -log(res) / k;
}
	
// Polynomial smooth min (k = 0.1);
float smin2(float a, float b, float k)
{
  float h = saturate(0.5 + 0.5 * (b - a) / k);
  return lerp(b, a, h) - k * h * (1.0 - h);
}
	
// Power smooth min (k = 8);
float smin3(float a, float b, float k)
{
  a = pow(a, k);
  b = pow(b, k);
  return pow((a * b) / (a + b), 1.0 / k);
}

// Non-smooth minimum
//float min(float a, float b)
//{
//  return (a < b) ? a : b;
//}

float2 opU(float2 d1, float2 d2)
{
  //float result = min(d1.x, d2.x);
  return d1.x < d2.x ? d1 : d2;
  //float result = smin1(d1.x, d2.x, 32);
  //float result = smin2(d1.x, d2.x, 0.1);
  //float result = smin3(d1.x, d2.x, 8);

  //return float2(result, smin1(d1.y, d2.y, 32));

  // FIXME
  /*if (result == d1.x)
    return float2(result, d1.y);
  else
    return float2(result, d2.y);*/
}

float2 opS(float2 d1, float2 d2)
{
  float result = max(-d2.x, d1.x);
  return -d2.x > d1.x ? float2(-d2.x, d2.y) : d1;
  /*
  // FIXME
  if (result == d1.x)
    return float2(result, d1.y);
  else
    return float2(result, d2.y);*/
}

float2 opI(float2 d1, float2 d2)
{
  float result = max(d1.x, d2.x);

  // FIXME
  if (result == d1.x)
    return float2(result, d1.y);
  else
    return float2(result, d2.y);
}

float3 opRep(float3 p, float3 c)
{
  return fmod(p, c) - 0.5 * c;
}

float opRep(float p, float c)
{
  return fmod(p, c) - 0.5 * c;
}

float3 opTwist(float3 p)
{
  float c = cos(10.0 * p.y + 10.0);
  float s = sin(10.0 * p.y + 10.0);
  float2x2 m = float2x2(c, -s, s, c);
  return float3(mul(m, p.xz), p.y);
}

float4x4 rotationAxisAngle(float3 v, float angle)
{
  float s = sin(angle);
  float c = cos(angle);
  float ic = 1.0 - c;

  return float4x4(v.x*v.x*ic + c,     v.y*v.x*ic - s*v.z, v.z*v.x*ic + s*v.y, 0.0,
                  v.x*v.y*ic + s*v.z, v.y*v.y*ic + c,     v.z*v.y*ic - s*v.x, 0.0,
                  v.x*v.z*ic - s*v.y, v.y*v.z*ic + s*v.x, v.z*v.z*ic + c,     0.0,
			            0.0,                0.0,                0.0,                1.0);
}

float4x4 identity()
{
  return float4x4(1.0, 0.0, 0.0, 0.0,
                  0.0, 1.0, 0.0, 0.0,
                  0.0, 0.0, 1.0, 0.0,
                  0.0, 0.0, 0.0, 1.0);
}

float mod(float x, float y)
{
  return x - y * floor(x / y);
}
             
// https://www.shadertoy.com/view/Mds3z2
// should make another bridge, this one just for example
float bridge(in float3 pos)
{
  float mindist = 10000000.0;

  float f = 0.5 - 0.5 * cos(PI * pos.z / 20.0);

  float3 xpos = float3(pos.x, 1.0 - 6.0 + pos.y + f * 4.0, pos.z);

  //float g = 0.5 + 0.5 * xpos.x / 4.0;
  //g = 1.0 - (smoothstep(0.15, 0.25, g) - smoothstep(0.75, 0.85, g));
  //float3 xpos5 = float3(xpos.x, xpos.y + 10.0 * f * f * g, xpos.z);

  float dis = sdBox(xpos, float3(3.0, 0.3, 10.0)).x;

  mindist = min(dis, mindist);

  //float f = 0.5-0.5*cos(3.14159*pos.z/20.0);

  //float3 xpos = float3( pos.x, 1.0-6.0+pos.y + f*4.0, pos.z );

  //float g = 0.5+0.5*xpos.x/4.0;
  //g = 1.0 - (smoothstep( 0.15, 0.25, g )-smoothstep( 0.75, 0.85, g ));
  //float3 xpos5 = float3( xpos.x, xpos.y + 10.0*f*f*g, xpos.z );

  //float dis = sdBox(xpos5, float3(4.0,0.5 + 10.0*f*f*g,20.0)  ).x;

  //mindist = min( dis, mindist );

  //dis = sdBox( xpos, float3(4.2,0.1,20.0) ).x;
  //mindist = min( dis, mindist );
	
  //float3 sxpos = float3( abs(xpos.x), xpos.y, xpos.z );
  //dis = sdBox( sxpos-float3(4.0-0.4,1.5,0.0), float3(0.4,0.2,20.0) ).x;
  //mindist = min( dis, mindist );

  //if( abs(xpos.z)<20.0 )
  //{
  //  int cid = int(floor( xpos.z ));
  //  if( cid == 0 || cid==14 || cid==-14 )
  //  {
  //    float3 xpos2 = float3( abs(xpos.x)-3.4, xpos.y-1.0, mod(1000.0+xpos.z,1.0)-0.5 );

  //    dis = sdBox( xpos2, float3(0.8,1.0,0.45) ).x;
  //    mindist = min( dis, mindist );
		//	
  //    dis = sdBox( xpos2+float3(-0.8,0.0,0.0), float3(0.15,0.9,0.35) ).x;
  //    mindist = max( mindist, -dis );

  //    dis = sdSphere( xpos2, float4(0.0, 1.3, 0.0, 0.35 ) ).x;
  //    mindist = min( dis, mindist );
  //  }
  //  else
  //  {
  //    float3 xpos2 = float3( abs(xpos.x)-(4.0-0.2), xpos.y-0.8, mod(1000.0+xpos.z,1.0)-0.5 );
  //    float3 xposc = float3( length( xpos2.xz ), xpos2.y, 0.0 );

  //    float mo = 0.8 + 0.2*cos(2.0*6.2831*xpos2.y);

  //    float ma = cos(4.0*atan2(xpos2.x,xpos2.z));
  //    mo -= 0.1*(1.0-ma*ma);

  //    dis = sdBox( xposc, float3(0.2*mo,0.5,0.0)  ).x;
  //    mindist = min( dis, mindist );
  //  }
  //}

  return 0.25*mindist;
}

float4 makeQuat(float3 v, float alpha)
{
  float theta = 0.5f * alpha;
  float cs = cos(theta), sn = sin(theta);
  return float4(cs, sn * v.x, sn * v.y, sn * v.z);
}

float4 multQuat(float4 q1, float4 q2)
{
  return float4(
    q1.w * q2.x + q1.x * q2.w + q1.z * q2.y - q1.y * q2.z,
    q1.w * q2.y + q1.y * q2.w + q1.x * q2.z - q1.z * q2.x,
    q1.w * q2.z + q1.z * q2.w + q1.y * q2.x - q1.x * q2.y,
    q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z);
}

float3 rotateVector(float4 quat, float3 vec)
{
  return vec + 2.0 * cross(cross(vec, quat.xyz) + quat.w * vec, quat.xyz);
}

// axis angle rotation (not tested yet)
float3x3 rotAA(in float3 v, in float angle)
{
	float  c = cos(angle);
	float3 s = v * sin(angle);
	return float3x3(v.xxx*v,v.yyy*v,v.zzz*v)*(1.0-c)+
		     float3x3(c,-s.z,s.y,s.z,c,-s.x,-s.y,s.x,c);
}

float3 scaleVector(float3 scale, float3 vec)
{
  // TODO: optimize
  return float3(vec.x * scale.x, vec.y * scale.y, vec.z * scale.z);
}

// This acts wierd dunno why
float3 opCheapBend(float3 p)
{
    float c = cos(0.1 * p.y);
    float s = sin(0.1 * p.y);
    float2x2 m = float2x2(c, -s, s, c);
    float3   q = float3(mul(m, p.xy), p.z);
    //return primitive(q);
    return q;
}

// We use column-major by-default
//#pragma pack_matrix ( row_major )

float4x4 translate(float x, float y, float z)
{
  return float4x4(1.0, 0.0, 0.0, 0.0,
                  0.0, 1.0, 0.0, 0.0,
                  0.0, 0.0, 1.0, 0.0,
                  x,   y,   z,   1.0);
}

float4x4 translate(in float3 v)
{
  return float4x4(1.0, 0.0, 0.0, 0.0,
                  0.0, 1.0, 0.0, 0.0,
                  0.0, 0.0, 1.0, 0.0,
                  v.x, v.y, v.z, 1.0);
}

float4x4 inverse(in float4x4 m)
{
  // Reference: http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche53.html
  return /*transpose*/(float4x4(m[0][0], m[1][0], m[2][0], 0.0,
                                m[0][1], m[1][1], m[2][1], 0.0,
                                m[0][2], m[1][2], m[2][2], 0.0,
                                -dot(m[0].xyz,m[3].xyz),
                                -dot(m[1].xyz,m[3].xyz),
                                -dot(m[2].xyz,m[3].xyz),
                                1.0));
}

#endif