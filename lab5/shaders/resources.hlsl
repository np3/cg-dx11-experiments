#ifndef _RESOURCES_HLSL_
#define _RESOURCES_HLSL_

cbuffer frameParams : register(b0)
{
  float4   camPos;
  float4   camTarget;
  float4   camParams; // x: xScale factor, y: yScale factor, z: zFar / (zFar - zNear), w: z * zNear
  float4x4 viewM;
  float4   params;    // x: time in seconds
};

// Bounding-box declaration
struct BoundingBox
{
  float4x4 txx;
  //float4x4 txi;
  float4   rad;
};

// Array of bounding boxes for scene nodes
//static BoundingBox _gBoundingBoxes[NODE_COUNT];
// Node positions
static float3 gPositions[MAX_NODES_COUNT];
// Node positions
static float3 gSizes[MAX_NODES_COUNT];
// Colors for bounding-boxes visualization
//static float3 _gColors[NODE_COUNT];

cbuffer boxesParams : register(b1)
{
  BoundingBox gBoundingBoxes[MAX_NODES_COUNT];
  float4 gColors[MAX_NODES_COUNT];
};

#endif