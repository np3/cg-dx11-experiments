//#include "common.hlsl"
#define MAX_LIGHTS 3
#define MAX_TEXTURES 7
#define MAX_SAMPLERS 2 // 0 - point + mip-map(for vs), 1 - bilinear + mip-map(for ps)

cbuffer perFrame : register(b0)
{
    matrix ViewProj;
    float4   eyePos;
}

cbuffer perObj : register(b1)
{
   matrix World;
   matrix WorldInv;
}

struct LIGHT_DATA
{
    float4 posDir; // w - parallel/non-parallel
    float4 spotDir;
    float4 diffuse;
    float4 specular;
    float4 params; // x, y - spot angular factors, z - falloff
    float4 att;
};

cbuffer lightBuffer : register(b2)
{
    LIGHT_DATA litData[MAX_LIGHTS];
}

SamplerState SAMPLERS[MAX_SAMPLERS] : register(s0);
Texture2D TEXTURES[MAX_TEXTURES] : register(t0);

struct VS_INPUT
{
    float3 Pos   : POSITION;
    float3 Norm  : NORMAL;
    float4 Color : COLOR0;
    float2 UV    : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 Pos  : SV_POSITION;
    float3 Norm : NORMAL;
    float4 Col  : COLOR0;
    float2 UV   : TEXCOORD0;
    float4 WPos : TEXCOORD1; // w - view-space Z
};

float calcAttenuation(in float3 L, in float4 attFactors)
{
   float dist = length(L);
   float dist_2 = dist * dist;
   float d4 = dist_2 * dist_2;
   float inv_r4 = attFactors.w;
   float k = saturate(1 - inv_r4 * d4);

   return (k * k) / (attFactors.x + attFactors.y * dist + attFactors.z * dist_2);
}

float calcSpotFactor(in float3 L, in float3 spotDir, in float3 spotParams)
{
   float spotFactor = dot(-L, spotDir);
   spotFactor = saturate(spotFactor * spotParams.x + spotParams.y);
   spotFactor = pow(spotFactor, spotParams.z);
   return spotFactor;
}

void calcLighting(in VS_OUTPUT input, in float3 _wnorm, inout float3 diff, inout float3 spec, in float3 V, in float2 specMul)
{
   diff = 0;
   spec = 0;
   float3 wnorm = _wnorm;
   [unroll]
   for (uint i = 0; i < MAX_LIGHTS; ++i) {
      float3 L = litData[i].posDir.xyz - input.WPos.xyz * litData[i].posDir.w;
      float att = calcAttenuation(L, litData[i].att);
      L = normalize(L);
      float spotFactor = calcSpotFactor(L, litData[i].spotDir.xyz, litData[i].params.xyz);
      att *= spotFactor;
      float NdotL = saturate(dot(wnorm, L));
      diff += att * NdotL * litData[i].diffuse.rgb;
      float3 H = normalize(L + V); // half - vector
      float NdotH = saturate(dot(wnorm, H));
      NdotH = pow(NdotH, specMul.x) * specMul.y;
      spec += att * NdotH * litData[i].specular.rgb;
   }
}

VS_OUTPUT VS(VS_INPUT input)
{
    VS_OUTPUT output = (VS_OUTPUT)0;
    float4 pos = float4(input.Pos, 1.0);
    float2 lC = float2(input.Pos.x, input.Pos.z) * 8;

    float4 WPos = mul(pos, World);
    output.Pos = mul(WPos, ViewProj);
    output.Norm = mul(float4(input.Norm, 1.0), WorldInv).xyz;
    output.Col = input.Color;
    output.UV = input.UV;
    output.WPos = float4(WPos.xyz, output.Pos.w);
    return output;
}

float2 dHdxy_fwd(Texture2D height_tex, SamplerState sampler_in, float2 texST)
{
   float2 dSTdx = ddx_fine(texST);
   float2 dSTdy = ddy_fine(texST);

   float Hll = height_tex.Sample(sampler_in, texST).x;
   float dBx = height_tex.Sample(sampler_in, texST+dSTdx).x - Hll;
   float dBy = height_tex.Sample(sampler_in, texST+dSTdy).x - Hll;
   
   return float2(dBx, dBy);
}

float2 dHdxy_cen(Texture2D height_tex, SamplerState sampler_in, float2 texST)
{
   float2 dSTdx = ddx_fine(texST);
   float2 dSTdy = ddy_fine(texST);

   float dBx = height_tex.Sample(sampler_in, texST+0.5*dSTdx).x - height_tex.Sample(sampler_in, texST-0.5*dSTdx).x;
   float dBy = height_tex.Sample(sampler_in, texST+0.5*dSTdy).x - height_tex.Sample(sampler_in, texST-0.5*dSTdy).x;

   return float2(dBx, dBy);
}

float2 dHdxy_bicubic_toplevel(Texture2D height_tex, SamplerState sampler_in, float2 texST);
// the faceted look that results from texture magnification of a height map, used for bump mapping,
// can be solved by using higher order filtering such as a bicubic evaluation
float2 dHdxy_bicubic2fwd(Texture2D height_tex, SamplerState sampler_in, float2 texST)
{
	float fMix = saturate(1-height_tex.CalculateLevelOfDetailUnclamped(sampler_in, texST));
	float2 dHdxy = dHdxy_fwd(height_tex, sampler_in, texST);
	if(fMix==0.0) return dHdxy;
	else
	{
		// the derivative of the bicubic sampling of level 0
		float2 dHdxy_bic = dHdxy_bicubic_toplevel(height_tex, sampler_in, texST);
		return dHdxy*(1-fMix) + dHdxy_bic*fMix;
	}
}

// the faceted look that results from texture magnification of a height map, used for bump mapping,
// can be solved by using higher order filtering such as a bicubic evaluation
float2 dHdxy_bicubic2cen(Texture2D height_tex, SamplerState sampler_in, float2 texST)
{
	float fMix = saturate(1-height_tex.CalculateLevelOfDetailUnclamped(sampler_in, texST));
	float2 dHdxy = dHdxy_cen(height_tex, sampler_in, texST);
	if(fMix==0.0) return dHdxy;
	else
	{
		// the derivative of the bicubic sampling of level 0
		float2 dHdxy_bic = dHdxy_bicubic_toplevel(height_tex, sampler_in, texST);
		return dHdxy*(1-fMix) + dHdxy_bic*fMix;
	}
}

float2 dHdxy_bicubic_toplevel(Texture2D height_tex, SamplerState sampler_in, float2 texST)
{
	uint2 vDim;
	height_tex.GetDimensions(vDim.x, vDim.y);

	float2 f2TexCoord = vDim*texST-float2(0.5,0.5);
	float2 f2FlTexCoord = floor(f2TexCoord);
	float2 t = saturate(f2TexCoord - f2FlTexCoord);

	const float4 vSamplesUL = height_tex.Gather( sampler_in, (f2FlTexCoord+float2(-1.0,-1.0) + float2(0.5,0.5))/vDim );
	const float4 vSamplesUR = height_tex.Gather( sampler_in, (f2FlTexCoord+float2(1.0,-1.0) + float2(0.5,0.5))/vDim );
	const float4 vSamplesLL = height_tex.Gather( sampler_in, (f2FlTexCoord+float2(-1.0,1.0) + float2(0.5,0.5))/vDim );
	const float4 vSamplesLR = height_tex.Gather( sampler_in, (f2FlTexCoord+float2(1.0,1.0) + float2(0.5,0.5))/vDim );
	
	// scanlines are given in the vertical direction. For instance float4(UL.wz, UR.wz) represents a scanline
	float4x4 H = {  vSamplesUL.w, vSamplesUL.x, vSamplesLL.w, vSamplesLL.x,
					vSamplesUL.z, vSamplesUL.y, vSamplesLL.z, vSamplesLL.y,
					vSamplesUR.w, vSamplesUR.x, vSamplesLR.w, vSamplesLR.x,
					vSamplesUR.z, vSamplesUR.y, vSamplesLR.z, vSamplesLR.y };

	float x = t.x, y = t.y;
	float x2 = x * x, x3 = x2 * x;
	float y2 = y * y, y3 = y2 * y;

	float4 X = float4(-0.5*(x3+x)+x2,		1.5*x3-2.5*x2+1,		-1.5*x3+2*x2+0.5*x,		0.5*(x3-x2));
	float4 Y = float4(-0.5*(y3+y)+y2,		1.5*y3-2.5*y2+1,		-1.5*y3+2*y2+0.5*y,		0.5*(y3-y2));
	float4 dX = float4(-1.5*x2+2*x-0.5,		4.5*x2-5*x,				-4.5*x2+4*x+0.5,		1.5*x2-x);
	float4 dY = float4(-1.5*y2+2*y-0.5,		4.5*y2-5*y,				-4.5*y2+4*y+0.5,		1.5*y2-y);
			
	// We scale by vDim to obtain the derivative w.r.t. the normalized texture domain [0;1]^2
	float2 dHdST = vDim * float2( dot(Y, mul(dX, H)), dot(dY, mul(X, H)) );


	// domain change to screen space using the chain rule.
	float2 dSTdx = ddx_fine(texST);
	float2 dSTdy = ddy_fine(texST);
	return float2( dot(dHdST.xy, dSTdx.xy), dot(dHdST.xy, dSTdy.xy) );
}


// Calculate the surface normal using screen-space partial derivatives of the height field
float3 CalculateSurfaceNormal(Texture2D hmap, float3 surf_pos, float3 surf_norm, float2 uv)
{
    float2 dHdxy = dHdxy_bicubic2cen(hmap, SAMPLERS[1], uv) * 25;
 
    float3 vSigmaX = ddx_fine( surf_pos );
    float3 vSigmaY = ddy_fine( surf_pos );
    float3 vN = surf_norm;		// normalized

    float3 R1 = cross(vSigmaY,vN);
    float3 R2 = cross(vN,vSigmaX);

    float fDet = dot(vSigmaX, R1);

    float3 vGrad = sign(fDet) * ( dHdxy.x * R1 + dHdxy.y * R2 );
    return normalize( abs(fDet)*surf_norm - vGrad);
}


float4 PS(VS_OUTPUT input) : SV_Target
{
    return float4(1, 0, 0, 0);
    float3 litDiff = (float3)0;
    float3 litSpec = (float3)0;
    float noise = TEXTURES[6].Sample(SAMPLERS[1], input.UV * 2).r;
    float noise2 = TEXTURES[6].Sample(SAMPLERS[1], input.UV * 1).r;
    float oHeight = TEXTURES[0].Sample(SAMPLERS[1], input.UV) + 0.05 * noise;
    float height = oHeight;
    float4 texGrass = TEXTURES[1].Sample(SAMPLERS[1], input.UV * 2);
    float4 texStone = TEXTURES[2].Sample(SAMPLERS[1], input.UV * 4);
    float4 texSnow = TEXTURES[3].Sample(SAMPLERS[1], input.UV * 25);
    float2 specGrass = float2(1.0, 0);
    float2 specStone = float2(1.0, 0);
    float2 specSnow = float2(8192 * 4, 1);
    float2 specPwr = specGrass;
    float4 texDiff = lerp(texGrass, texStone, saturate(4 * height));
    specPwr = lerp(specPwr, specStone, saturate(4 * height));
    specPwr = lerp(specPwr, specSnow, saturate(4 * height - 1));
    texDiff = lerp(texDiff, texSnow, saturate(4 * height - 1));

    input.Norm = normalize(input.Norm);
    float3 V = normalize(eyePos.xyz - input.WPos.xyz);
    calcLighting(input, input.Norm, litDiff, litSpec, V, specPwr);
    
    return float4(float3(1, 0, 0) * litDiff.rgb + litSpec.rgb, input.Col.a);
}