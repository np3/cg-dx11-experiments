#ifndef _COLUMNS_HLSL_
#define _COLUMNS_HLSL_

#define PLANE_SIZE          40.0
#define PLANE_THICKNESS     1.0

#define COLUMN_H           20.0
#define COLUMN_R            0.5
#define COLUMNS_N           10

#define SPHERE_R            10.0
#define SPHERE_POS          float3(0.0, 20.0, 0.0)

  void initScene()
  {
  //  gSizes[0] = float3(PLANE_SIZE, PLANE_THICKNESS, PLANE_SIZE);
  //  gSizes[1] = float3(PLANE_SIZE, COLUMN_H, COLUMN_R);
  //  //gSizes[1] = float3(PLANE_SIZE, PLANE_SIZE, PLANE_SIZE);

  //  gPositions[0] = float3(0.0, 0.0, 0.0);
  //  gPositions[1] = float3(0.0, COLUMN_H, 0.0);

  //  gColors[0] = float3(0.9, 0.1, 0.0);
  //  gColors[1] = float3(0.1, 0.0, 0.9);

  //  // Compute bounding boxes transformation matrix and its inverse
  //  for (int i = 0; i < NODE_COUNT; ++i)
  //  {
  //    gVisible[i] = false;

  //    //float4x4 rot = rotationAxisAngle(normalize(float3(1.0,1.0,0.0)), params.x);
  //    float4x4 tra = translate(gPositions[i]);
  //    //txi = identity();
  //    //txi = tra * rot;
  //    float4x4 txi = tra; // Unneeded
  //    float4x4 txx = inverse(txi);
  //    gBoundingBoxes[i].txi = txi; // Unneeded
  //    gBoundingBoxes[i].txx = txx;
  //    gBoundingBoxes[i].rad = gSizes[i];
  //  }
  }

  float2 map(in float3 pos);

  float2 mapVisible(in float3 pos)
  {
    float2 dis = float2(INF, -1.0);

#if USE_BB_OPTIMIZATION 
    // Plane
    //[branch] if (gVisible[0])
    {
      float d = sdBox1(pos, float3(PLANE_SIZE, PLANE_THICKNESS, PLANE_SIZE));
      dis = opU(dis, float2(d, 1.0));
    }

    // Columns
    [branch] if (gVisible[1])
    {
      float l = PLANE_SIZE * 2;
      float px = pos.x + PLANE_SIZE;
      px = opRep(px, 30 * COLUMN_R) + INF*(px > l || px < -l);
      //float px = pos.x;
      float3 p = float3(px, pos.y - COLUMN_H, pos.z);
      float d = sdCylinder(p, float2(COLUMN_R, COLUMN_H));
      dis = opU(dis, float2(d, 1.0));
    }

#else
    return map(pos);
#endif // USE_BB_OPTIMIZATION 

    return float2(dis.x * 0.25, dis.y);
  }


  static bool stop = false;

  float2 map(in float3 pos)
  {
    float2 dis = float2(INF, -1.0);

    // Plane
    {
      float d = sdBox1(pos, float3(PLANE_SIZE, PLANE_THICKNESS, PLANE_SIZE));
      dis = opU(dis, float2(d, 1.0));
    }                                

    {
      float3 p = pos - SPHERE_POS + y * params.x*2;
      float3 p_box = pos - y * params.x*2;
      float d_box = sdBox1(p_box, float3(PLANE_SIZE, PLANE_THICKNESS, PLANE_SIZE));
      if (d_box <= SPHERE_R)
      {
        stop = true;
      }
      if (stop)
      {
        p = pos - SPHERE_POS;
      }
      float d = sdSphere1(p, SPHERE_R);
      dis = opU(dis, float2(d, 1.0));
    }

    // Columns
    //{
    //  float l = PLANE_SIZE * 2;
    //  float px = pos.x + PLANE_SIZE;
    //  px = opRep(px, 30 * COLUMN_R) + INF*(px > l || px < -l);
    //  //float px = pos.x;
    //  float3 p = float3(px, pos.y - COLUMN_H, pos.z);
    //  float d = sdCylinder(p, float2(COLUMN_R, COLUMN_H));
    //  dis = opU(dis, float2(d, 1.0));
    //}

    return float2(dis.x * 0.25, dis.y);
  }

#endif // COLUMNS