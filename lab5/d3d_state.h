#ifndef _D3D_STATE_H_
#define _D3D_STATE_H_

struct d3dSTATE_DESC
{
   enum CULL_MODE
   {
      NONE,
      BACK,
      FRONT
   }; 
   CULL_MODE cullMode;
   bool zWrite;
   enum ZTEST_FUNC {
      LESS_EQUAL,
      LESS,
      ALWAYS,
      GREATER,
      GREATER_EQUAL
   };
   ZTEST_FUNC zFunc;
   bool zTest;
   enum FILL_MODE
   {
      SOLID,
      WIREFRAME
   };
   FILL_MODE fillMode;
   enum {
     S_NONE = 0,
     S_SET = 1,
     S_CHECK = 2
   } stencilMode; // TMP

   d3dSTATE_DESC() : cullMode(BACK), zWrite(true), zFunc(LESS_EQUAL), zTest(true), fillMode(SOLID), stencilMode(S_NONE) {}
   unsigned int GetCode() const {
      unsigned int val = 0;

      val |= (int)cullMode; // 2 bits
      val |= ((int)zWrite) << 2; // 3 bits
      val |= ((int)zFunc) << 3; // 6 bits
      val |= ((int)zTest) << 6; // 7 bits
      val |= ((int)fillMode) << 7; // 8 bits
      val |= ((int)stencilMode) << 8; // 10 bits
      return val;
   }
};

#endif