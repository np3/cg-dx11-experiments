#pragma once
#ifndef _SCN_PRIM_H_
#define _SCN_PRIM_H_
#include <d3d11.h>
#include "scn_transform.h"
#include <list>
#include "d3d_state.h"

class d3dDRIVER;
class d3dBUFFER;
class d3dTEX;

struct scnVERTEX
{
   D3DXVECTOR3 position;
   D3DXVECTOR3 normal;
   DWORD color;
   float U, V;
};

struct scnMATERIAL
{
   static const int MAX_TEX = 7;
   d3dTEX* textures[MAX_TEX];
   float tilingU, tilingV;
   DWORD tint;
   float glossScale, glossBias;
   scnMATERIAL() { 
      memset(textures, 0, MAX_TEX * sizeof(d3dTEX*)); 
      tilingU = tilingV = 1.0f;
      tint = 0xFFFFFFFF;
      glossBias = 0.001f;
      glossScale = 0.0f;
   }
   void SetTex(int slot, d3dTEX* pTex) { textures[slot] = pTex; }
   void Setup(d3dDRIVER *drv);
};

class scnPRIM
{
private:
   d3dBUFFER *pVB;
   d3dBUFFER *pIB;
   scnVERTEX *pVert;
   USHORT *pInd;
   D3D11_PRIMITIVE_TOPOLOGY topology;
   int vertNum, indNum;
   scnTRANSFORM local;
   DWORD color;
   d3dSTATE_DESC primDesc;
   UINT d3dStateID;
   scnMATERIAL mtl;
public:
   scnPRIM(DWORD col = 0xFFFF0000);
   ~scnPRIM();
   void Init(d3dDRIVER *driver);
   void Render(d3dDRIVER *driver);
   void Release();
   scnTRANSFORM& LocalTransform() { return local; }
   d3dSTATE_DESC& GetPrimitiveDesc() { return primDesc; }
   void FitDesc(d3dDRIVER *driver);
   void CreatePlane();
   void CreateGrid(int sizeU, int sizeV);
   void CreateBox();
   void CreateSphere(float radius);
   void CreateFullScreenQuad();
   void CreateCyl(float radius);
   void SetColor(DWORD col) { color = col; }
   void SetMtl(const scnMATERIAL &mat) { mtl = mat; }
};

class scnHIERARCHY
{
public:
   struct NODE {
      NODE() : ID(-1), prim(nullptr), pNext(nullptr), pChild(nullptr), pParent(nullptr) {}
      void SetGeom(int IDKey, scnPRIM *pPrim) { ID = IDKey, prim = pPrim; }
      NODE* AddChild();
      int ID;
      scnPRIM *prim;
      scnTRANSFORM trans;
      NODE *pNext;
      NODE *pChild;
      NODE *pParent;
   };
   NODE* GetRoot() { return root; }
   template<class P>
   void Apply(P &pred) {
       scnHIERARCHY_ITER iter(this);

       for (iter.Begin(); !iter.IsDone(); ++iter) {
         pred(*iter);
         if (iter->pParent) {
          iter->trans *= iter->pParent->trans;
         }
       }
   }
   scnHIERARCHY();
   ~scnHIERARCHY();
private:
   NODE *root;
};

class scnHIERARCHY_ITER
{
public:
   scnHIERARCHY_ITER(scnHIERARCHY *hier) : pHier(hier) {}
   void Begin();
   scnHIERARCHY::NODE* operator*() { return pCur; }
   scnHIERARCHY::NODE* operator->() { return pCur; }
   void  operator++();
   void  operator++(int);
   bool IsDone() { return pCur == nullptr && stack.size() == 0; }
private:
   std::list<scnHIERARCHY::NODE*> stack;
   scnHIERARCHY::NODE *pCur;
   scnHIERARCHY *pHier;
};
#endif