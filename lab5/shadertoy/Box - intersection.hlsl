// https://www.shadertoy.com/view/ld23DV

// Created by inigo quilez - iq/2014
// License Creative Commons Attribution-NonCommercial-ShareAlike 3.0 Unported License.

// Ray-Box intersection, by convertig the ray to the local space of the box.
//
// If this was used to raytace many equally orietned boxes (saym you are traversing a BVH,
// then the transformations in line 17 and 18, and the computations of m and n could be
// precomputed for the whole set of cubes. You probably wouldn't need line 33 and 36 either.


// Ray-Box intersection, by convertig the ray to the local space of the box.
// Returns: t and normal
float4 iBox(in float3 ro, in float3 rd, in float4x4 txx, in float4x4 txi, in float3 rad) 
{
  // convert from ray to box space
  float3 rdd = (txx * float4(rd, 0.0)).xyz;
  float3 roo = (txx * float4(ro, 1.0)).xyz;

  // ray-box intersection in box space
  float3 m = 1.0 / rdd;
  float3 n = m * roo;
  float3 k = abs(m) * rad;

  float3 t1 = -n - k;
  float3 t2 = -n + k;

  float tN = max(max(t1.x, t1.y), t1.z);
  float tF = min(min(t2.x, t2.y), t2.z);

  if (tN > tF || tF < 0.0) return float4(-1.0);

  float3 nor = -sign(rdd) * step(t1.yzx, t1.xyz) * step(t1.zxy, t1.xyz);

  // convert to ray space
  nor = (txi * float4(nor, 0.0)).xyz;

  return float4(tN, nor);
}


float sBox(in float3 ro, in float3 rd, in float4x4 txx, in float3 rad)
{
  float3 rdd = (txx * float4(rd, 0.0)).xyz;
  float3 roo = (txx * float4(ro, 1.0)).xyz;

  float3 m = 1.0 / rdd;
  float3 n = m * roo;
  float3 k = abs(m) * rad;

  float3 t1 = -n - k;
  float3 t2 = -n + k;

  float tN = max(max(t1.x, t1.y), t1.z);
  float tF = min(min(t2.x, t2.y), t2.z);
  if (tN > tF || tF < 0.0) return -1.0;

  return tN;
}


//-----------------------------------------------------------------------------------------

float4x4 rotationAxisAngle( float3 v, float angle )
{
  float s = sin( angle );
  float c = cos( angle );
  float ic = 1.0 - c;

  return float4x4(v.x*v.x*ic + c,     v.y*v.x*ic - s*v.z, v.z*v.x*ic + s*v.y, 0.0,
                  v.x*v.y*ic + s*v.z, v.y*v.y*ic + c,     v.z*v.y*ic - s*v.x, 0.0,
                  v.x*v.z*ic - s*v.y, v.y*v.z*ic + s*v.x, v.z*v.z*ic + c,     0.0,
                  0.0,                0.0,                0.0,                1.0 );
}

float4x4 translate( float x, float y, float z )
{
  return float4x4(1.0, 0.0, 0.0, 0.0,
                  0.0, 1.0, 0.0, 0.0,
                  0.0, 0.0, 1.0, 0.0,
                  x,   y,   z,   1.0 );
}

float4x4 inverse( in float4x4 m )
{
  return float4x4(m[0][0], m[1][0], m[2][0], 0.0,
                  m[0][1], m[1][1], m[2][1], 0.0,
                  m[0][2], m[1][2], m[2][2], 0.0,
                  -dot(m[0].xyz,m[3].xyz),
                  -dot(m[1].xyz,m[3].xyz),
                  -dot(m[2].xyz,m[3].xyz),
                  1.0 );
}

void main( void )
{
  float2 p = (-iResolution.xy + 2.0*gl_FragCoord.xy) / iResolution.y;

  // camera movement	
  float an = 0.4*iGlobalTime;
  float3 ro = float3( 2.5*cos(an), 1.0, 2.5*sin(an) );
  float3 ta = float3( 0.0, 0.8, 0.0 );
  // camera matrix
  float3 ww = normalize( ta - ro );
  float3 uu = normalize( cross(ww,float3(0.0,1.0,0.0) ) );
  float3 vv = normalize( cross(uu,ww));
  // create view ray
  float3 rd = normalize( p.x*uu + p.y*vv + 2.0*ww );

  // rotate and translate box	
  float4x4 rot = rotationAxisAngle( normalize(float3(1.0,1.0,0.0)), iGlobalTime );
  float4x4 tra = translate( 0.0, 1.0, 0.0 );
  float4x4 txi = tra * rot; 
  float4x4 txx = inverse( txi );

  // raytrace
  float tmin = 10000.0;
  float3  nor = float3(0.0);
  float3  pos = float3(0.0);

  // raytrace-plane
  float oid = 0.0;
  float h = (0.0-ro.y)/rd.y;
  if (h>0.0 ) 
  { 
    tmin = h; 
    nor = float3(0.0,1.0,0.0); 
    oid = 1.0;
  }

  // raytrace box
  float3 box = float3(0.4,0.6,0.8) ;
  float4 res = iBox( ro, rd, txx, txi, box);
  if (res.x>0.0 && res.x<tmin )
  {
    tmin = res.x; 
    nor = res.yzw;
    oid = 2.0;
  }

  // shading/lighting	
  float3 col = float3(0.9);
  if (tmin<100.0 )
  {
    float3 lig = normalize(float3(-0.8,0.4,0.1));
    pos = ro + tmin*rd;

    // material
    float occ = 1.0;
    float3  mate = float3(1.0);
    if (oid<1.5 ) // plane
    {
      mate = texture2D( iChannel0, 0.25*pos.xz ).xyz;
      occ = 0.2 + 0.8*smoothstep( 0.0, 1.5, length(pos.xz) );
    }			
    else // box
    {
      // recover box space data (we want to do shading in object space)			
      float3 opos = (txx*float4(pos,1.0)).xyz;
      float3 onor = (txx*float4(nor,0.0)).xyz;
      mate = abs(onor.x)*texture2D( iChannel0, 0.5*opos.yz ).xyz + 
      abs(onor.y)*texture2D( iChannel0, 0.5*opos.zx ).xyz + 
      abs(onor.z)*texture2D( iChannel0, 0.5*opos.xy ).xyz;

      // wireframe
      mate *= 1.0 - (1.0-abs(onor.x))*smoothstep( box.x-0.04, box.x-0.02, abs(opos.x) );
      mate *= 1.0 - (1.0-abs(onor.y))*smoothstep( box.y-0.04, box.y-0.02, abs(opos.y) );
      mate *= 1.0 - (1.0-abs(onor.z))*smoothstep( box.z-0.04, box.z-0.02, abs(opos.z) );

      occ = 0.6 + 0.4*nor.y;
    }		
    mate = mate*mate*1.5;

    // lighting
    float dif = clamp( dot(nor,lig), 0.0, 1.0 );
    dif *= step( sBox( pos+0.01*nor, lig, txx, box ), 0.0 );
    col = float3(0.13,0.17,0.2)*occ*3.0 + 1.5*dif*float3(1.0,0.9,0.8);

    // material * lighting		
    col *= mate;

    // fog
    col = mix( col, float3(0.9), 1.0-exp( -0.003*tmin*tmin ) );
  }

  col = sqrt( col );

  gl_FragColor = float4( col, 1.0 );
}