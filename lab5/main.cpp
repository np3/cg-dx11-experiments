#include <windows.h>
#include "lab_wnd.h"

#pragma comment(lib, "lib\\D3DCompiler47.lib")

INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nShowCmd)
{
   LAB_WND wnd("Raymarching", 640, 480);

   return wnd.Loop();
}