#include "scn_transform.h"
#include "common.h"

scnTRANSFORM::scnTRANSFORM()
{
   Id();
}

scnTRANSFORM& scnTRANSFORM::Translate(const D3DXVECTOR3& v)
{
   D3DXMATRIX m;
   D3DXMatrixTranslation(&m, v.x, v.y, v.z);
   D3DXMatrixMultiply(&mat, &mat, &m);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::SetTranslate(const D3DXVECTOR3& v)
{
   D3DXMatrixTranslation(&mat, v.x, v.y, v.z);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::Scale(const D3DXVECTOR3 &v)
{
   D3DXMATRIX m;

   D3DXMatrixScaling(&m, v.x, v.y, v.z);
   D3DXMatrixMultiply(&mat, &mat, &m);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::SetScale(const D3DXVECTOR3 &v)
{
   D3DXMatrixScaling(&mat, v.x, v.y, v.z);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::RotateX(float angle)
{
   D3DXMATRIX m;

   D3DXMatrixRotationX(&m, angle);
   D3DXMatrixMultiply(&mat, &mat, &m);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::SetRotateX(float angle)
{
   D3DXMatrixRotationX(&mat, angle);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::RotateY(float angle)
{
   D3DXMATRIX m;

   D3DXMatrixRotationY(&m, angle);
   D3DXMatrixMultiply(&mat, &mat, &m);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::SetRotateY(float angle)
{
   D3DXMatrixRotationY(&mat, angle);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::RotateZ(float angle)
{
   D3DXMATRIX m;

   D3DXMatrixRotationZ(&m, angle);
   D3DXMatrixMultiply(&mat, &mat, &m);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::SetRotateZ(float angle)
{
   D3DXMatrixRotationZ(&mat, angle);
   return *this;
}

scnTRANSFORM scnTRANSFORM::operator*(const scnTRANSFORM& right)
{
   scnTRANSFORM newTrans;
   D3DXMatrixMultiply(&newTrans.mat, &mat, &right.mat);
   return newTrans;
}

scnTRANSFORM& scnTRANSFORM::operator*=(const scnTRANSFORM& right)
{
   D3DXMatrixMultiply(&mat, &mat, &right.mat);
   return *this;
}

scnTRANSFORM& scnTRANSFORM::Id()
{
   D3DXMatrixIdentity(&mat);
   return *this;
}
