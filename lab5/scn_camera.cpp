#include "scn_camera.h"

scnCAMERA::scnCAMERA()
{
   // setup initial cam basis
   look = D3DXVECTOR3(0, 0, 1);
   up = D3DXVECTOR3(0, 1, 0);
   pos = D3DXVECTOR3(0, 0, 0);
   right = D3DXVECTOR3(1, 0, 0);

   D3DXMatrixIdentity(&viewMatrix);
   D3DXMatrixIdentity(&projMatrix);
   camMoved = true;
}

void scnCAMERA::BuildViewMatrix()
{
   camMoved = true;
   D3DXVec3Normalize(&look, &look);
   D3DXVec3Cross(&up, &look, &right);
   D3DXVec3Normalize(&up, &up);
   D3DXVec3Cross(&right, &up, &look);
   D3DXVec3Normalize(&right, &right);
   // Build view matrix
   float x = -D3DXVec3Dot(&right, &pos);
   float y = -D3DXVec3Dot(&up, &pos);
   float z = -D3DXVec3Dot(&look, &pos);

   viewMatrix(0, 0) = right.x;
   viewMatrix(0, 1) = up.x;
   viewMatrix(0, 2) = look.x;
   viewMatrix(0, 3) = 0.f;

   viewMatrix(1, 0) = right.y;
   viewMatrix(1, 1) = up.y;
   viewMatrix(1, 2) = look.y;
   viewMatrix(1, 3) = 0.f;

   viewMatrix(2, 0) = right.z;
   viewMatrix(2, 1) = up.z;
   viewMatrix(2, 2) = look.z;
   viewMatrix(2, 3) = 0.f;

   viewMatrix(3, 0) = x;
   viewMatrix(3, 1) = y;
   viewMatrix(3, 2) = z;
   viewMatrix(3, 3) = 1.f;
}

void scnCAMERA::LookAt(D3DXVECTOR3& _pos, D3DXVECTOR3& target, 
                       D3DXVECTOR3& Up)
{
   at = target;
   // Set look vector
   D3DXVECTOR3 L = target - _pos;
   radius = D3DXVec3Length(&L);
   D3DXVec3Normalize(&L, &L);
   look = L;
   // Keep axes orthonormal
   D3DXVECTOR3 R;
   D3DXVec3Cross(&R, &Up, &look);
   D3DXVec3Normalize(&R, &R);
   right = R;

   D3DXVECTOR3 U;
   D3DXVec3Cross(&U, &look, &right);
   up = U;
   pos = _pos;
   theta = acos(look.y);
   phi = atan2(look.x, look.z);
   BuildViewMatrix();
}

void scnCAMERA::BuildPerspProjMatrix(float ang, float aRatio, 
                                    float _zNear, float _zFar)
{
   fovFactor = tan(ang * 0.5f);
   zNear = _zNear;
   zFar = _zFar;
   aspectRatio = aRatio;
   D3DXMatrixPerspectiveFovLH(&projMatrix, ang, aRatio, zNear, zFar);
   camMoved = true;
}

scnCAMERA& scnCAMERA::RotateLookAtHorz(float ang)
{
   theta = acos(look.y);
   phi = atan2(look.x, look.z) + ang;
   if (phi > 2.0f * D3DX_PI) {
      phi -= 2.0f * D3DX_PI;
   }
   Rebuild();
   return *this;
}

scnCAMERA& scnCAMERA::RotateLookAtVert(float ang)
{
   theta = acos(look.y) - ang;
   if (theta > D3DX_PI - 0.001f) {
      theta = D3DX_PI - 0.001f;
   }
   if (theta < 0.001f) {
      theta = 0.001f;
   }
   phi = atan2(look.x, look.z);
   Rebuild();
   return *this;
}

void scnCAMERA::Rebuild()
{
   look.x = sin(theta) * sin(phi);
   look.y = cos(theta);
   look.z = sin(theta) * cos(phi);

   pos = at - look * radius;

   right.x = sin(theta) * sin(phi - 0.01f);
   right.y = cos(theta);
   right.z = sin(theta) * cos(phi - 0.01f);

   right = right - look;
   D3DXVec3Cross(&up, &look, &right);
   D3DXVec3Normalize(&up, &up);
   D3DXVec3Cross(&right, &look, &up);
   D3DXVec3Normalize(&right, &right);
   BuildViewMatrix();
}

scnCAMERA& scnCAMERA::Zoom(float dist)
{
   if (radius + dist >= 0.0f) 
   {
      radius += dist;
      pos = at - look * radius;
      BuildViewMatrix();
   }
   return *this;
}

const D3DXMATRIX& scnCAMERA::GetViewProjMatrix()
{
   D3DXMatrixMultiplyTranspose(&viewProjMatrix, &viewMatrix, &projMatrix);
   return viewProjMatrix;
}