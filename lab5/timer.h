#pragma once
#ifndef _TIMER_H_
#define _TIMER_H_
#include <windows.h>

class Timer
{
private:
   LARGE_INTEGER lFreq, lLastTick;
   float dt, time;
public:
   Timer() : dt(0), time(0) {
      QueryPerformanceFrequency(&lFreq);
      QueryPerformanceCounter(&lLastTick);
   }
   void Update() {
      LARGE_INTEGER tmp = lLastTick;

      QueryPerformanceCounter(&lLastTick);
      dt = (float)(lLastTick.QuadPart - tmp.QuadPart) / (float)lFreq.QuadPart;
      time += dt;
   }
   float GetDelta() {
      return dt;
   }
   float GetTimeSec() const {
      return time;
   }
};

#endif