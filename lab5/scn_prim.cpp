#include "common.h"
#include "scn_prim.h"
#include "d3d_drv.h"
#include "common.h"

void scnMATERIAL::Setup(d3dDRIVER *drv)
{
   ID3D11ShaderResourceView *ppView[MAX_TEX];

   for (int i = 0; i < MAX_TEX; ++i) {
      ppView[i] = textures[i] ? textures[i]->GetSRV() : nullptr;
   }

   drv->GetD3DContext()->VSSetShaderResources(0, MAX_TEX, ppView);
   drv->GetD3DContext()->PSSetShaderResources(0, MAX_TEX, ppView);
}

scnPRIM::scnPRIM(DWORD col) :  pVB(nullptr), pIB(nullptr), vertNum(0), indNum(0), 
                               color(col), pVert(nullptr), pInd(nullptr) {
   topology = D3D11_PRIMITIVE_TOPOLOGY_UNDEFINED;
}

scnPRIM::~scnPRIM() {
   Release();
}

void scnPRIM::FitDesc(d3dDRIVER *driver)
{
   d3dStateID = driver->FitD3DState(primDesc);
}

void scnPRIM::Init(d3dDRIVER *driver)
{
   FitDesc(driver);
   if (pVert) {
    pVB = driver->CreateVertexBuffer((void *)pVert, sizeof(scnVERTEX) * vertNum);
    delete[] pVert;
    pVert = nullptr;
   }
   if (pInd) {
    pIB = driver->CreateIndexBuffer((void *)pInd, sizeof(USHORT) * indNum);
    delete[] pInd;
    pInd = nullptr;
   }

}

void scnPRIM::Render(d3dDRIVER *driver)
{
   UINT offset = 0;
   UINT stride = sizeof(scnVERTEX);
   
   mtl.Setup(driver);
   driver->SetD3DState(d3dStateID);
   if (pVB) {
     driver->SetVB(pVB, &stride, &offset);
   } else {
     driver->SetVB(nullptr, nullptr, nullptr);
   }
   
   if (pIB) {
      driver->SetIB(pIB);
      driver->RenderIndexed(indNum, topology);
   } else {
      driver->Render(vertNum, topology);
   }
}

void scnPRIM::Release()
{
   vertNum = 0;
   indNum = 0;
   if (pVert) {
      delete[] pVert;
   }
   if (pInd) {
      delete[] pInd;
   }
}

void scnPRIM::CreatePlane()
{
   Release();
   pVert = new scnVERTEX[4];
   auto vertices = pVert;
   vertNum = 4;

   static const D3DXVECTOR3 planeNormal = D3DXVECTOR3(0.0f, 0.0f, -1.0f);
   //
   vertices[0].position = D3DXVECTOR3(0.f, 1.f, 0.f);
   vertices[0].normal = planeNormal;
   vertices[0].color = color;
   vertices[0].U = 0.0f;
   vertices[0].V = 1.0f;
   //
   vertices[1].position = D3DXVECTOR3(1.f, 1.f, 0.f);
   vertices[1].normal = planeNormal;
   vertices[1].color = color;
   vertices[1].U = 1.0f;
   vertices[1].V = 1.0f;
   //
   vertices[2].position = D3DXVECTOR3(0.f, 0.f, 0.f);
   vertices[2].normal = planeNormal;
   vertices[2].color = color;
   vertices[2].U = 0.0f;
   vertices[2].V = 0.0f;
   //
   vertices[3].position = D3DXVECTOR3(1.f, 0.f, 0.f);
   vertices[3].normal = planeNormal;
   vertices[3].color = color;
   vertices[3].U = 1.0f;
   vertices[3].V = 0.0f;
   //
   topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
}

void scnPRIM::CreateGrid(int sizeU, int sizeV)
{
   Release();
   vertNum = (sizeU) * (sizeV);
   pVert = new scnVERTEX[vertNum];
   auto pVertices = pVert;
   indNum = ((sizeU * 2) + 1) * (sizeV - 1);
   pInd = new USHORT[indNum];
   auto pIndices = pInd;
   static const D3DXVECTOR3 planeNormal = D3DXVECTOR3(0.0f, 1.0f, 0.0f);
   const float tileSizeX = 1.0f;
   const float tileSizeY = 1.0f;
   for (int y = 0; y < sizeV; ++y) {
      for (int x = 0; x < sizeU; ++x) {
         pVertices->color = color;
         pVertices->normal = planeNormal;
         pVertices->U = (float)x / (float)sizeU;
         pVertices->V = (float)y / (float)sizeV;
         pVertices->position.x = x * tileSizeX;
         pVertices->position.y = 0.0f;
         pVertices->position.z = y * tileSizeY;
         ++pVertices;
      }
   }

   for (int y = 0; y < sizeV - 1; ++y) {
      USHORT idx = 0;
      for (int x = 0; x < sizeU; ++x) {
         *pIndices++ = x + y * sizeU;
         idx = x + (y + 1) * sizeU;
         *pIndices++ = idx;
      }
      // Join two triangle strips
      *pIndices++ = 0xFFFF;
   }
   topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
}

void scnPRIM::CreateCyl(float radius)
{
   Release();
   
   const int xzSides = 30;
   const int ySides = 30;
   const float dTheta = 2.0f * D3DX_PI / (float)xzSides;
   const float dY = 1.0f / (float)ySides;
   int p = 0, k = 0;
   vertNum = (ySides + 1) * (xzSides + 1);
   indNum = 2 * (xzSides + 1) * ySides;
   pVert = new scnVERTEX[vertNum];
   scnVERTEX *pVertices = pVert;
   pInd = new USHORT[indNum];
   USHORT *pIndices = pInd;
   USHORT *pTmpIndices = pIndices;

   float y = 0.0f;
   for (int i = 0; i <= ySides; ++i) {
      float theta = 0.0f;
      for (int j = 0; j <= xzSides; ++j) {
         float x = cos(theta), z = sin(theta);

         pVertices[p].position.x = radius * x;
         pVertices[p].position.y = y;
         pVertices[p].position.z = radius * z;
         pVertices[p].normal.x = x;
         pVertices[p].normal.y = 0.0f;
         pVertices[p].normal.z = z;
         pVertices[p].color = color;
         pVertices[p].U = j / (float)xzSides;
         pVertices[p].V = i / (float)ySides;
         ++p;
         theta += dTheta;
         if (i != ySides) {
            *pTmpIndices++ = k;
            *pTmpIndices++ = k + (xzSides + 1);
            k++;
         }
      }
      y += dY;
   }
   topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
}

void scnPRIM::CreateBox()
{
  Release();
  vertNum = 8;
  indNum = 6 * 6;

  pVert = new scnVERTEX[vertNum];
  pInd = new USHORT[indNum];
  auto pVertices = pVert;
  auto pIndices = pInd;

  pVertices[0].position = D3DXVECTOR3(-1, 1, -1);
  pVertices[0].color = 0xFFFFFFFF;
  pVertices[1].position = D3DXVECTOR3(1, 1, -1);
  pVertices[1].color = 0xFFFFFFFF;
  pVertices[2].position = D3DXVECTOR3(-1, -1, -1);
  pVertices[2].color = 0xFFFFFFFF;
  pVertices[3].position = D3DXVECTOR3(1, -1, -1);
  pVertices[3].color = 0xFFFFFFFF;
  pVertices[4].position = D3DXVECTOR3(-1, 1, 1);
  pVertices[4].color = 0xFFFFFFFF;
  pVertices[5].position = D3DXVECTOR3(1, 1, 1);
  pVertices[5].color = 0xFFFFFFFF;
  pVertices[6].position = D3DXVECTOR3(-1, -1, 1);
  pVertices[6].color = 0xFFFFFFFF;
  pVertices[7].position = D3DXVECTOR3(1, -1, 1);
  pVertices[7].color = 0xFFFFFFFF;

  USHORT indices[] =
  {
    0, 1, 2,    // side 1
    2, 1, 3,
    4, 0, 6,    // side 2
    6, 0, 2,
    7, 5, 6,    // side 3
    6, 5, 4,
    3, 1, 7,    // side 4
    7, 1, 5,
    4, 5, 0,    // side 5
    0, 5, 1,
    3, 7, 2,    // side 6
    2, 7, 6,
  };
  memcpy_s(pInd, 36 * sizeof(USHORT), indices, 36 * sizeof(USHORT));
  topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
}
void scnPRIM::CreateSphere(float radius)
{
  int N = 17, M = 30, p = 0, k = 0;
  float theta, phi;
  vertNum = (M + 1) * (N + 1);
  indNum = 2 * (M + 1) * N;
  pVert = new scnVERTEX[vertNum];
  scnVERTEX *pVertices = pVert;
  pInd = new USHORT[indNum];
  unsigned short *pIndices = pInd;
  unsigned short *pTmpIndices = pIndices;
  
  for (int i = 0; i <= N; ++i) {
     theta = D3DX_PI * i / N;
     for (int j = 0; j <= M; ++j) {
        phi = 2 * D3DX_PI * j / M;
        pVertices[p].position.x = radius * sin(theta) *  cos(phi);
        pVertices[p].position.y = radius * cos(theta);
        pVertices[p].position.z = radius * sin(theta) *  sin(phi);
        pVertices[p].normal = pVertices[p].position; // because center (0, 0, 0)
        pVertices[p].color = color;
        pVertices[p].U = (float)j / M, pVertices[p].V = (float)(i + 1) / N;
        ++p;
        if (i != N) {
           *pTmpIndices++ = k + (M + 1);
           *pTmpIndices++ = k;
           k++;
        }
     }
  }
  topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
}

void scnPRIM::CreateFullScreenQuad()
{
  vertNum = 4;
  indNum = 0;
  topology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
}

scnHIERARCHY::scnHIERARCHY() {
   root = new NODE();
   root->SetGeom(0, nullptr);
}

scnHIERARCHY::~scnHIERARCHY() {
   scnHIERARCHY_ITER iter(this);

   for (iter.Begin(); !iter.IsDone(); ++iter) {
      {
         delete *iter;
      }
   }
}

scnHIERARCHY::NODE* scnHIERARCHY::NODE::AddChild()
{
   NODE *newNode = new NODE();

   newNode->pParent = this;
   newNode->pNext = this->pChild;
   this->pChild = newNode;
   return newNode;
}

void scnHIERARCHY_ITER::Begin()
{
   pCur = pHier->GetRoot();

   scnHIERARCHY::NODE *pChild = pCur->pChild;
   while (pChild != nullptr) {
      stack.push_back(pChild);
      pChild = pChild->pNext;
   }
}

void scnHIERARCHY_ITER::operator++()
{
   if (stack.size() != 0) {
      pCur = stack.back();
      stack.pop_back();
   } else {
      pCur = NULL;
      return;
   }
   scnHIERARCHY::NODE *pNode = pCur->pChild;

   while (pNode != NULL) {
      stack.push_back(pNode);
      pNode = pNode->pNext;
   }
}

void scnHIERARCHY_ITER::operator++(int)
{
   operator++();
}