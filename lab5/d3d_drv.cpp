#include "common.h"
#include "d3d_drv.h"
#include <fstream>
#include <vector>
#include <D3Dcompiler.h>
#include <d3dx11.h>
#include <cmath>

bool d3dTEX::LoadFromFile(d3dDRIVER *drv, const char *pFileName)
{
   Release();
   if (FAILED(D3DX11CreateShaderResourceViewFromFile(drv->GetD3DDevice(), pFileName, 
                                                     nullptr, nullptr, &this->pView, nullptr))) {
      return false;
   }
   D3D11_SHADER_RESOURCE_VIEW_DESC desc;
   pView->GetResource((ID3D11Resource **)&pTex2D);
   pView->GetDesc(&desc);
   D3D11_TEXTURE2D_DESC tDesc;
   pTex2D->GetDesc(&tDesc);
   W = tDesc.Width;
   H = tDesc.Height;
   return true;
}

bool d3dTEX::GenerateNoiseTex(d3dDRIVER *drv, int Width, int Height)
{
   Release();
   W = Width;
   H = Height;
   auto pBuf = GeneratePerlinLikeNoise(W, H);
   D3D11_TEXTURE2D_DESC tDesc;

   tDesc.Width = Width;
   tDesc.Height = Height;
   tDesc.BindFlags = D3D11_BIND_SHADER_RESOURCE;
   tDesc.ArraySize = 1;
   tDesc.CPUAccessFlags = 0;
   tDesc.MiscFlags = 0;
   tDesc.Usage = D3D11_USAGE_DEFAULT;
   tDesc.Format = DXGI_FORMAT_R8_TYPELESS;
   tDesc.SampleDesc.Count = 1;
   tDesc.SampleDesc.Quality = 0;
   int nMipMaps = 1;//(int)floor(log(W) / log(2));
   tDesc.MipLevels = nMipMaps;

   D3D11_SUBRESOURCE_DATA sData;
   sData.pSysMem = pBuf;
   sData.SysMemPitch = W;
   sData.SysMemSlicePitch = W * H;
   
   if (FAILED(drv->GetD3DDevice()->CreateTexture2D(&tDesc, &sData, &pTex2D))) {
      return false;
   }
   delete[] pBuf;
   D3D11_SHADER_RESOURCE_VIEW_DESC sDesc;
   sDesc.Format = DXGI_FORMAT_R8_UNORM;
   sDesc.Texture2D.MipLevels = nMipMaps;
   sDesc.Texture2D.MostDetailedMip = 0;
   sDesc.ViewDimension = D3D11_SRV_DIMENSION_TEXTURE2D;
   if (FAILED(drv->GetD3DDevice()->CreateShaderResourceView(pTex2D, &sDesc, &pView))) {
      return false;
   }
   return true;
}

d3dSHADER::~d3dSHADER()
{
   D3D_RELEASE(pVS);
   D3D_RELEASE(pPS);
   D3D_RELEASE(pInputLayout);
}

d3dDRIVER* d3dDRIVER::Inst()
{
   static d3dDRIVER theDriver;

   return &theDriver;
}

void d3dSHADER::Init(d3dDRIVER *drv,const char *pFilename)
{
   std::ifstream input(pFilename, std::ios::binary);

   if (!input.is_open()) {
      D3D_CHECK(-1);
   }
   input.seekg(0, std::ios::end);
   int size = (int)input.tellg();
   input.seekg(0, std::ios::beg);
   std::vector<unsigned char> buf(size);

   if (!input.read((char *)buf.data(), size)) {
      D3D_CHECK(-1);
   }
   input.close();
   ID3DBlob *pBlob, *pErr = nullptr;
   // Vertex shader
   HRESULT hr = D3DCompile(buf.data(), size, nullptr, nullptr, 
                           (ID3DInclude*)0x1,
                           "VS", "vs_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3 | D3DCOMPILE_AVOID_FLOW_CONTROL, 0, &pBlob, &pErr);
   if (FAILED(hr)) {
      DBG_OUTPUT("Error while compiling vertex shader\n");
      DBG_OUTPUT((char*)pErr->GetBufferPointer());
      D3D_CHECK(-1);
   }
   D3D_CHECK(drv->GetD3DDevice()->CreateVertexShader(pBlob->GetBufferPointer(), 
                                                     pBlob->GetBufferSize(), 
                                                     NULL, &pVS));
   // Create input layout
   int num = 0;
   const D3D11_INPUT_ELEMENT_DESC* eDesc = CreateIALayout(&num);
   if (eDesc) {
     D3D_CHECK(drv->GetD3DDevice()->CreateInputLayout(eDesc, num, 
               pBlob->GetBufferPointer( ), pBlob->GetBufferSize(), 
               &pInputLayout));
   } else {
     pInputLayout = nullptr;
   }
   
   pBlob->Release();
   if (!skipPixelShader) {
     hr = D3DCompile(buf.data(), size, nullptr, nullptr, 
       (ID3DInclude*)0x1,
       "PS", "ps_5_0", D3DCOMPILE_OPTIMIZATION_LEVEL3 | D3DCOMPILE_AVOID_FLOW_CONTROL, 0, &pBlob, &pErr);
     if (FAILED(hr)) {
       DBG_OUTPUT("Error while compiling pixel shader");
       DBG_OUTPUT((char*)pErr->GetBufferPointer());
       D3D_CHECK(-1);
     }
     D3D_CHECK(drv->GetD3DDevice()->CreatePixelShader(pBlob->GetBufferPointer(), 
       pBlob->GetBufferSize(), 
       NULL, &pPS));
     pBlob->Release();
   }
}

void d3dCONSTANT_BUFFER::SetConst(int offset, void *Data, int byteSize)
{
   memcpy(&cpuData[offset], Data, byteSize);
   needUpload = true;
}

void d3dDRIVER::SetShader(const d3dSHADER &shd)
{
   pImmContext->VSSetShader(shd.pVS, nullptr, 0);
   pImmContext->PSSetShader(shd.pPS, nullptr, 0);
   pImmContext->IASetInputLayout(shd.pInputLayout);
}

void d3dDRIVER::SetVB(d3dBUFFER *pVB, UINT *strides, UINT *offsets)
{
   if (pVB) {
      pImmContext->IASetVertexBuffers(0, 1, &pVB->buf, strides, offsets);
   } else {
      pImmContext->IASetVertexBuffers(0, 0, nullptr, nullptr, nullptr);
   }
   
}

void d3dDRIVER::SetIB(d3dBUFFER *pIB)
{
   if (pIB) {
     pImmContext->IASetIndexBuffer(pIB->buf, DXGI_FORMAT_R16_UINT, 0);
   } else {
     pImmContext->IASetIndexBuffer(nullptr, DXGI_FORMAT_UNKNOWN, 0);
   }
   
}

void d3dDRIVER::SetConstantBuffer(int slot, d3dCONSTANT_BUFFER *pCB)
{
   pImmContext->VSSetConstantBuffers(slot, 1, &pCB->buf);
   pImmContext->PSSetConstantBuffers(slot, 1, &pCB->buf);
}

void d3dDRIVER::Render(int numVert, D3D11_PRIMITIVE_TOPOLOGY topology, int vertOffset)
{
   pImmContext->IASetPrimitiveTopology(topology);
   pImmContext->Draw(numVert, vertOffset);
}

void d3dDRIVER::RenderIndexed(int numInd, D3D11_PRIMITIVE_TOPOLOGY topology, 
                              int vertOffset, int indOffset)
{
   pImmContext->IASetPrimitiveTopology(topology);
   pImmContext->DrawIndexed(numInd, indOffset, vertOffset);
}

void d3dDRIVER::SetupViewport()
{
   D3D11_VIEWPORT vp = {0.0f, 0.0f, (float)W, (float)H, 0.0f, 1.0f};

   pImmContext->RSSetViewports(1, &vp);
}

bool d3dDRIVER::SetupDSV()
{
   D3D11_TEXTURE2D_DESC dsDesc;

   dsDesc.Width = W;
   dsDesc.Height = H;
   dsDesc.MipLevels = 1;
   dsDesc.ArraySize = 1;
   dsDesc.Format = DXGI_FORMAT_R24G8_TYPELESS;
   dsDesc.SampleDesc.Count = 1;
   dsDesc.SampleDesc.Quality = 0;
   dsDesc.Usage = D3D11_USAGE_DEFAULT;
   dsDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
   dsDesc.CPUAccessFlags = 0;
   dsDesc.MiscFlags = 0;
   D3D_CHECK(pDevice->CreateTexture2D(&dsDesc, NULL, &pDepthStencilBuf));

   D3D11_DEPTH_STENCIL_VIEW_DESC dsv;

   dsv.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
   dsv.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2DMS;
   dsv.Texture2D.MipSlice = 0;
   dsv.Flags              = 0;
   D3D_CHECK(pDevice->CreateDepthStencilView(pDepthStencilBuf, &dsv, &pDSV));
   return true;
}

bool d3dDRIVER::SetupSamplers()
{
   D3D11_SAMPLER_DESC sDesc;

   sDesc.AddressU = D3D11_TEXTURE_ADDRESS_WRAP;
   sDesc.AddressV = D3D11_TEXTURE_ADDRESS_WRAP;
   sDesc.AddressW = D3D11_TEXTURE_ADDRESS_WRAP;
   sDesc.ComparisonFunc = D3D11_COMPARISON_ALWAYS;
   sDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_POINT;
   sDesc.MaxAnisotropy = 1;
   sDesc.MinLOD = 0.0f;
   sDesc.MaxLOD = FLT_MAX;
   sDesc.MipLODBias = 0.0f;
   if (FAILED(pDevice->CreateSamplerState(&sDesc, &pSamplers[0]))) {
      return false;
   }
   sDesc.Filter = D3D11_FILTER_COMPARISON_MIN_MAG_MIP_LINEAR;
   if (FAILED(pDevice->CreateSamplerState(&sDesc, &pSamplers[1]))) {
      return false;
   }
   return true;
}

bool d3dDRIVER::SetupRTV()
{
   ID3D11Texture2D *pBackBuf;
   // Create a back buffer render target, get a view on it to clear it later
   pSwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), (LPVOID*)&pBackBuf) ;
   D3D_CHECK(pDevice->CreateRenderTargetView((ID3D11Resource*)pBackBuf, NULL, &pRTV));
   pBackBuf->Release();
   return true;
}

bool d3dDRIVER::Init(HWND hWnd, int Width, int Height)
{
   W = Width;
   H = Height;
   DXGI_SWAP_CHAIN_DESC sd;

   sd.BufferDesc.Width = W;
   sd.BufferDesc.Height = H;
   sd.BufferDesc.RefreshRate.Numerator = 60;
   sd.BufferDesc.RefreshRate.Numerator = 1;
   sd.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
   sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
   sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
   sd.SampleDesc.Count = 1;
   sd.SampleDesc.Quality = 0;
   sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
   sd.BufferCount = 1;
   sd.OutputWindow = hWnd;
   sd.Windowed = TRUE;
   sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
   sd.Flags = 0;
   D3D_FEATURE_LEVEL featureLevel;

   D3D_CHECK(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE,
                                           NULL, 0, NULL, 0, D3D11_SDK_VERSION,
                                           &sd, &pSwapChain,
                                           &pDevice, &featureLevel, &pImmContext));

   if (featureLevel < D3D_FEATURE_LEVEL_11_0)
   {
      //MessageBox(NULL, "Need DirectX11 or higher hardware support, falling back to WARP (low FPS is expected)",
      //           "Warning", MB_OK | MB_ICONWARNING);
      D3D_CHECK(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_WARP,
                                              NULL, 0, NULL, 0, D3D11_SDK_VERSION,
                                              &sd, &pSwapChain,
                                              &pDevice, &featureLevel, &pImmContext));

      if (featureLevel < D3D_FEATURE_LEVEL_11_0) 
      {
        MessageBox(NULL, "WARP didn't worked as well, you should submit bug report (be sure to include your full system configuration)",
                   "Error", MB_OK | MB_ICONERROR);
        return false;
      }
   }
   SetupRTV();
   SetupDSV();
   SetupViewport();
   SetupSamplers();
   // Because no linear filtering in vertex shader
   pImmContext->VSSetSamplers(0, 1, &pSamplers[0]);
   pImmContext->PSSetSamplers(0, 2, pSamplers);
   // Init wireframe
   d3dSTATE_DESC wireDesc;
   wireDesc.fillMode = d3dSTATE_DESC::WIREFRAME;
   wireDesc.cullMode = d3dSTATE_DESC::NONE;
   int id = FitD3DState(wireDesc);
   wireframeState = states[id];
   curState = wireframeState;
   //
   pImmContext->OMSetRenderTargets(1, &pRTV, pDSV);
   // Set clear color
   clearColor[0] = clearColor[1] = clearColor[2] = 0.0f;
   clearColor[3] = 0.0f;
   inited = true;
   return true;
}

void d3dDRIVER::ToggleWireframe()
{
   isWireframe = !isWireframe;
}

void d3dDRIVER::Resize(int nW, int nH)
{
   if (W == nW && H == nH) {
      return;
   }
   W = nW;
   H = nH;
   if (pSwapChain) {
      pImmContext->OMSetRenderTargets(0, NULL, NULL);
      D3D_RELEASE(pRTV);
      D3D_RELEASE(pDepthStencilBuf);
      D3D_RELEASE(pDSV);
      D3D_CHECK(pSwapChain->ResizeBuffers(0, 0, 0, DXGI_FORMAT_UNKNOWN, 0));
      SetupRTV(); // recreate render-target view
      SetupDSV();
      pImmContext->OMSetRenderTargets(1, &pRTV, pDSV);
      SetupViewport();
   }
}

void d3dDRIVER::Release()
{
   D3D_RELEASE(pSwapChain);
   D3D_RELEASE(pDevice);
   D3D_RELEASE(pImmContext);
   // Z - buffer
   D3D_RELEASE(pDepthStencilBuf);
   D3D_RELEASE(pDSV);
   // Back buffer
   D3D_RELEASE(pRTV);
   // Samplers
   for (int i = 0; i < sizeof(pSamplers) / sizeof(ID3D11SamplerState*); ++i) {
      D3D_RELEASE(pSamplers[i]);
   }
   // Buffers pool
   for (int i = 0; i < (int)buffersPool.size(); ++i) {
      delete buffersPool[i];
   }
   buffersPool.clear();
   // States
   for (int i = 0; i < (int)states.size(); ++i) {
      delete states[i];
   }
   states.clear();
}

void d3dDRIVER::Clear(UINT c)
{
   UINT clearFlags = 0;
   if (c & C_DEPTH) {
     clearFlags |= D3D11_CLEAR_DEPTH;
   }
   if (c & C_STENCIL) {
     clearFlags |= D3D11_CLEAR_STENCIL;
   }
   if (clearFlags) {
    pImmContext->ClearDepthStencilView(pDSV, clearFlags, 1.0f, 0);
   }
   if (c & C_COLOR) {
    pImmContext->ClearRenderTargetView(pRTV, clearColor);
   }
}

void d3dDRIVER::BeginRender()
{
}

void d3dDRIVER::EndRender()
{
   pSwapChain->Present(0, 0);
}

void d3dDRIVER::SetClearColor(float cc[4])
{
   memcpy_s(clearColor, 4 * sizeof(float), cc, 4 * sizeof(float));
}

static ID3D11Buffer* CreateBuffer(ID3D11Device *pDevice, UINT bindFlags, void *pData, int byteWidth)
{
   ID3D11Buffer* pBuf;
   D3D11_BUFFER_DESC bDesc;
   D3D11_SUBRESOURCE_DATA bData;

   bDesc.Usage = D3D11_USAGE_DEFAULT;
   bDesc.ByteWidth = byteWidth;
   bDesc.BindFlags = bindFlags;

   bDesc.CPUAccessFlags = 0;
   bDesc.MiscFlags = 0;
   //
   bData.pSysMem = pData;
   bData.SysMemPitch = 0;
   bData.SysMemSlicePitch = 0;
   D3D_CHECK(pDevice->CreateBuffer(&bDesc, pData ? &bData : nullptr, &pBuf));

   return pBuf;
}

d3dBUFFER* d3dDRIVER::CreateVertexBuffer(void *pVert, int byteWidth)
{
   ID3D11Buffer *ptr = CreateBuffer(pDevice, D3D11_BIND_VERTEX_BUFFER, pVert, byteWidth);
   d3dBUFFER *pBuf = new d3dBUFFER();
   pBuf->buf = ptr;
   buffersPool.push_back(pBuf);
   return pBuf;
}

d3dBUFFER* d3dDRIVER::CreateIndexBuffer(void *pInd, int byteWidth)
{
   ID3D11Buffer *ptr = CreateBuffer(pDevice, D3D11_BIND_INDEX_BUFFER, pInd, byteWidth);
   d3dBUFFER *pBuf = new d3dBUFFER();
   pBuf->buf = ptr;
   buffersPool.push_back(pBuf);
   return pBuf;
}

d3dCONSTANT_BUFFER* d3dDRIVER::CreateConstantBuffer(int byteWidth)
{
   ID3D11Buffer *ptr = CreateBuffer(pDevice, D3D11_BIND_CONSTANT_BUFFER, nullptr, byteWidth);
   d3dCONSTANT_BUFFER *pBuf = new d3dCONSTANT_BUFFER(byteWidth);
   pBuf->buf = ptr;
   buffersPool.push_back(pBuf);
   return pBuf;
}

void d3dDRIVER::UploadConstantBuffer(d3dCONSTANT_BUFFER *pBuf)
{
   pImmContext->UpdateSubresource(pBuf->buf, 0, nullptr, pBuf->cpuData, 0, 0);
   pBuf->needUpload = false;
}

d3dDRIVER::d3dSTATE* d3dDRIVER::CreateNewState(const d3dSTATE_DESC &desc)
{
   d3dSTATE *newState = new d3dSTATE();
   // Raster state
   {
      D3D11_RASTERIZER_DESC rsDesc;

      rsDesc.AntialiasedLineEnable = false;
      D3D11_CULL_MODE cMode[3] = {D3D11_CULL_NONE, D3D11_CULL_BACK, D3D11_CULL_FRONT};
      rsDesc.CullMode = cMode[(int)desc.cullMode];
      rsDesc.DepthBias = 0;
      rsDesc.DepthBiasClamp = 0.0f;
      rsDesc.DepthClipEnable = false;
      D3D11_FILL_MODE fillModes[] = {D3D11_FILL_SOLID, D3D11_FILL_WIREFRAME};
      rsDesc.FillMode = fillModes[(int)desc.fillMode];
      rsDesc.FrontCounterClockwise = false;
      rsDesc.MultisampleEnable = false;
      rsDesc.ScissorEnable = false;
      rsDesc.SlopeScaledDepthBias = 0.0f;

      D3D_CHECK(pDevice->CreateRasterizerState(&rsDesc, &newState->pRasterState));
   }
   // Depth-stencil state
   {
      D3D11_DEPTH_STENCIL_DESC modeDesc = {0};

      modeDesc.DepthEnable = desc.zTest;
      D3D11_COMPARISON_FUNC zFunc[] = {D3D11_COMPARISON_LESS_EQUAL,
                                       D3D11_COMPARISON_LESS,
                                       D3D11_COMPARISON_ALWAYS,
                                       D3D11_COMPARISON_GREATER,
                                       D3D11_COMPARISON_GREATER_EQUAL};
      modeDesc.DepthFunc = zFunc[(int)desc.zFunc];
      modeDesc.DepthWriteMask = desc.zWrite ? D3D11_DEPTH_WRITE_MASK_ALL : D3D11_DEPTH_WRITE_MASK_ZERO;
      modeDesc.StencilEnable = false;
      modeDesc.StencilEnable = desc.stencilMode > d3dSTATE_DESC::S_NONE;
      if (desc.stencilMode == d3dSTATE_DESC::S_SET) {
        modeDesc.StencilReadMask = 0xFF;
        modeDesc.StencilWriteMask = 0xFF;
        modeDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
        modeDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
        modeDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
        modeDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_INCR;
        modeDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
        modeDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
        modeDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
        modeDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
        newState->stencilRef = 0;
      } else if (desc.stencilMode == d3dSTATE_DESC::S_CHECK) {
        modeDesc.StencilReadMask = 0xFF;
        modeDesc.StencilWriteMask = 0xFF;
        modeDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
        modeDesc.FrontFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
        modeDesc.FrontFace.StencilFunc = D3D11_COMPARISON_NOT_EQUAL;
        modeDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
        modeDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
        modeDesc.BackFace.StencilFailOp = D3D11_STENCIL_OP_KEEP;
        modeDesc.BackFace.StencilFunc = D3D11_COMPARISON_NOT_EQUAL;
        modeDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
        newState->stencilRef = 0;
      }
      
      //
      D3D_CHECK(pDevice->CreateDepthStencilState(&modeDesc, &newState->pDSState));
   }
   // Blend state
   {
      D3D11_BLEND_DESC bDesc;

      bDesc.AlphaToCoverageEnable = false;
      bDesc.IndependentBlendEnable = false;
      bDesc.RenderTarget[0].BlendEnable = false;
      bDesc.RenderTarget[0].RenderTargetWriteMask = D3D11_COLOR_WRITE_ENABLE_ALL;

      D3D_CHECK(pDevice->CreateBlendState(&bDesc, &newState->pBlendState));
      memset(newState->blendFactors, 0, sizeof(newState->blendFactors));
      newState->sampleMask = 0xFFFFFFFF;
   }
   return newState;
}

UINT d3dDRIVER::FitD3DState(const d3dSTATE_DESC &desc) {
   UINT descID = desc.GetCode();
   for (int i = 0; i < (int)states.size(); ++i) {
      if (states[i]->descID == descID) {
         return i;
      }
   }
   // Add new one
   d3dSTATE *newState = CreateNewState(desc);
   newState->descID = descID;
   states.push_back(newState);
   return states.size() - 1;
}

void d3dDRIVER::SetD3DState(UINT ID)
{
   d3dSTATE *newState = states[ID];

   if (isWireframe) {
      newState = wireframeState;
   }

   if (curState != newState) {
      if (curState->pRasterState != newState->pRasterState) {
         pImmContext->RSSetState(newState->pRasterState);
      }
      if (curState->pDSState != newState->pDSState || curState->stencilRef != newState->stencilRef) {
         pImmContext->OMSetDepthStencilState(newState->pDSState, newState->stencilRef);
      }
      if (curState->pBlendState != newState->pBlendState ||
          memcmp(curState->blendFactors, newState->blendFactors, sizeof(curState->blendFactors)) ||
          curState->sampleMask != newState->sampleMask) {
         pImmContext->OMSetBlendState(newState->pBlendState, newState->blendFactors, newState->sampleMask);
      }
   }
   curState = newState;
}