#ifndef _VID_DRV_H_
#define _VID_DRV_H_
#include <d3dx9.h>
#include "d3d_drv.h"

class BASE_SHADER : public d3dSHADER
{
public:
   const D3D11_INPUT_ELEMENT_DESC* CreateIALayout(int *num);
};

class QUAD_SHADER : public d3dSHADER
{
public:
  const D3D11_INPUT_ELEMENT_DESC* CreateIALayout(int *num);
};

class BASE_STENCIL_SHADER : public BASE_SHADER
{
public:
  BASE_STENCIL_SHADER() : BASE_SHADER() {
    skipPixelShader = true;
  }
};

class scnPRIM;
class scnHIERARCHY;

enum SCENE_NAME {
  BRIDGE2
};
class rendMANAGER
{
private:
   static const int vecWidth = 4 * sizeof(float);
   static const int matWidth = 4 * vecWidth;
   // Common constants per frame
   d3dCONSTANT_BUFFER *pFrameCB;
   d3dCONSTANT_BUFFER *pObjCB;
   d3dCONSTANT_BUFFER *pLightCB;
   d3dCONSTANT_BUFFER *pQuadFrameCB;
   d3dCONSTANT_BUFFER *pQuadBoxesCB;

   d3dCONSTANT_BUFFER *bufStates[3];
   static const int MAX_NODES_COUNT = 9;
   static const int COMMON_SLOT = 0;
   static const int OBJ_SLOT = 1;
   static const int LIGHTING_SLOT = 2;
   static const int COMMON_VIEWPROJ_MATRIX_OFFSET = 0;
   static const int COMMON_EYEPOS_OFFSET = 4;
   static const int COMMON_BUFFER_SIZE = 5 * vecWidth;
   static const int OBJ_WORLD_MATRIX_OFFSET = 0;
   static const int OBJ_WORLD_INV_MATRIX_OFFSET = 4;
   static const int OBJ_BUFFER_SIZE = 8 * vecWidth;
   static const int LIGHTING_BUFFER_SIZE = 6 * 3 * vecWidth;
   static const int QUAD_FRAME_BUFFER_SIZE = 4 * vecWidth + matWidth;
   static const int QUAD_BOXES_BUFFER_SIZE = 9 * (matWidth + 2 * vecWidth);
   void SetupConstantBuffer(d3dDRIVER *, d3dCONSTANT_BUFFER *, UINT slot);
public:
   rendMANAGER();
   ~rendMANAGER();
   void InitCommonData(d3dDRIVER *);
   void SetTransform(const D3DXMATRIX &m);
   void SetViewProjTransform(const D3DXMATRIX &m);
   void SetLightingData(void *pData, int size);
   void SetEyePos(const D3DXVECTOR3& eyePos);
   void SetFullscreenData(const D3DXVECTOR3& eyePos, const D3DXVECTOR3& eyeTarget, 
                          const D3DXMATRIX &projM, const D3DXMATRIX &viewM,
                          float time);
   void UploadCommonData(d3dDRIVER *);
   void Render(d3dDRIVER*, scnPRIM *);
   void Render(d3dDRIVER*, scnHIERARCHY *);
   void RenderScreenQuad(d3dDRIVER *, scnPRIM *);
   void SetBoxesData(SCENE_NAME scn, scnPRIM *);
};
#endif