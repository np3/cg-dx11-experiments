#pragma once
#ifndef _COMMON_H_
#define _COMMON_H_

#pragma warning(disable:4005)
const float EPS = 0.0001f;

#define DBG_OUTPUT(x) OutputDebugString((x))

void *align_malloc(int size, int alignment);
void align_free(void *p);

unsigned char* GeneratePerlinLikeNoise(int W, int H);

#endif