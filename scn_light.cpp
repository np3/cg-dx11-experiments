#include "scn_light.h"
#include <cstring>
#include <cmath>
#include "common.h"

scnLIGHT::scnLIGHT(Type t) : type(t)
{
   diffuse.x = 1.0f, diffuse.y = 1.0f, diffuse.z = 1.0f;
   specular.x = 0.0f, specular.y = 0.0f, specular.z = 0.0f, specular.w = 1.0f;
   direction.x = 1.0f, direction.y = 0.0f, direction.z = 0.0f;
   memset(&position, 0, sizeof(position));
   attenuation.x = 1.0f, attenuation.y = 0.0f, attenuation.z = 0.0f;
   falloff = 2.0f;
   range = 1.0f;
   theta = D3DX_PI * 0.25f;
   phi = 0.0f; // penumbra
   enabled = true;
}

void scnLIGHT::TransformDir(D3DXVECTOR3& d, scnTRANSFORM &t)
{
   D3DXMATRIX& m = t.Get();

   d.x = d.x * m.m[0][0] + d.y * m.m[1][0] + d.z * m.m[2][0];
   d.y = d.x * m.m[0][1] + d.y * m.m[1][1] + d.z * m.m[2][1];
   d.z = d.x * m.m[0][2] + d.y * m.m[1][2] + d.z * m.m[2][2];
}

void scnLIGHT::TransformPoint(D3DXVECTOR3& p, scnTRANSFORM &t)
{
   D3DXMATRIX& m = t.Get();

   TransformDir(p, t);
   p.x += m.m[3][0];
   p.y += m.m[3][1];
   p.z += m.m[3][2];
}

void scnLIGHT::SetAttenuation(float att0, float att1 /* = 0.0f */, float att2 /* = 0.0f */)
{
   attenuation.x = att0;
   attenuation.y = att1;
   attenuation.z = att2;
}

void scnLIGHT_MANAGER::PushLight(scnLIGHT *p)
{
   lights.push_back(p);
}

void* scnLIGHT_MANAGER::BuildLightData()
{
   packed.clear();
   packed.resize(3);
   for (int i = 0; i < (int)lights.size(); ++i) {
      // Pack light here
      PACKED_LIGHT &pl = packed[i];
      scnLIGHT *light = lights[i];
      
      if (!light->enabled) {
         continue;
      }
      //pl.diffuse = XMLoadFloat3(&light->diffuse);
      const int F4width = 4 * sizeof(float);
      const int F3width = 3 * sizeof(float);
      memcpy_s(&pl.diffuse, F4width, &light->diffuse, F3width);
      memcpy_s(&pl.specular, F4width, &light->specular, F4width);
      memcpy_s(&pl.att, F4width, &light->attenuation, F3width);

      float r2 = light->range * light->range;
      float w = 1.0f / (r2 * r2);
      D3DXVECTOR3 tD = light->direction;
      D3DXVECTOR3 tP = light->position;
      switch (light->GetType()) {
         case scnLIGHT::DIRECTIONAL:
            {
               light->TransformDir(tD, light->Transform());
               D3DXVec3Normalize(&tD, &tD);
               memcpy_s(&pl.posDir, F4width, &tD, F3width); 
               pl.posDir.w = 0.0f;
            }
            
            break;
         case scnLIGHT::POINT:
            light->TransformPoint(tP, light->Transform());
            memcpy_s(&pl.posDir, F4width, &tP, F3width); 
            pl.posDir.w = 1.0f;
            pl.att.w = w;
            break;
         case scnLIGHT::SPOT:
            {
               light->TransformPoint(tP, light->Transform());
               memcpy_s(&pl.posDir, F4width, &tP, F3width); 
               pl.posDir.w = 1.0f;
               light->TransformDir(tD, light->Transform());
               D3DXVec3Normalize(&tD, &tD);
               memcpy_s(&pl.spotDir, F4width, &tD, F3width);
               float cosTheta = cos(0.5f * light->theta);
               float cosPhi = cos(0.5f * light->phi);
               float x = (cosTheta - cosPhi);
               float y = - cosPhi / (x + EPS);
               float z = light->falloff;

               pl.params.x = x;
               pl.params.y = y;
               pl.params.z = z;

               pl.att.w = w;
            }
            
            break;
      }
   }
   return &packed[0];
}
