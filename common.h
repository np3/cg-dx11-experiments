#pragma once
#ifndef _COMMON_H_
#define _COMMON_H_

#pragma warning(disable:4005)
const float EPS = 0.0001f;
#ifdef _DEBUG
#define DBG_OUTPUT(x) OutputDebugString((x))
#else
#define DBG_OUTPUT(x) (void)0
#endif

void *align_malloc(int size, int alignment);
void align_free(void *p);

#endif