#include "common.h"
#include <cstdlib>

void *align_malloc(int size, int alignment)
{
   // Very simple alignment
   int offset = alignment - 1 + sizeof(void *);
   void *p = malloc(size + offset);
   void *res = (void *)(((int)p + alignment) & ~offset);
   // pack original ptr for correct free
   *((int*)res - 1) = (int)p;
   return res;
}

void align_free(void *p)
{
   void *pp = ((int *)p - 1);
   free(pp);
}