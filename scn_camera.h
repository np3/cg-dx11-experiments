#pragma once
#ifndef _SCN_CAMERA_H_
#define _SCN_CAMERA_H_
#include <d3dx9.h>

class scnCAMERA
{
public:
   scnCAMERA();
   const D3DXMATRIX& GetViewMatrix() { return viewMatrix; }
   const D3DXMATRIX& GetProjMatrix() { return projMatrix; }
   const D3DXMATRIX& GetViewProjMatrix();
   void LookAt(D3DXVECTOR3& pos, 
               D3DXVECTOR3& target,
               D3DXVECTOR3& up);
   void BuildPerspProjMatrix(float ang, float aRatio,
                             float zNear, float zFar);
   scnCAMERA& RotateLookAtVert(float ang);
   scnCAMERA& RotateLookAtHorz(float ang);
   scnCAMERA& Zoom(float);
   const D3DXVECTOR3& GetPos() { return pos; }
   bool IsMoved() { return camMoved; }
   void ResetMoveFlag() { camMoved = false; }
private:
   void Rebuild();
   void BuildViewMatrix();
   D3DXMATRIX viewMatrix, projMatrix, viewProjMatrix;
   // Camera coordinate system
   D3DXVECTOR3 right, up, look, pos;
   D3DXVECTOR3 at;
   float radius;  
   float theta, phi;
   bool camMoved;
};
#endif