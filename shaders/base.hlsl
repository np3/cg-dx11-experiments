//#include "common.hlsl"
#define MAX_LIGHTS 3

cbuffer perFrame : register(b0)
{
    matrix ViewProj;
    float4   eyePos;
}

cbuffer perObj : register(b1)
{
   matrix World;
   matrix WorldInv;
}

struct LIGHT_DATA
{
    float4 posDir; // w - parallel/non-parallel
    float4 spotDir;
    float4 diffuse;
    float4 specular;
    float4 params; // x, y - spot angular factors, z - falloff
    float4 att;
};

cbuffer lightBuffer : register(b2)
{
    LIGHT_DATA litData[MAX_LIGHTS];
}

struct VS_INPUT
{
    float3 Pos   : POSITION;
    float3 Norm  : NORMAL;
    float4 Color : COLOR0;
    float2 UV    : TEXCOORD0;
};

struct VS_OUTPUT
{
    float4 Pos  : SV_POSITION;
    float3 Norm : NORMAL;
    float4 Col  : COLOR0;
    float2 UV   : TEXCOORD0;
    float4 WPos : TEXCOORD1; // w - view-space Z
};

float calcAttenuation(in float3 L, in float4 attFactors)
{
   float dist = length(L);
   float dist_2 = dist * dist;
   float d4 = dist_2 * dist_2;
   float inv_r4 = attFactors.w;
   float k = saturate(1 - inv_r4 * d4);

   return (k * k) / (attFactors.x + attFactors.y * dist + attFactors.z * dist_2);
}

float calcSpotFactor(in float3 L, in float3 spotDir, in float3 spotParams)
{
   float spotFactor = dot(-L, spotDir);
   spotFactor = saturate(spotFactor * spotParams.x + spotParams.y);
   spotFactor = pow(spotFactor, spotParams.z);
   return spotFactor;
}

void calcLighting(in VS_OUTPUT input, inout float3 diff, inout float3 spec, in float3 V)
{
   diff = 0;
   spec = 0;

   [unroll]
   for (uint i = 0; i < MAX_LIGHTS; ++i) {
      float3 L = litData[i].posDir.xyz - input.WPos.xyz * litData[i].posDir.w;
      float att = calcAttenuation(L, litData[i].att);
      L = normalize(L);
      float spotFactor = calcSpotFactor(L, litData[i].spotDir.xyz, litData[i].params.xyz);
      att *= spotFactor;
      float NdotL = saturate(dot(input.Norm, L));
      diff += att * NdotL * litData[i].diffuse.rgb;
      float3 H = normalize(L + V); // half - vector
      float NdotH = saturate(dot(input.Norm, H));
      NdotH = pow(NdotH, litData[i].specular.a);
      spec += att * NdotH * litData[i].specular.rgb;
   }
}

VS_OUTPUT VS(VS_INPUT input)
{
    VS_OUTPUT output = (VS_OUTPUT)0;
    float4 pos = float4(input.Pos, 1.0);
    float4 WPos = mul(pos, World);

    output.Pos = mul(WPos, ViewProj);
    output.Norm = mul(float4(input.Norm, 1.0), WorldInv).xyz;
    output.Col = input.Color;
    output.UV = input.UV;
    output.WPos = float4(WPos.xyz, output.Pos.w);
    return output;
}

float4 PS(VS_OUTPUT input) : SV_Target
{
    float3 litDiff = (float3)0;
    float3 litSpec = (float3)0;

    input.Norm = normalize(input.Norm);
    float3 V = normalize(eyePos.xyz - input.WPos.xyz);
    calcLighting(input, litDiff, litSpec, V);
    return float4(input.Col.rgb * litDiff + litSpec, input.Col.a);
}