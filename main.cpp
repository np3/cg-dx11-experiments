#include <windows.h>
#include "lab_wnd.h"

#pragma comment(lib, "d3d11.lib")
#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "dxerr.lib")
#pragma comment(lib, "D3DX11.lib")
//#pragma comment(lib, "Effects11.lib") 

INT WINAPI WinMain(HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmdLine, int nShowCmd)
{
   LAB_WND wnd("[CG] lab4", 800, 600);

   return wnd.Loop();
}