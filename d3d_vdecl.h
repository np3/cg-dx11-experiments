#pragma once
#ifndef _D3D_VDECL_H_
#define _D3D_VDECL_H_

enum d3dVERTEX_DECL : unsigned char
{
   POSITION_XYZ,
   TEXCOORD,
   COLOR,
   NORMAL,
   TANGENT
};
#endif