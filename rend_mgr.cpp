#include "common.h"
#include "rend_mgr.h"
#include "d3d_drv.h"
#include "scn_prim.h"

const D3D11_INPUT_ELEMENT_DESC* BASE_SHADER::CreateIALayout(int *num)
{
   //
   static D3D11_INPUT_ELEMENT_DESC inpDesc[4];

   inpDesc[0].Format = DXGI_FORMAT_R32G32B32_FLOAT;
   inpDesc[0].InputSlot = 0;
   inpDesc[0].AlignedByteOffset = 0;
   inpDesc[0].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
   inpDesc[0].SemanticIndex = 0;
   inpDesc[0].SemanticName = "POSITION";
   inpDesc[0].InstanceDataStepRate = 0;

   inpDesc[1].Format = DXGI_FORMAT_R32G32B32_FLOAT;
   inpDesc[1].InputSlot = 0;
   inpDesc[1].AlignedByteOffset = 3 * sizeof(float);
   inpDesc[1].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
   inpDesc[1].SemanticIndex = 0;
   inpDesc[1].SemanticName = "NORMAL";
   inpDesc[1].InstanceDataStepRate = 0;

   inpDesc[2].Format = DXGI_FORMAT_B8G8R8A8_UNORM;
   inpDesc[2].InputSlot = 0;
   inpDesc[2].AlignedByteOffset = 3 * sizeof(float) + inpDesc[1].AlignedByteOffset;
   inpDesc[2].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
   inpDesc[2].SemanticIndex = 0;
   inpDesc[2].SemanticName = "COLOR";
   inpDesc[2].InstanceDataStepRate = 0;

   inpDesc[3].Format = DXGI_FORMAT_R32G32_FLOAT;
   inpDesc[3].InputSlot = 0;
   inpDesc[3].AlignedByteOffset = sizeof(DWORD) + inpDesc[2].AlignedByteOffset;
   inpDesc[3].InputSlotClass = D3D11_INPUT_PER_VERTEX_DATA;
   inpDesc[3].SemanticIndex = 0;
   inpDesc[3].SemanticName = "TEXCOORD";
   inpDesc[3].InstanceDataStepRate = 0;
   //
   *num = sizeof(inpDesc) / sizeof(D3D11_INPUT_ELEMENT_DESC);

   return inpDesc;
}

rendMANAGER::rendMANAGER() {

}

rendMANAGER::~rendMANAGER() {

}

void rendMANAGER::InitCommonData(d3dDRIVER *d3d) {
   pFrameCB = d3d->CreateConstantBuffer(COMMON_BUFFER_SIZE);
   d3d->SetConstantBuffer(COMMON_SLOT, pFrameCB);
   pObjCB = d3d->CreateConstantBuffer(OBJ_BUFFER_SIZE);
   d3d->SetConstantBuffer(OBJ_SLOT, pObjCB);
   pLightCB = d3d->CreateConstantBuffer(LIGHTING_BUFFER_SIZE);
   d3d->SetConstantBuffer(LIGHTING_SLOT, pLightCB);
}

void rendMANAGER::UploadCommonData(d3dDRIVER *d3d)
{
   if (pFrameCB->NeedUpload()) {
      d3d->UploadConstantBuffer(pFrameCB);
   }
   if (pObjCB->NeedUpload()) {
      d3d->UploadConstantBuffer(pObjCB);
   }
   if (pLightCB->NeedUpload()) {
      d3d->UploadConstantBuffer(pLightCB);
   }
}

void rendMANAGER::SetTransform(const D3DXMATRIX &m)
{
   D3DXMATRIX tW;
   D3DXMatrixTranspose(&tW, &m);
   pObjCB->SetConst(OBJ_WORLD_MATRIX_OFFSET * vecWidth, (void *)&tW, matWidth);
   D3DXMatrixInverse(&tW, nullptr, &m);
   pObjCB->SetConst(OBJ_WORLD_INV_MATRIX_OFFSET * vecWidth, (void *)&tW, matWidth);
}

void rendMANAGER::SetViewProjTransform(const D3DXMATRIX &m)
{
   pFrameCB->SetConst(COMMON_VIEWPROJ_MATRIX_OFFSET * vecWidth, (void *)&m, matWidth);
}

void rendMANAGER::SetLightingData(void *pData, int size)
{
   pLightCB->SetConst(0, pData, size);
}

void rendMANAGER::Render(d3dDRIVER* drv, scnPRIM *prim)
{
   SetTransform(prim->LocalTransform().GetConst());
   UploadCommonData(drv);
   prim->Render(drv);
}

void rendMANAGER::Render(d3dDRIVER* drv, scnHIERARCHY *hierPrim)
{
   scnHIERARCHY_ITER iter(hierPrim);

   for (iter.Begin(); !iter.IsDone(); ++iter)
   {
      if (iter->prim) {
         scnTRANSFORM finalTr = iter->prim->LocalTransform() * iter->trans;
         SetTransform(finalTr.GetConst());
         UploadCommonData(drv);
         iter->prim->Render(drv);
      }
   }
}

void rendMANAGER::SetEyePos(const D3DXVECTOR3& eyePos)
{
   pFrameCB->SetConst(COMMON_EYEPOS_OFFSET * vecWidth, (void*)&eyePos, 3 * sizeof(float));
}