#pragma once
#ifndef _LAB_WND_H_
#define _LAB_WND_H_
#include "common.h"
#include "wnd.h"
#include "timer.h"
#include "d3d_drv.h"
#include "scn_camera.h"
#include "scn_prim.h"
#include "rend_mgr.h"
#include "scn_light.h"

class LAB_WND : public WND
{
private:
   Timer fpsTimer;
   int frameCounter;
   scnCAMERA camera;
   scnPRIM plane;
   scnPRIM *handParts[12];
   scnHIERARCHY hier;
   int prevMouseX;
   int prevMouseY;
   rendMANAGER rendMgr;
   scnLIGHT dirLight;
   scnLIGHT pointLight;
   scnLIGHT spotLight;
   scnLIGHT_MANAGER lightMgr;
   BASE_SHADER baseShader;
   float prevTime;
   void SetupHier();
public:
   LAB_WND(const char *pTitle, int Width, int Height);
   ~LAB_WND();
   void InputFunc(UINT msg, WPARAM wParam, LPARAM lParam);
   void PrepareFrame();
   void ProcessFrame();
};
#endif